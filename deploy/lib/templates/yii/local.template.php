<?php

return array(
	'name' => '--name--',
	'timezone' => '--timezone--',
	'components' =>
	array(
		'db' =>
		array(
			'connectionString' => 'mysql:host=--db_host--;dbname=--db_database--',
			'username' => '--db_username--',
			'password' => '--db_password--',
			'tablePrefix' => '',
		),
	)
);