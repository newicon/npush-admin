<div class="page-header">
	<h1>Updates</h1>
</div>
<a class="btn danger" href="<?php echo CHtml::normalizeUrl(array('/admin/updates/install')) ?>">Run Install</a>
<p class="hint">Runs the main application installation and calls the install function on each active module.</p>