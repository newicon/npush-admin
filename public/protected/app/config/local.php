<?php 
return array (
  'name' => 'Newicon Pusher Manager',
  'timezone' => 'Europe/London',
  'components' => 
  array (
    'db' => 
    array (
      'connectionString' => 'mysql:host=localhost;dbname=pusher',
      'username' => 'root',
      'password' => '',
      'tablePrefix' => '',
    ),
  ),
);
