<?php

/**
 * Activity class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Activity!! FEED!
 * 
 * A few key concepts to grasp with this component and all will make perfect sense.
 * Really there is only adding of logs and attaching render functions to these logs, just two steps.
 * BUT its best practise to attach logs through standard Yii events. DONT BE LAZY! USE EVENTS!
 * 
 * 1: The function to draw the activity log is seperate to the data. We have to specifically register a render function
 *    for each "type" of activity. The main thing is to add logs using @see Yii::app()->avtivity->add()
 *    then register render functions using Yii::app()->activity->renderer = array... or Yii::app()->activity->addRenderer()
 *    
 * 2: Each activity log is categorised by a top level "category" field and a "type" field. The category is typically the module which is responsible
 *    for that particular activity, for example adding an email to a contact could have the category of "contact" and the type of "email-add" or possibly
 *    you could create a more generic "attribute-changed" type. This is stored in the database together with any additional data you may want
 * 
 *    To add an activity log:
 *    Yii::app()->activity->add("category", "type", [data], $model)
 * 
 * 3: Activity logs are usually usefull events!
 *    Generally as activity logs represent key events in the system it is good practice to rasie an event where you would
 *    like to trigger the activity log then later attach the add log function Yii::app()->activity->add(...) to the event.
 *    you can attach data and info to the event object as necessary but typically you would write a function like so:
 *    <code>
 *    $object->onMyEvent = function($event) {
 *        Yii::app()->activity->add('mymodule', 'my-event-name', $event->params, $event->sender)
 *    }
 *    </code>
 * 
 * 4: After attaching to events and adding logs we need to create the function to render them and then register this with the activity component
 *    you can add individual renderers one at a time using Yii::app()->activity->addRenderer(...) but most of the time you will want to specify
 *    them in bulk or in the config like so
 *    Yii::app()->activity->renderers = array('mymodule'=>array(
 *        'my-event-name'=>array('NHtmlHelper', 'activityMyEventRender')
 *    ))
 * 
 * 5: Finally, the render function must return an array with the keys: title, link, body
 *    'title' is the title of the acitivty e.g. : Steve changed Lukes status to ...
 *    'link' a link url to show more detail or link to the record, resource or thing in question, if there isnt one this can be an empty string
 *    'body' body html of your choosing. Perhaps a progress bar, an image, an excerpt of a textual change etc etc.
 *    
 * <code>
 * // .. somewhere in a contact model (raise an event) (this is standard yii)
 * 
 * $data = array_merge($this->attributes, array('attribute_name'=>$name, 'attribute_old_value'=>$value))
 * Yii::app()->module('contact')->onAttributeChanged(new CEvent($this, $data))
 * 
 * // .. defined event in the contact module (this is standard yii)
 * //    if your module has lots of events its probably wise to create a seperate singleton class to attach and raise events from
 * //    just make sure it extends CComponent so that is can raise events and have events attached to it
 * 
 * public function onAttributeChanged($event)
 * {
 *     $this->raiseEvent('onAttributeChanged', $event);
 * }
 * 
 * // now we have our events (which may or may not already exist) we can set up our activity logs
 * // somewhere in our setup we can 
 * Yii::app()->activity->renderers = array('contact'=>array(
 *		'attribute-changed'=>array('NContactHtml','acitivtyAttributeChanged')
 * ))
 * 
 * Yii::app()->module('contact')->onAttributeChanged = function($event){
 *     Yii::app()->activity->add('contact', 'attribute-changed', $event->params, $event->sender)
 * }
 * 
 * // finally in NContactHtml
 * public static function($log)
 * {
 *     return array(
 *         'title' => $log->user->name . ' changed an attribute with name ' . $log->data['attribute_name'] . ' to ' . $log->data['attribute_old_value'],
 *		   'link'  => 'http://somelink.com',
 *         'body' => '<strong>congrats you managed to change it!</strong>'
 *     );
 * }
 * </code>
 */
class NActivity extends CApplicationComponent
{
	/**
	 * stores render functionsformat:
	 * array(
	 *		'category'=>array(
	 *			'type'=>callback
	 *		)
	 * )
	 * @var array
	 */
	protected $_renderFuns = array();
	
	/**
	 * Add a log feed item
	 * @param string $category the category of the log, typically the module name
	 * @param string $type the name of the log, e.g. task-status-change there should be a corosponding renderer with he same category and type defined
	 * @param array $data
	 * @param mixed $model
	 */
	public function add($category, $type, $data, $model='')
	{
		$al = new NActivityLog;
		$al->setAttributes(array(
			'category'=>$category,
			'type'=>$type,
			'model'=> is_object($model) ? get_class($model) : $model,
			'model_id'=> is_object($model) ? $model->id : 0,
			'date'=>NTime::unixToDatetime(time()),
			'data'=>$data,
			'user_id'=>Yii::app()->user->id
		));
		$al->save();
	}
	
	/**
	 * Use a render function to render the $data
	 * @param string $category
	 * @param string $type
	 * @param NActivityLog $log
	 * @return string
	 */
	public function render($category, $type, $log)
	{
		$callback = $this->getRenderer($category, $type);
		if(!is_callable($callback, true))
			throw new CException("The activity log render function for category:$category type:$type is not a valid callback function");
		return call_user_func($callback, $log);
	}

	/**
	 * set the render functions
	 *	$renderers = array(
	 *		'project'=>array(
	 *			'status_changed'=>'NProjectHtml::activityStatusChanged'
	 *			'assigned_changed'=>'NProjectHtml::activityAssigned'
	 *		)
	 * 	)
	 * 
	 * @param array $renderers
	 */
	public function setRenderers($renderers)
	{
		$this->_renderFuns = CMap::mergeArray($this->_renderFuns, $renderers);
	}
	
	/**
	 * return the list of renderer functions
	 * @return array
	 * format:
	 * array('category'=>array(
	 *		'type'=>callback
	 * ))
	 */
	public function getRenderers()
	{
		return $this->_renderFuns;
	}
	
	/**
	 * get the renderer callback function for the specified category and type
	 * @param string $category
	 * @param string $type
	 */
	public function getRenderer($category, $type)
	{
		//dp($this->_renderFuns);exit;
		if(!isset($this->_renderFuns[$category][$type]))
			throw new CException("There is no activity log renderer defined for category:$category, type:$type");
		return $this->_renderFuns[$category][$type];
	}
	
	/**
	 * Add a renderer function
	 * @param string $category the category of the log, typically the module name
	 * @param string $type the name of the log, e.g. task-status-change
	 * @param mixed $callback a callable php function (array, string or callback)
	 * @return type
	 */
	public function addRenderer($category, $type, $callback)
	{	
		$newRenderer = array($category=>array($type=>$callback));
		$this->_renderFuns = CMap::mergeArray($this->_renderFuns, $newRenderer);
	}
	
	/**
	 * install necessary table NActivityLog
	 */
	public static function install()
	{
		NActivityLog::install('NActivityLog');
	}
}