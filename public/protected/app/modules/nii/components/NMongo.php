<?php

/**
 * NMongo class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Static helper class for useful mongo db related functions
 *
 * @author 
 */
class NMongo
{
	
	/**
	 * checks if a collection with the given name $collectionName
	 * exists in the MongoDb $db
	 * @param MongoDB $db
	 * @param string $collectionName the name of the collection to look for
	 */
	public static function collectionExists ($db, $collectionName) 
	{
		if (!$db instanceof MongoDB || !$collectionName)
			return false;
		
		$collections = $db->listCollections();
		
		$colNames = array();
		foreach ($collections as $c) {
			$colNames[$c->getName()] = $c->getName();
		}
		
		return array_key_exists($collectionName, $colNames);
	}
	
}