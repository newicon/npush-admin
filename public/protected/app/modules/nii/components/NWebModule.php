<?php
/**
 * NWebModule class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>, 
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of NWebModule
 * 
 * NWebModule allows modules to be understood in the grander scheme of an application.
 * - Modules have an install and uninstall function to manage database table installation and module setup.
 * - Permissions system
 * - Setting system
 * - Helper to publish module specific assets.
 * @author Steven O'Brien <steven.obrien@newicon.net>, Luke Spencer <luke.spencer@newicon.net>
 */
class NWebModule extends CWebModule
{
    public $defaultController = 'index';
	
	/**
	 * Enabled property, yii's config accepts a config parameter.
	 * 
	 * @var boolean
	 */
	public $enabled;
	
	/**
	 * Atore an array of component=>array of behaviors
	 * 
	 * @var type 
	 */
	public $componentBehaviors;
	
	/**
	 * During module activation we want to access module info but not call the modules init function
	 * 
	 * @param type $id
	 * @param type $parent
	 * @param type $config 
	 */
	public function __construct($id, $parent, $config = null, $activate=true) {
		// if activate is false don't bother setting up the module officially
		// during module activation we want an instance of the module that we can use
		// but that does not set its self up and call its init functions or attach to the application
		//$this->_id = $id;
		$this->id = $id;
		if($activate)
			parent::__construct($id, $parent, $config);
	}
	
	/**
	 * Stores behavior arrays for components.
	 * Call this function within a components behavior function to get / add additional registered componentns.
	 * This enables the user module to be updated with additonal behaviors even 
	 * if the model / component has not been instantiated yet
	 * 
	 * @param string $componentName
	 * @return array
	 */
	public function getBehaviorsFor($componentName)
	{
        if (isset($this->componentBehaviors[$componentName])) {
            return $this->componentBehaviors[$componentName];
        } else {
            return array();
        }
	}
	
	/**
	 * enables the user module to store behavior mappings 
	 * to use this function the component / models should merge their behaviors array with the behaviors listed here.
	 * Or manualy check the getBehaviorsFor function
	 * 
	 * @see self::getBehaviorsFor
	 * @param string $componentName
	 * @param mixed $behavior string of behavior or behavior object
	 */
	public function addBehaviorFor($componentName, $behavior)
	{
		$this->componentBehaviors[$componentName] = $behavior;
	}
	
	/**
	 * Note that at this moment, the module is not configured yet.
	 * @see init
	 */
	public function preinit() 
	{
		parent::preinit();
		$this->loadSettings();
	}
	
	/**
	 * loads the modules database settings and applies them to the module.
	 * This function is called automatically when loading activated modules.  
	 * With core modules it is manually triggered in the preInit function.
	 * All configuration should be loaded before the init function
	 * @return void
	 */
	public function loadSettings()
	{
		$moduleConfig = Yii::app()->settings->get('system_modules', 'system', array());
		if(array_key_exists($this->name, $moduleConfig)){
			$this->configure($moduleConfig[$this->id]);
		}
	}
	
	/**
	 * stores the module.assets url
	 * @var type 
	 */
	private $_assetsUrl;
	
	/**
	 * Will publish a modules asset folder and return the url
	 * the assets folder should be in the root level of the module e.g. modules/user/asssets
	 * @return string the base URL to modules assets folder
	 */
	public function getAssetsUrl()
	{
		if($this->_assetsUrl===null)
			$this->_assetsUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias($this->getId().'.assets'));
		return $this->_assetsUrl;
	}

	/**
	 * Override this function to provide an install for the module
	 * This function is ran after the module initialisation
	 */
	public function install(){}
	
	/**
	 * Override this function to add uninstall code in the module
	 */
	public function uninstall(){}
	
	/**
	 * This function is called after all modules have been initialised.
	 * This can be used to access active record classes as the install function is ran before
	 */
	public function setup(){}
	
	/**
	 * NO COMMENTS!!! WTF??
	 */
	public function settings()
	{
		return array();
	}
	
	/**
	 * specifies an array which maps permissions
	 * you can then call $this->installPermissions() to install permissions
	 * for the module
	 * @return array in the format:
	 * return array(
	 *		
	 *		'user' => array(
	 *			'description' => 'Users', // name placed on the tab
	 *			'tasks' => array(
	 *				// task and properties
	 *				'view' => array(
	 *					'description' => 'View Users',    // displayed to the user as description of permission
	 *					'help'=>'This is some help text', // help text to describe this permission
	 *					'roles' => array('administrator'), // roles that should have these permisions by default
	 *					// not shown to user but used for authorisation
	 *					'operations' => array(
	 *						'user/admin/index',
	 *						'user/admin/users',
	 *						'menu-admin',
	 *						'/user/audit/index/',
	 *					),
	 *				),
	 *				'manage' => array(
	 *					'description' => 'Add/Edit/Delete Users',
	 *					'help' => 'Access to this permission enables you to add new users to the system, edit existing users, and remove users',
	 *					'roles' => array('administrator'),
	 *					'operations' => array(
	 *						'user/admin/addUser',
	 *						'user/admin/editUser',
	 *						'user/admin/deleteUser',
	 *						'user/admin/changePassword',
	 *					),
	 *				),
	 *			)
	 *		),
	 *		'my-feature-area' => array(
	 *			...
	 *		)
	 * )
	 */
	public function permissions()
	{
		return array();
	}
	
	/**
	 * loops though the permissions for this module and installs them
	 * @see self::permissions
	 * @return void
	 */
	public function installPermissions()
	{
		foreach($this->permissions() as $id => $permissions){
			if(!Yii::app()->authManager->getAuthItem('task-'.$id))
				Yii::app()->authManager->createTask('task-'.$id, $permissions['description']);
			// add help text
			if(array_key_exists('help', $permissions))
				UserTask::model()->updateByPk('task-'.$id, array('help'=>$permissions['help']));
			
			foreach($permissions['tasks'] as $taskName => $task){
				if(!Yii::app()->authManager->getAuthItem('task-'.$id.'-'.$taskName)){
					Yii::app()->authManager->createTask('task-'.$id.'-'.$taskName, $task['description']);
					// add help text
					if(array_key_exists('help', $task))
						UserTask::model()->updateByPk('task-'.$id.'-'.$taskName, array('help'=>$task['help']));
					
					if(!Yii::app()->authManager->hasItemChild('task-'.$id, 'task-'.$id.'-'.$taskName))
						Yii::app()->authManager->addItemChild('task-'.$id, 'task-'.$id.'-'.$taskName);
					// Only apply tasks to roles if the task doesn't exist
					foreach($task['roles'] as $role){
						if(Yii::app()->authManager->getAuthItem('role-'.$role)){
							if(!Yii::app()->authManager->hasItemChild('role-'.$role, 'task-'.$id.'-'.$taskName))
								Yii::app()->authManager->addItemChild('role-'.$role, 'task-'.$id.'-'.$taskName);
						}
					}
				}
				foreach($task['operations'] as $operation){
					if(!Yii::app()->authManager->getAuthItem($operation))
						Yii::app()->authManager->createOperation($operation);
					if(!Yii::app()->authManager->hasItemChild('task-'.$id.'-'.$taskName, $operation))
						Yii::app()->authManager->addItemChild('task-'.$id.'-'.$taskName, $operation);
				}
			}
		}
	}
}