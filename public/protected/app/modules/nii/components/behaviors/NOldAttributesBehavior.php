<?php

/**
 * newPHPClass class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * This class enables an active record to remember the values originally loaded from the database
 * - typical useage is to check the difference between old values and new attribute values saved
 *
 * @author steve
 */
class NOldAttributesBehavior extends CActiveRecordBehavior {

	/**
	 * store the old attributes
	 * "attribute name"=>"attribute value"
	 * @var array 
	 */
    private $_oldAttributes = array();

	/**
	 * whether the old attributes should be refreshed after the model is saved
	 * defaults false
	 * @var boolean 
	 */
    public $reloadAfterSave = false;

	/**
	 * after find method is used to store the attributes (in $this->_oldAttributes) as soon
	 * as the record has been found and instantiated
	 * @param CEvent $event 
	 * @return void
	 */
    public function afterFind($event) 
	{
        parent::afterFind($event);
        $this->_getAttributesFromOwner();
    }

	/**
	 * called after save, will update the store of old attributes if
	 * specified by $reloadAfterSave
	 * @param CEvent $event 
	 * @return void
	 */
    public function afterSave($event) 
	{
        parent::afterSave($event);
        if($this->reloadAfterSave) {
            $this->_getAttributesFromOwner();
        }
    }

	/**
	 * store the attributes currently on the owners model
	 * @return void 
	 */
    private function _getAttributesFromOwner() 
	{
		$this->_oldAttributes = $this->owner->attributes;
    }

	/**
	 * true or false if attributes have been changed
	 * @return boolean 
	 */
    public function attributesChanged() 
	{
        if(empty($this->_oldAttributes)) {
            return false;
        }
        
        $number_of_changed_attributes = count(array_diff($this->owner->attributes, $this->_oldAttributes));
        return $number_of_changed_attributes > 0;
    }

	/**
	 * retrun boolean true or false if the specified attribute has been changed
	 * @param string $attribute_name
	 * @return boolean 
	 */
    public function attributeChanged($attribute_name) 
	{
        if(array_key_exists($attribute_name, $this->_oldAttributes) && array_key_exists($attribute_name, $this->owner->attributes)) {
            return $this->_oldAttributes[$attribute_name] != $this->owner->$attribute_name;
        }

        return false;
    }
	
	/**
	 * get an array of all changed attributes
	 * @return array of attribute names that have been changed 
	 */
	public function getAttributesChanged()
	{
		$attrs = array();
		foreach($this->getOwner()->attributes as $attribute=>$value){
			if($this->attributeChanged($attribute))
				$attrs[] = $attribute;
		}
		return $attrs;
	}

	/**
	 * get the old value of the attribute (as orginially retrieved from db)
	 * @param string $attribute_name
	 * @return mixed | null attribute value or null if it does not exist
	 */
    public function getOldAttributeValue($attribute_name) 
	{
        if(array_key_exists($attribute_name, $this->_oldAttributes)) {
            return $this->_oldAttributes[$attribute_name];
        }
        return null;
    }
}