<?php

/**
 * NActivityFeed class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Description of NActivityFeed
 *
 * @author 
 */
class NActivityFeed extends NPortlet 
{
	
	public $title = '<h3>Activity</h3>';
	public $limit = 100;
//	public $contentCssClass='portlet-body h250 overflow-scroll mbs';
	
	protected function renderContent() 
	{
		$logs = NActivityLog::model()->findAll(array('order'=>'date DESC', 'limit'=>$this->limit));
		$this->render('feed', array('logs'=>$logs));
	}
}	