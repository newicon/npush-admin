<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php $oldD = 0; ?>
<?php foreach($logs as $k => $l): ?>
	<?php if (!$l->shouldDisplay()) continue; ?>
	<?php if($oldD == 0 || date('d-m-y', NTime::datetimeToUnix($l->date)) != date('d-m-y', NTime::datetimeToUnix($oldD))): ?>
		<div class="line pbm">
			<?php echo ($k) ? '<hr class="mtn" />' : ''; ?>
			<strong><?php echo NTime::niceShort($l->date, true); ?></strong>
		</div>
	<?php endif; ?>
	<div class="line " style="padding: 6px 6px; margin: 0px; margin-bottom: 10px;  position: relative; ">
		<div class="unit prm">
			<?php echo $l->user->profileImage; ?>
		</div>
		<div class="lastUnit">
			<?php echo $l->title; ?> <br/>
			<?php echo $l->body ?>
			<span class="help-block" style="color:#999">
				<?php echo $l->user->name; ?> <?php echo NTime::niceShort($l->date) ?>
			</span>
		</div>
	</div>
	<?php $oldD = $l->date; ?>
<?php endforeach; ?>
