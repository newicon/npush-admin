<?php

/**
 * Nii class file.
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

Class NEditInPlace extends NInputWidget
{
	
	public $noValue = 'No value';
	
	public function run()
	{
		$p = dirname(__FILE__).'/views/edit-in-place.js';
		// nasty but nice... Is there a sexy way to do js in php?? sob...
		$js = str_replace('[url]', NHtml::url('nii/widget/editInPlaceUpdate'), file_get_contents($p));
		Yii::app()->clientScript->registerScript('NEditInPlace', $js, CClientScript::POS_READY);
		$this->render('edit-in-place');
	}
}
