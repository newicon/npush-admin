$('.nii-edit-in-place .eip-view').on('click', function(e){
	var eip = $(e.currentTarget).closest('.nii-edit-in-place');
	var v = eip.find('.eip-view').hide();
	eip.find('.eip-edit').show().find('input').val(v.text()).focus();
})
$('.nii-edit-in-place .eip-edit input').on('blur', function(){
	$(this).closest('.nii-edit-in-place').find('.eip-view').show();$(this).closest('.eip-edit').hide();
}).on('keyup', function(e){
	if (e.which == 27) $(this).trigger('blur');
	if (e.which == 13){
		var val = $(this).val();
		var eid = $(this).closest('.nii-edit-in-place');
		$.post('[url]', {
			id:eid.attr('data-id'), val:val, attribute:eid.attr('data-attribute'), model:eid.attr('data-model')
		})
		$(this).closest('.eip-edit').hide();
		$(this).closest('.nii-edit-in-place').find('.eip-view').html(val).show();
	}
})