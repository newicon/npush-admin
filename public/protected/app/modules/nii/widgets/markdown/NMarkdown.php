<?php
/**
 * Show a markdown input textarea,
 * Transforms a typical textarea to have a preview button displaying markdown rendered
 * output for the textarea's text.
 *
 * @version 0.2
 */
class NMarkdown extends CInputWidget
{
	/**
	 * Route to a controller action that implements the markdownPreview action
	 * @var array
	 */
	public $action = '/nii/index/markdownPreview';

	/**
	 * The classes to be applied to the edit button
	 * @var array
	 */
	public $editButtonAttrs = array('class'=>'');

	/**
	 * The classes to be applied to the preview button
	 * @var array
	 */
	public $previewButtonAttrs = array('class'=>'');

	/**
	 * Options to be applied to the text area
	 * @var array
	 */
	public $htmlOptions = array();

	/**
	 * turn off displaying the buttons
	 * @var boolean 
	 */
	public $displayButtons = true;
	
	/**
	 * link to goto when help is clicked
	 * @var string 
	 */
	public $helpLink = 'http://daringfireball.net/projects/markdown/syntax';
	
	
	public function run() {
		// The location of the markdown widgets asset folder
		$assetLocation = dirname(__FILE__) . DIRECTORY_SEPARATOR. 'assets';

		// The location the system ajax's to for its prview
		$ajaxLocation = NHtml::url($this->action);
		
		list($name, $id) = $this->resolveNameID();
		
		// Includes the markdown style sheet
		$assetManager = yii::app()->getAssetManager();
		$assetFolder = $assetManager->publish($assetLocation);
		yii::app()->clientScript->registerCssFile("$assetFolder/style.css");
		yii::app()->clientScript->registerScriptFile("$assetFolder/markdown.js");
		// Handles the ajaxing of the preview
		$script = "jQuery('#md-$id').markdown({ajaxAction:'".NHtml::url($this->action)."'});";

		
		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('maskedinput');
		$cs->registerScript('MarkdownEditor', $script);
		
		// set default style for textarea
		if(isset($this->htmlOptions['style']))
			$this->htmlOptions['style'] = 'width:100%;margin:0px;border:0px;' . $this->htmlOptions['style'];
		
		// If I have a model then display an active text area.
		if ($this->hasModel())
			$inputElement = CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
		else
			$inputElement = CHtml::textArea($this->name, $this->value, $this->htmlOptions);
		$this->render('markdown', array('inputElement' => $inputElement,'id'=>$id));
	}
	
	/**
	 * echos out the buttons for edit and preview as well as the help link
	 */
	public function displayButtons(){
		$this->editButtonAttrs['class'] = $this->editButtonAttrs['class'] . ' edit active btn btn-mini';
		$this->previewButtonAttrs['class'] = $this->previewButtonAttrs['class'] . ' preview btn btn-mini';
		echo '<div class="unit size1of2 btn-group">';
		echo CHtml::link('Edit', '#', $this->editButtonAttrs);
		echo CHtml::link('Preview', '#', $this->previewButtonAttrs);
		echo '</div>';
		echo '<div class="lastUnit" style="text-align:right;">';
		echo CHtml::link('Help', $this->helpLink, array('target' => 'blank', 'class' => 'help_button'));
		echo '</div>';
	}
	
}