<div class="line note note<?php echo $data->id; ?>" data-noteid="<?php echo $data->id; ?>" data-model="<?php echo $data->model;?>" data-modelid="<?php echo $data->model_id;?>">
	<?php if($displayUserPic):?>
		<div class="unit profilePic prm">
			<div style="padding:2px;">
				<?php 
					$this->widget('user.widgets.NUserImage',array(
						'user' => $data->user_id,
					));
				?>
			</div>
		</div>
	<?php endif;?>
	<div class="lastUnit">
		<div class="nnote-text">
			<?php echo NHtml::renderMarkdown($data->note); ?>
		</div>
		
		<p class="hint">
			<?php echo $data->name . ', ' . NTime::timeAgoInWords($data->added);?>
		</p>
		<div>
			<div class="nnote-controls" style="display:none;">
				<?php if($canEdit):?>
					<i class="nnote-edit nnote-button icon-pencil" style="cursor:pointer;"></i>
				<?php endif; ?>
				 <?php if($canDelete):?>
					<i class="nnote-delete nnote-button icon-trash" style="cursor:pointer;"></i>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>