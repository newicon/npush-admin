<?php
/**
 * Copyright (c) 2010, Gareth Bond, http://www.gazbond.co.uk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *     following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *     the following disclaimer in the documentation and/or other materials provided with the distribution.
 *   * Neither the name of Yii Software LLC nor the names of its contributors may be used to endorse or
 *     promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Yii widget wrapper for Pupload: http://www.plupload.com/
 * Allows you to upload files using HTML5 Gears, Silverlight, Flash, BrowserPlus or normal forms,
 * providing some unique features such as upload progress, image resizing and chunked uploads.
 *
 * Config options: http://www.plupload.com/documentation.php
 *
 * Usage:
 * <pre>
 * <?php $this->widget('application.extensions.Plupload.PluploadWidget', array(
 *   'config' => array(
 *       'runtimes' => 'flash',
 *       'url' => '/image/upload/',
 *   ),
 *   'id' => 'uploader'
 * )); ?>
 * </pre>
 *
 * @author gazbond
 */ 
class PluploadWidget extends CWidget {

    const ASSETS_DIR_NAME       = 'assets';
    const PLUPLOAD_FILE_NAME    = 'plupload.full.js';
    const JQUERYQUEUE_FILE_NAME = 'jquery.plupload.queue/jquery.plupload.queue.js';
	const PUPLOAD_CSS_PATH      = 'jquery.plupload.queue/css/jquery.plupload.queue.css';
    const JQUERYUI_FILE_NAME    = 'jquery.ui.plupload/jquery.ui.plupload.js';
	const JQUERYUI_CSS_PATH     = 'jquery.ui.plupload/css/jquery.ui.plupload.css';
    const GEARS_FILE_NAME       = 'plupload.gears.js';
    const BROWSER_PLUS          = 'plupload.browserplus.js';
    const FLASH_FILE_NAME       = 'plupload.flash.swf';
    const SILVERLIGHT_FILE_NAME = 'plupload.silverlight.xap';
    const DEFAULT_RUNTIMES      = 'html5,flash,gears,silverlight,browserplus,html5';
    
    
    const I18N_DIR_NAME         = 'i18n';

    public $config = array();

    public $callbacks = array();

    
    public function init(){
        // dont do anything here so that we can use $this->createWidget() to access the widget object directly
    }
    
    public function setup() {        
		$this->registerScript();
    }
	
	/**
	 * process the filemanager id
	 * @return int filemanager id 
	 */
	public function processUploadFileManager()
	{
		$fileContents = $this->processUpload($targetDir, $fileName, $orginalName);
		$id = NFileManager::get()->addFile($fileName, $fileContents);
		return $id;
	}
	
	/**
	 * Process the upload
	 * @param string $targetDir byref 
	 * @param string $fileName
	 * @param string $orginalName
	 * @return binary file contents 
	 */
	public function processUpload(&$targetDir, &$fileName, &$orginalName)
	{
		// HTTP headers for no cache etc
		header('Content-type: text/plain; charset=UTF-8');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		
		// Settings
		$targetDir = Yii::getPathOfAlias('application.runtime');
		$cleanupTargetDir = false; // Remove old files
		$maxFileAge = 60 * 60; // Temp file age in seconds

		// 5 minutes execution time
		@set_time_limit(5 * 60);

		// Uncomment this one to fake upload time
		//sleep(5);
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
		$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
		$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
		$orginalName = $fileName;
		// Clean the fileName for security reasons
		$fileName = preg_replace('/[^\w\._]+/', '', $fileName);

		// Make sure the fileName is unique but only if chunking is disabled
		if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
			$ext = strrpos($fileName, '.');
			$fileName_a = substr($fileName, 0, $ext);
			$fileName_b = substr($fileName, $ext);

			$count = 1;
			while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
				$count++;

			$fileName = $fileName_a . '_' . $count . $fileName_b;
		}

		// Create target dir
		if (!file_exists($targetDir))
			@mkdir($targetDir);

		// Remove old temp files
		if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
			while (($file = readdir($dir)) !== false) {
				$filePath = $targetDir . DIRECTORY_SEPARATOR . $file;

				// Remove temp files if they are older than the max age
				if (preg_match('/\\.tmp$/', $file) && (filemtime($filePath) < time() - $maxFileAge))
					@unlink($filePath);
			}

			closedir($dir);
		} else {
			Yii::log('Failed to open temp directory.', 'error', 'nii.widgets.plupload');
			die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
		}

		// Look for the content type header
		if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
			$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

		if (isset($_SERVER["CONTENT_TYPE"]))
			$contentType = $_SERVER["CONTENT_TYPE"];

		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");

					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else {
						Yii::log('Failed to open input stream.', 'error', 'nii.widgets.plupload');
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					}
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else {
					Yii::log('Failed to open output stream.', 'error', 'nii.widgets.plupload');
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
				}
			} else {
				Yii::log('Failed to move uploaded file.', 'error', 'nii.widgets.plupload');
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		} else {
			// Open temp file
			$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");

				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else {
					Yii::log('Failed to open input stream.', 'error', 'nii.widgets.plupload');
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
				}
				fclose($in);
				fclose($out);
			} else {
				Yii::log('Failed to open output stream.', 'error','nii.widgets.plupload');
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			}
		}
		
		$file = $targetDir . DIRECTORY_SEPARATOR . $fileName;
		$fileContents = file_get_contents($file);
		
		// remove the tempory file
		unlink($file);
		
		return $fileContents;
	}
    
    
    public function getAssetsUrl()
	{
        $localPath = dirname(__FILE__) . "/" . self::ASSETS_DIR_NAME;
        return Yii::app()->getAssetManager()->publish($localPath);
    }
	
	
	public function registerScript()
	{
        $publicPath = $this->getAssetsUrl();

        if(!isset($this->config['flash_swf_url'])) {
            $flashUrl = $publicPath . "/" . self::FLASH_FILE_NAME;
            $this->config['flash_swf_url'] = $flashUrl;
        }

        if(!isset($this->config['silverlight_xap_url'])) {
            $silverLightUrl = $publicPath . "/" . self::SILVERLIGHT_FILE_NAME;
            $this->config['silverlight_xap_url'] = $silverLightUrl;
        }

        if(!isset($this->config['runtimes'])) {
            $this->config['runtimes'] = self::DEFAULT_RUNTIMES;
        }

        $runtimes = explode(',', $this->config['runtimes']);
        foreach($runtimes as $key => $value) {

            $value = strtolower(trim($value));
            if($value === 'gears') {
                $gearsPath = $publicPath . "/" . self::GEARS_FILE_NAME;
                Yii::app()->clientScript->registerScriptFile($gearsPath);
            }
            if($value === 'browserplus') {
                Yii::app()->clientScript->registerScriptFile(self::BROWSER_PLUS);
            }
        }

        $pluploadPath = $publicPath . "/" . self::PLUPLOAD_FILE_NAME;
        Yii::app()->clientScript->registerScriptFile($pluploadPath);

        $use_jquery_ui = (isset($this->config['jquery_ui']) && $this->config['jquery_ui']);
        if($use_jquery_ui) {

            $jQueryUIPath = $publicPath . "/" . self::JQUERYUI_FILE_NAME;
            Yii::app()->clientScript->registerScriptFile($jQueryUIPath);

            $jQueryUICssPath = $publicPath . "/" . self::JQUERYUI_CSS_PATH;
            Yii::app()->clientScript->registerCssFile($jQueryUICssPath);
        } else {

            $jQueryQueuePath = $publicPath . "/" . self::JQUERYQUEUE_FILE_NAME;
            Yii::app()->clientScript->registerScriptFile($jQueryQueuePath);

            $cssPath = $publicPath . "/" . self::PUPLOAD_CSS_PATH;
            Yii::app()->clientScript->registerCssFile($cssPath);
        }
	}

    public function run()
    {
        $this->setup();
//        echo "<div id=\"$this->id\">";
//        echo "<p>".Yii::t('plupload', "Your browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.")."</p>";
//        echo "</div>";
    }
}
