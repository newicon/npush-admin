<?php

/**
 * PluploadAction class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * Action for handling the uploading of a files from plupload widget
 * uses the NFileManager to manage the uploaded file
 */
class NPluploadAction extends CAction
{
	public function run()
	{
		$id = Yii::app()->controller->createWidget('nii.widgets.plupload.PluploadWidget')->processUploadFileManager();
		echo CJSON::encode(array(
			'id'=>$id,
			'url'=>NFileManager::get()->getUrl($id)
		));
	}
}