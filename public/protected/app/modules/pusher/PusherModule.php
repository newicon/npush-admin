<?php

class PusherModule extends NWebModule {

	public $name = 'Newicon Pusher Manager';
	public $description = 'Pusher manager to admin API';
	public $version = '1.0';
	
	public $show_menu = true;

	public function init() {
		Yii::import('pusher.components.*');
		Yii::import('pusher.models.*');
		$this->setComponent('mongo', new NMongo());
		Yii::app()->getClientScript()->registerPackage('gritter');
	}
	
	public function setup(){
		Yii::app()->menus->addMenu('main');
		Yii::app()->menus->addMenu('secondary');

		Yii::app()->menus->addItem('secondary', 'Admin', '#', null, array(
			'visible' => Yii::app()->user->checkAccess('menu-admin'),
		));
		Yii::app()->menus->addItem('secondary', 'Updates', array('/admin/updates/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/updates/index'),
		));
		Yii::app()->menus->addItem('secondary', 'Settings', array('/admin/settings/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/settings/index'),
		));
		Yii::app()->menus->addItem('secondary', 'Notifications', array('/admin/notifications/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/notifications/index'),
		));
		Yii::app()->menus->addItem('secondary', 'Modules', array('/admin/modules/index'), 'Admin', array(
			'visible' => Yii::app()->user->checkAccess('admin/modules/index'),
		));
		
		Yii::app()->menus->addMenu('user');
		Yii::app()->menus->addItem('user','User','#');
		Yii::app()->menus->addItem('user','My Account',array('/user/admin/account'),'User',array('linkOptions'=>array('data-toggle'=>'modal','data-target'=>'#modal-user-account','data-backdrop'=>'static')));
		Yii::app()->menus->addItem('user','Settings',array('/user/admin/settings'),'User');
		
		Yii::app()->menus->addDivider('secondary','Admin');
		Yii::app()->menus->addItem('secondary','Users',array('/user/admin/users'),'Admin',array(
			'visible' => Yii::app()->user->checkAccess('user/admin/users'),
		));
		Yii::app()->menus->addItem('secondary','Permissions',array('/user/admin/permissions'),'Admin',array(
			'visible' => Yii::app()->user->checkAccess('user/admin/permissions'),
		));
		
		Yii::app()->menus->addDivider('secondary','Admin');
		Yii::app()->menus->addItem('secondary','Audit Trail',array('/user/audit/index'),'Admin',array(
			'visible' => Yii::app()->user->checkAccess('user/audit/index'),
		));
		
		if($this->show_menu){
			Yii::app()->menus->addItem('main', 'Servers', '#', null, array(
				'visible' => Yii::app()->user->checkAccess('menu-kashflow'),
			));
			Yii::app()->menus->addItem('main', 'Create new', array('#'), 'Servers', array('linkOptions'=>array('id'=>'ModuleServerModalButtonNewServer', 'data-action' => 'new'),
			));
			Yii::app()->menus->addItem('main', 'Switch to server', array('/pusher/manager/switch-server'), 'Servers', array('linkOptions'=>array('data-toggle'=>'modal','data-target'=>'#ModuleServerModalSwitchServer'),
			));		
			Yii::app()->menus->addDivider('main','Servers');
			Yii::app()->menus->addItem('main', 'Change default', array('/pusher/manager/settings'), 'Servers', array('linkOptions'=>array('data-toggle'=>'modal','data-target'=>'#ModuleServerModalSettings'),
			));			
		}		
		
		if($this->show_menu){
			Yii::app()->menus->addItem('main', 'Current server', '#', '', array('itemOptions' => array("id"=>"ModuleServerCurrentMenu", "style" => "display:none;")), array());
			Yii::app()->menus->addItem('main', 'Settings', array('/pusher/manager/server-settings'), 'Current server', array('linkOptions'=>array('id'=>'ModuleServerModalButtonSettingServer', 'data-action' => 'update'),
			));	
			Yii::app()->menus->addDivider('main','Current server');
			Yii::app()->menus->addItem('main', 'Empty DB', array('/pusher/manager/server-mongo-empty'), 'Current server', array('linkOptions'=>array('data-toggle'=>'modal','data-target'=>'#ModuleServerModalServerMongoEmpty'),
			));								
			Yii::app()->menus->addDivider('main','Current server');
			Yii::app()->menus->addItem('main', 'Delete server', array('/pusher/manager/server-delete'), 'Current server', array('linkOptions'=>array('data-toggle'=>'modal','data-target'=>'#ModuleServerModalServerDelete'),
			));						
		}			
	}
	
	public function install(){
		$this->installPermissions();
		NActiveRecord::install('PusherManagerServer');
	}

	public function settings() {
		return array();
	}

	public function permissions() {
		return array(
			'admin' => array('description' => 'Admin',
				'tasks' => array(
					'modules' => array('description' => 'Manage Modules',
						'roles' => array('administrator'),
						'operations' => array(
							'admin/modules/index',
							'admin/modules/enable',
							'admin/modules/disable',
							'menu-admin',
						),
					),
					'settings' => array('description' => 'Manage Settings',
						'roles' => array('administrator'),
						'operations' => array(
							'admin/settings/index',
							'admin/settings/page',
							'admin/settings/general',
							'admin/settings/presentation',
							'menu-admin',
						),
					),
					'settings' => array('description' => 'Trash Items',
						'roles' => array('administrator','editor'),
						'operations' => array(
							'nii/index/trash',
						),
					),
					'dashboard' => array('description' => 'Dashboard',
						'roles' => array('administrator','editor','viewer'),
						'operations' => array(
							'admin/index/index',
							'admin/index/dashboard',
						),
					),
				),
			),
		);
	}

}