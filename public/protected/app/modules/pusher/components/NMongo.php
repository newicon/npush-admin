<?php

/**
 * Description of NMongo
 *
 * @author krzyztofstasiak
 */

class NMongo extends CApplicationComponent {
	
	public $mongo;
	
	public function __construct() {
		
		$this->mongo = false;
	}
	
	public function _connect($_address, $_user = '', $_passwd = '', $_port = '', $_db = '')
	{
		$_url = 'mongodb://';
		if($_user != '') $_url .= $_user.':'.$_passwd.'@';
		if($_address != '') $_url .= $_address;
		if($_port != '') $_url .= ':'.$_port;		

		try {
			$this->mongo = new Mongo($_url);
			
			if($_db != '') {
				
				$_dbs = $this->mongo->listDBs(); 
				
				foreach($_dbs['databases'] as $d) {
					
					if($d['name'] == $_db)
                        return $this->mongo->$_db;
				}
				
				return false;
			}
			
			return $this->mongo;
				
		} catch(Exception $e) {
			
			return false;
		}
	}
	
	public function _disconnect()
	{
		$this->mongo = false;
	}
}

?>
