<?php

/**
 * NPush class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * 
 * on the terminal:
 * > curl -H "Content-Type: application/post" -d '{"event":"message","data":"hello"}' "http://push.vm.newicon.net:80/api/channel/facilitator/room/0/api/ab3245cbeaf456244abcdfa/t/1345206734329/suid/1/suname/me"
 * send an event named "message" to the channel "facilitator"
 * @property string $api
 * @property $user_id
 * @property $user_name
 * @property $host 'http://push.vm.newicon.net';
 * @property $port = 80
 * @property $jsonEncode = true
 * @property $room = 0;
 * @property $debug = false;
 * @property $timeout = 30;
 */
class NPush extends CApplicationComponent
{
	
	const VERSION = '0.1';

	public $api;
	public $user_id;
	public $user_name;
	public $url = 'push.vm.newicon.net';
	public $port = 80;
	public $jsonEncode = true;
	public $room = 0;
	public $debug = false;
	public $timeout = 2;

	/**
	 * Trigger an event on a channel
	 * 
	 * @param string $channel
	 * @param string $event
	 * @param mixed $data will encode to json
	 * @param array $options optional paramters, you can override all deafult options here
	 * - jsonEncode: defaults to true, will run the passed $data through CJSON::encode(), set this to false if you $data is already json encoded
	 * - user_id: overrides default $this->user_id for this call
	 * - user_name: overrides default $this->user_name for this call
	 * @return bool|string
	 */
	public function trigger($channel, $event, $data, $options=array())
	{
		$t = time();
		$options = CMap::mergeArray($this->options, $options);
		FB::log($t);
		// user names might contain dodgy characters
		$uname = urlencode($options['user_id']);
		// make the url
		$url = "http://{$options['url']}:{$options['port']}/api/channel/$channel/room/{$options['room']}/api/{$options['api']}/version/2.0/t/$t/suid/{$options['user_id']}/suname/$uname";
		
		if ($options['jsonEncode'])
			$data = CJSON::encode(array('event'=>$event, 'data'=>$data));
		else
			// decode first then encode... nasty.. update the nodejs server stuff to accept the event as a url param and pass back correctly
			$data = CJSON::encode(array('event'=>$event, 'data'=>CJSON::decode($data)));
		
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: application/post"));
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $data);
		curl_setopt($c, CURLOPT_TIMEOUT, $options['timeout']);
		curl_setopt($c, CURLOPT_VERBOSE, true);
		$response = curl_exec($c);
		curl_close($c);
		return $response;
	}
	
	/**
	 * Trigger an event on a API
	 * 
	 * Message will be sent to all rooms and all channels
	 * 
	 * @param mixed $data will encode to json
	 * @param array $options optional paramters, you can override all deafult options here
	 * - jsonEncode: defaults to true, will run the passed $data through CJSON::encode(), set this to false if you $data is already json encoded
	 * - user_id: overrides default $this->user_id for this call
	 * - user_name: overrides default $this->user_name for this call
	 * @return bool|string
	 */
	public function triggerToAPI($event, $data, $options=array())
	{
		$t = time();
		$options = CMap::mergeArray($this->options, $options);
		FB::log($t);
		// user names might contain dodgy characters
		$uname = urlencode($options['user_id']);
		// make the url
		$url = "http://{$options['url']}:{$options['port']}/api/api/{$options['api']}/version/2.0/t/$t/suid/{$options['user_id']}/suname/$uname";
		
		if ($options['jsonEncode'])
			$data = CJSON::encode(array('event'=>$event, 'data'=>$data));
		else
			// decode first then encode... nasty.. update the nodejs server stuff to accept the event as a url param and pass back correctly
			$data = CJSON::encode(array('event'=>$event, 'data'=>CJSON::decode($data)));
		
		$c = curl_init();
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: application/post"));
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $data);
		curl_setopt($c, CURLOPT_TIMEOUT, $options['timeout']);
		curl_setopt($c, CURLOPT_VERBOSE, true);
		$response = curl_exec($c);
		curl_close($c);
		return $response;
	}	
	
	/**
	 * return a list of all the class options
	 * @return array
	 */
	public function getOptionNames()
	{
		return array('api','url','port','jsonEncode','user_id','user_name','room','debug','timeout');
	}

	/**
	 * return an array of all the class options and their values option=>value
	 * @return array
	 */
	public function getOptions()
	{
		$options = array();
		foreach($this->getOptionNames() as $option)
			$options[$option] = $this->$option;
		return $options;
	}
}