<?php

/**
 * Description of NManager
 *
 * @author krzyztofstasiak
 */

/*
 * $connection - returned from NMongo
 */

/*
 * COLLECTION : SETTINGS
 * 
 * { "_id" : ObjectId( "50a4d667547eb9d240691ece" ),
	"key" : "PUSH_VERSION",
	"value" : "1.0" 
 * }
 * 
 * COLLECTION : ACCOUNTS
 * { "_id" : ObjectId( "5044b7e1b7c54a69842e776f" ),
  "password" : "ab3245cbeaf456244abcdfa",
  "username" : "newicon",
  "client_room" : "newicon",
  "name" : "Newicon Ltd.",
  "apps" : { 
 *				"0" : { 
 *						"name" : "app1",
						"apis" : 
 *								{ 
 *									"0" : { 
*											"api_key" : "123456789",
											"name" : "for skype",
 *										} 
 *								} 
 *						} 
 *			} 
 * }
 * 
 * COLLECTION : APIS
 * 
 * { "_id" : ObjectId( "50a4bbcc547eb9d240691ecd" ),
  "api" : "123456789",
  "enabled" : "true" } 
 */

/*
 *  db.accounts.update({"_id" : ObjectId("50a52389b19160861a000000")}, { "$addToSet": { apps : { "name":"ccc" } } } );
 *  db.accounts.update({"apps.id" : ObjectId("50a63a2bb19160701f000000")}, { "$set" : { "apps.$.name" : "newname" }  });
 */

class NPusherManager {
	
	// MongoDB
	
	public $connection;
	
	// Manager Version
	public $version;
	
	// Collection Names in DB
	public $accounts_collection;
	public $settings_collection;
	public $apis_collection;
	
	public function __construct($connection) {
		
		$this->version = '1.0';
		$this->accounts_collection = 'accounts';
		$this->settings_collection = 'settings';
		$this->apis_collection = 'apis';
		
		$this->connection = $connection;
	}
	
	/*
	 * CREATE
	 */
	
	public function addClient($params)
	{
		return $this->insertDocument($this->accounts_collection, $params);
	}
	
	public function addApp($clientId, $params) 
	{
		return $this->updateDocument($this->accounts_collection, 
						array('_id' => new MongoId($clientId)), 
						array('$addToSet' => array('apps' => $params)));		
	}
	
	public function addAPIKey($clientId, $appId, $params)
	{
		return $this->updateDocument($this->accounts_collection, 
						array(
								'apps.id' => new MongoId($appId)
							), 
						array(
								'$addToSet' => array('apps.$.apis' => $params)));
	}
	
	public function addSetting($key, $value) 
	{
            $this->insertDocument($this->settings_collection, array($key => $value));
	}	
	
	/*
	 * DELETE
	 */
	
	public function deleteClient($clientId)
	{
		
	}
	
	public function deleteApp($clientId, $appId)
	{
		
	}
	
	public function deleteAPIKey($clientId, $appId, $apiId)
	{
		
	}	
	
	public function deleteSetting($key)
	{
		
	}		
	
	/*
	 * UPDATE
	 */
	
	public function updateClient($clientId)
	{
		
	}
	
	public function updateApp($clientId, $appId)
	{
		
	}
	
	public function updateAPIKey($clientId, $appId, $apiId)
	{
		
	}	
	
	public function updateSetting($key, $value)
	{
            
	}	
	
	/*
	 * LOAD DATA
	 */
	
	public function getClients($params = array(), $sort = array())
	{
		return $this->findDocuments($this->accounts_collection, $params, 0, $sort); //
	}
	
	public function getClientApps($params = array(), $sort = array())
	{
		return $this->findDocument($this->accounts_collection, $params, array(), $sort);
	}	
	
	public function getClientAppAPIs($params = array(), $sort = array())
	{
		return $this->findDocument($this->accounts_collection, $params, array(), $sort); //
	}	
	
	public function getClientAppAPIMessages($appId, $params, $sort, $limit = false, $fields = array())
	{
		return $this->findDocuments('msg_'.$appId, $params, 0, $sort, $limit, $fields);
	}
	
	public function getClientAppAPIMessageReceipt($appId, $msgId)
	{
		return $this->findDocuments('msg_read_'.$appId, array('msg_id' => $msgId));
	}	
	
	public function getClientAppAPIActivity($appId, $params, $sort, $limit = false)
	{
		return $this->findDocuments('act_'.$appId, $params, 0, $sort, $limit);
	}
    
    /*
     * DELETE DATA
     */
    
    public function deleteCollectionMessageDocument($clientId, $appId, $apiId, $msgId)
    {
        return $this->deleteDocument('msg_'.$apiId, array('_id' => new MongoId($msgId)), array("justOne" => true));
    }    
	
	/*
	 * STATS
	 */
	
	public function getMongoStats()
	{
		// n/a in first realse
	}

	/*
    * APIs Management
    */

	public function setAPIKeyEnabled($clientId, $appId, $apiId)
	{
		return $this->updateDocument($this->apis_collection, array('key' => $apiId), array('enabled' => 'true', 'key' => $apiId, 'client' => $clientId, 'time' => time()), array('upsert' => true));
	}
	
	public function setCreateAPIKeyEnabled($clientId, $appId, $apiId)
	{
		return $this->insertDocument($this->apis_collection, array('enabled' => 'true', 'key' => $apiId, 'client' => $clientId, 'time' => time()), array('upsert' => true));
	}	

	public function setAPIKeyDisabled($clientId, $appId, $apiId)
	{
		return $this->updateDocument($this->apis_collection, array('key' => $apiId), array('enabled' => 'false', 'key' => $apiId), array('upsert' => true));
	}

	public function getAPIKeyStatus($clientId, $appId, $apiId)
	{
		return $this->findDocument($this->apis_collection, array('key' => $apiId));
	}        

	public function getAPIActivity($clientId, $appId, $apiId)
	{
		$time = -1;
		$activity = $this->findDocument('act_'.$apiId, array());

		foreach($activity as $a)
		{
			if(isset($a['date'])) {
				if(isset($a['date']) && $a['date'] > $time) {

					$time = $a['date'];
				}
			}
		}

		return $time;
	}	

	public function getAPICollectionsMessageInfo($clientId, $appId, $apiId)
	{
		if($respond = $this->connection->execute('db.msg_'.$apiId.'.stats();'))
			if(isset($respond['retval'])) return $respond['retval'];

		return false;
	}

	public function getAPICollectionsActivityInfo($clientId, $appId, $apiId)
	{
		if($respond = $this->connection->execute('db.act_'.$apiId.'.stats();'))
			if(isset($respond['retval'])) return $respond['retval'];

		return false;
	}		
    
	/*
	 * SERVICE
	 */
	
	public function verifyInstalledServices()
	{
		// check if all collections exist
		// get version from collection "settings"
	}
	
	public function installServices($connection)
	{
		$this->emptyMongoData();
		
		// Collections
		$this->createCollection($this->settings_collection);
		$this->createCollection($this->accounts_collection);
		$this->createCollection($this->apis_collection);
		
		// Data
		$this->addSetting('PUSH_VERSION', $this->version);
				
		return false;
	}
	
	public function emptyMongoData()
	{
		return $this->deleteAllCollections();		
	}	
	
	public function dropCollection($collection)
	{
		return $this->deleteCollection($collection);		
	}	
	
	/*
	 * Prvate
	 */
	
	private function generateAPIKey($clientId, $appId, $apiId)
	{
		return false;
	}
    
    private function deleteDocument($colleciton, $params = array(), $options = array())
    {
        try {
            
            return $this->connection
                        ->$colleciton
                        ->remove($params, $options);
            
        } catch(MongoCursorException $e) {
            
            return false;
        }
    }
	
	private function insertDocument($collection, $params)
	{
		try {
			return $this->connection
						->$collection
						->insert($params, true);
		} catch(MongoCursorException $e) {
			
			return false;
		}
	}
	
	private function updateDocument($collection, $criteria, $params, $options = array())
	{
		try {
			return $this->connection
						->$collection
						->update($criteria, $params, $options);
		} catch(MongoCursorException $e) {
			
			return false;
		}
	}	
	
	private function findDocuments($collection, $params = array(), $skip = 0, $sort = array(), $limit = 30, $fields = array())
	{
		return $this->connection
					->$collection
					->find($params, $fields)
					->skip($skip)
					->limit($limit)				
					->sort($sort);
	}
	
	private function findDocument($collection, $params = array(), $fields = array(), $sort = array())
	{
		return $this->connection
					->$collection
					->find($params, $fields)
					->limit(1)				
					->sort($sort);
	}	
	
	private function createCollection($name)
	{
		return false;
	}
	
	private function deleteCollection($collection)
	{
		return $this->connection->$collection->drop();
	}
	
	private function deleteAllCollections()
	{
		$list = $this->connection->listCollections();
		foreach ($list as $collection) {

			$collection->drop();
		}
		return true;
	}
}