<?php

class ManagerController extends AController
{
	public $pusherManager;
	public $defaultServer;
	
	public $publicServerMongoAddress;
	public $publicServerMongoUsername;
	public $publicServerMongoPassword;
	public $publicServerMongoPort;
	public $publicServerMongoDbName;
	public $publicServerNPushAddress;
	public $publicServerNPushPort;
	public $publicServerNPushVersion;
	
	public $publicAPIOnErrorEmailTo;

	public $messagesCollectionData;
	public $activityCollectionData;
	
	public function init()
	{		
		$this->pusherManager = false;
		$this->defaultServer = false;
		
		$this->publicServerMongoAddress = 'pushdb.default.newicon.uk0.bigv.io';
		$this->publicServerMongoUsername = '';
		$this->publicServerMongoPassword = '';
		$this->publicServerMongoPort = '27017';
		$this->publicServerMongoDbName = 'ni_sockets';
		$this->publicServerNPushAddress = 'push.vm.newicon.net';
		$this->publicServerNPushPort = '80';
		$this->publicServerNPushVersion = '2.0';
		
		$this->publicAPIOnErrorEmailTo = 'chris.stasiak@newicon.net';
		
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../pusher/css/style.css');
		Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/../pusher/css/font-awesome.css');
	}
	
	/**
	 * Access rules for this controller
	 * only enable facilitators to access this area
	 * @return array 
	 */
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('CompileLibrary', 'CompileLibraryGetAPI'),
				'users'=>array('*'),
			),
		);
	}	
	
	public function createServerConnection()
	{
		if($mongo = $this->module->mongo->_connect($this->defaultServer->mongo_address, 
													$this->defaultServer->mongo_username, 
													$this->defaultServer->mongo_password, 
													$this->defaultServer->mongo_port, 
													$this->defaultServer->mongo_dbname
												))
		$this->pusherManager = new NPusherManager($mongo);		
	}
	
	public function getServerDefault()
	{
		return PusherManagerServer::model()->findByAttributes(array('default' => '1'));
	}	
	
	public function getServerSettings($serverId)
	{
		return PusherManagerServer::model()->findByPk($serverId);
	}
	
	public function actionIndex()
	{		
		if($this->defaultServer = $this->getServerDefault())
			$this->createServerConnection();
		
		$this->render('index', array(
									'manager' => $this->pusherManager,
									'server' => $this->defaultServer,
									'servers' => PusherManagerServer::model()->findAll()
								));
	}
	
	/*
	 * Server Management
	 */
	
    /*
     * Add & Update Server information
     */
    
	public function actionAddServer()
	{
		$new = PusherManagerServer::model()->findByPk($_POST['PusherManagerServer']['id']);
                if(!$new) $new = new PusherManagerServer();
		$new->attributes = $_POST['PusherManagerServer'];
		if($new->save()) {
			
			$this->respondHTMLStatus('OK', '', 0);
		} else {
			$this->respondHTMLStatus('ERROR');
		}
	}		
	
	public function actionDeleteServer($serverId)
	{
		
	}		
	
	/*
	 * Clients Management
	 */
	
	public function actionAddClient($serverId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
		
			$data = $_POST['PusherManagerClient'];
					
			if($this->pusherManager->getClients(array('username' => $data['username']))->count())
			{
				$this->respondHTMLStatus('USERNAME_EXIST');
			} else {					
                
				unset($data['password_repeat']);
				$data['client_room'] = $data['username'];
				
                if($data['id'] != '0') {
                    
                    // update action
                    
                } else {
                
                    if($this->pusherManager->addClient($data))
                    {
                        $this->respondHTMLStatus('OK');
                    } else {
                        $this->respondHTMLStatus('ERROR');
                    }
                }
			}
		}
	}	
	
	public function actionUpdateClient()
	{
		
	}
	
	public function actionDeleteClient()
	{
		
	}	
    
	public function actionDeleteItemFromMessageCollection($serverId, $clientId, $appId, $apiId, $msgId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
            
			if($this->pusherManager->deleteCollectionMessageDocument($clientId, $appId, $apiId, $msgId))
			{
				$this->respondHTMLStatus('OK');
			} else {
				$this->respondHTMLStatus('ERROR');
			}            
        }		
	}    
	
	/*
	 * Apps Management
	 */

	public function actionAddApp($serverId, $clientId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
		
			$data = $_POST['PusherManagerApp'];
			$data['id'] = new MongoId($serverId.$clientId.microtime());
					
			if($this->pusherManager->addApp($clientId, $data))
			{
				$this->respondHTMLStatus('OK');
			} else {
				$this->respondHTMLStatus('ERROR');
			}
		}
	}		
	
	public function actionUpdateApp()
	{
		
	}
	
	public function actionDeleteApp()
	{
		
	}	
	
	/*
	 * APIs Key Management
	 */
	
	public function actionAddAPIKey($serverId, $clientId, $appId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
		
			$data = $_POST['PusherManagerAPIKey'];
			$data['id'] = new MongoId($serverId.$clientId.microtime());
					
			if($this->pusherManager->addAPIKey($clientId, $appId, $data))
			{
				$this->respondHTMLStatus('OK');
			} else {
				$this->respondHTMLStatus('ERROR');
			}
		}
	}		

	public function actionUpdateAPIKey()
	{
		
	}
	
	public function actionDeleteAPIKey()
	{
		
	}
	
    /*
     * Empty MongoDB
     */
    
    public function actionEmptyMongoDatabase($serverId)
    {
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
            
            if($this->pusherManager->emptyMongoData())
                $this->respondHTMLStatus();
        } else {
			$this->respondHTMLStatus('ERROR');
		}
    }
    
	public function actionEmptyMongoCollectionMessageForAPI($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
            
            if($this->pusherManager->dropCollection('msg_'.$apiId));
                $this->respondHTMLStatus();
        }  
	}
	
	public function actionEmptyMongoCollectionActivityForAPI($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
            
            if($this->pusherManager->dropCollection('act_'.$apiId));
                $this->respondHTMLStatus();
        }  
	}	
	
	/*
	 * Preview data
	 */
	
	public function actionGetClientsInJSON($serverId) {
		
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
			
			$i = 0;
			$return = array();
			
			if($clients = $this->pusherManager->getClients())
				while($clients->hasNext()) {
					$data = $clients->getNext();
					if(isset($data['apps']))
						foreach($data['apps'] as $app)
							if(isset($app['apis']))
								foreach($app['apis'] as $api) {

									$return[(string)$api['id']] = array('cn'=>$data['username'], 'appn'=>$app['name'], 'apin'=>$api['name']);
								}
				}
			
			echo json_encode($return);
		}		
	}
	
	public function actionGetPreviewForClients($serverId, $name)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
			
            $search = array();
       
            if($name != '') {
                
                $search = array('name' => array('$regex' => $name));
            }                
                
			$this->renderPartial('ajax/_clients-list', array('manager' => $this->pusherManager, 'search' => $search, 'sort' => array('name' => 0)));
		} else {
			$this->respondHTMLStatus('NO_DB_CONNECTION');
		}
	}	
	
	public function actionGetPreviewForClientApps($serverId, $clientId, $name)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {

            $search = array('_id' => new MongoId($clientId));
       
            if($name != '') {
                
                $search = array('_id' => new MongoId($clientId), 'apps.name' => array('$regex' => $name));
            }                
            
			$this->renderPartial('ajax/_apps-list', array('client' => $this->pusherManager->getClientApps($search, array('sort' => array('name' => 0))), 'name' => $name));
		}		
	}			
	
	public function actionGetPreviewForClientAppAPIs($serverId, $clientId, $appId, $name)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
		
		if($this->pusherManager) {
			
			$search = array('apps.id' => new MongoId($appId));
			
            if($name != '') {
                
                $search = array('apps.id' => new MongoId($appId), 'apps.apis.name' => array('$regex' => $name));
            }			
			
			$this->renderPartial('ajax/_apis-list', array('apis' => $this->pusherManager->getClientAppAPIs($search, array('sort' => array('name' => 0))), 'name' => $name, 'appId' => $appId));
		}	
	}		
	
	public function actionGetPreviewForClientAPIKey($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
                
                if($this->pusherManager) {
                    
                    $status = $this->pusherManager->getAPIKeyStatus($clientId, $appId, $apiId);
                    
                    if($status && $status->count()) {
                        
                        $status->next(); 
                        $status = $status->current();
                    } else {
                        
                        $status = array('enabled' => false);
                    }
                    
                    $this->renderPartial('ajax/_preview', array(
											'server'=>$this->defaultServer, 
											'api' => $apiId,
											'apiStatus' => $status,
											'lastActivity' =>	$this->pusherManager->getAPIActivity($clientId, $appId, $apiId),
											'msgStorageInfo' => $this->pusherManager->getAPICollectionsMessageInfo($clientId, $appId, $apiId),
											'actStorageInfo' => $this->pusherManager->getAPICollectionsActivityInfo($clientId, $appId, $apiId),
											'messagesByDay' =>	$this->getChart('MESSAGES_LAST_7_DAYS', $apiId),
											'usersByDay' =>					$this->getChart('USERS_LAST_7_DAYS', $apiId),
											'usersAndMessagesByDay' =>		$this->getChart('USERS_MESSAGES_LAST_7_DAYS', $apiId),
										));
                } else {
                    $this->respondHTMLStatus('ERROR! try again...');
                }
	}	
    
	public function actionGetPreviewForServer($serverId)
	{    
        if($this->defaultServer = $this->getServerSettings($serverId))
            $this->createServerConnection();		

        if($this->pusherManager) { 		
			
			$this->layout = '//layouts/pusher';
			$this->render('view/_server-dashboard', array('server' => $this->defaultServer,
                                                        'channel' => 'npush',
                                                        'room' => 'system/stats',
                                                        'userName' => 'root',
                                                        'userId' => '-995'
                                                        ));
		}
    }
	
	public function actionGetPreviewForServers()
	{
		$this->renderPartial('ajax/_servers', array(
									'servers' => PusherManagerServer::model()->findAll()
								));
	}
	
	public function actionGetPreviewForCollectionActivity($serverId, $clientId, $appId, $apiId, $name, $limit)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
                
		if($this->pusherManager) {
			
            $search = array();
       
            if($name != '') {
                
                $search = array('$or' => array(
												array('user_id' => array('$regex' => $name)), 
												array('date_string' => array('$regex' => $name)),
												//array('rooms.$.name' => array('$regex' => $name)),
												//array('rooms.date' => $name),
											));
            } 				
			
			$data = $this->pusherManager->getClientAppAPIActivity($apiId, $search, array(), (int)$limit);
			$this->renderPartial('ajax/_collection_activity', array('data' => $data, 'apiId' => $apiId));
		}
	}
	
	public function actionGetPreviewForCollectionMessageReceipt($serverId, $clientId, $appId, $apiId, $msgId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
                
		if($this->pusherManager) {
						
			$data = $this->pusherManager->getClientAppAPIMessageReceipt($apiId, $msgId);
			$this->renderPartial('ajax/_collection_message_receipt', array('data' => $data));
		}		
	}
	
	public function actionGetPreviewForCollectionMessage($serverId, $clientId, $appId, $apiId, $name, $limit)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
                
		if($this->pusherManager) {
			
            $search = array();
       
            if($name != '') {
                
                $search = array('$or' => array(
												array('room' => array('$regex' => $name)), 
												array('date_string' => array('$regex' => $name)), 
												array('user.name' => array('$regex' => $name)),
												array('user.id' => array('$regex' => $name)),
												array('message.object' => array('$regex' => $name)),
												array('message.data' => array('$regex' => $name)),
											));
            } 			
			
			$data = $this->pusherManager->getClientAppAPIMessages($apiId, $search, array("date" => 0), (int)$limit, array());
			$this->renderPartial('ajax/_collection_message', array('data' => $data, 'apiId' => $apiId));
		}
	}
	
	public function actionGetPreviewForCollectionSummary($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();
                
		if($this->pusherManager) {
			
			$this->renderPartial('ajax/_collection_summary', array(
										'message' => $this->pusherManager->getAPICollectionsMessageInfo($clientId, $appId, $apiId),
										'activity' => $this->pusherManager->getAPICollectionsActivityInfo($clientId, $appId, $apiId)
										));
		}
	}
	
	/*
	* APIs Enable / Disable
	*/

	public function actionAPIEnabled($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();

		if($this->pusherManager) {            
			$this->pusherManager->setAPIKeyEnabled($clientId, $appId, $apiId);
		}
	}

	public function actionAPIDisabled($serverId, $clientId, $appId, $apiId)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();

		if($this->pusherManager) {      
			
			echo $this->pusherManager->setAPIKeyDisabled($clientId, $appId, $apiId);
		}            
	}        
		
	/*
	 * Default Server Connection
	 */
	
	public function actionSetServerDefault($serverId)
	{
		$reset = PusherManagerServer::model()->updateAll(array('default' => '0'));
		$set = PusherManagerServer::model()->updateByPk($serverId, array('default' => '1'));
		
		if($set) {
			$this->respondHTMLStatus('OK');
		} else {
			$this->respondHTMLStatus('ERROR');
		}
	}
	
	/*
	 * Servers connection
	 */
	
	public function actionDeleteServerConnection($serverId)
	{
		$delete = PusherManagerServer::model()->deleteByPk($serverId);
		
		if($delete) {
			$this->respondHTMLStatus('OK');
		} else {
			$this->respondHTMLStatus('ERROR');
		}		
	}
	
	/*
	 * Servers Tools
	 */
	
	public function actionPusherGetStatus($serverId, $apiId) 
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();

		if($this->pusherManager) { 
			
			$push = new NPush();
				
			$options = array(
								'api' => $apiId,
								'url' => $this->defaultServer['push_address'],
								'port' => $this->defaultServer['push_port'],
								'user_id' => '9999',
								'user_name' => 'service',
								'room' => 'service',
								'timeout' => 2
							);

			$curl = $push->trigger('service', 'message', 'Service Initial Authorisation', $options);
			$this->renderPartial('ajax/_curl', array('respond' => $curl));
		}
	}
	
	/*
	 * Servers Tools
	 */
	
	public function actionPusherBroadcastMessage($serverId, $apiId, $message) 
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();

		if($this->pusherManager) { 
			
			$push = new NPush();

			$options = array(
								'api' => $apiId,
								'url' => $this->defaultServer['push_address'],
								'port' => $this->defaultServer['push_port'],
								'user_id' => '9999',
								'user_name' => 'service',
								'channel' => 'service',
								'room' => 'service',
								'timeout' => time()
							);
			
			$curl = $push->triggerToAPI('message', $message, $options);			
			$this->respondHTMLStatus('OK');
		}
	}	
	
	public function actionPusherServerPing($serverId) 
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();

		if($this->pusherManager) { 
			
			$ip = $this->defaultServer['push_address'];
			exec("ping -c 4 $ip 2>&1", $output, $retval);			
			$this->renderPartial('ajax/_ping_push_server', array('respond' => $output, 'server' => $this->defaultServer['push_address']));
		}
	}
	
	/*
	* Resources
	* - Pusher library with Server and API Key settings
	*/
	
	public function actionPusherSetLibraryWithSettings($serverId, $clientId, $appId, $apiId, $type)
	{
		if($this->defaultServer = $this->getServerSettings($serverId))
			$this->createServerConnection();		
		
		if($this->pusherManager) { 
			
			if($type == 'full') {
				$file = $this->renderPartial('resources/v2-full', array('server' => $this->defaultServer, 'apiId' => $apiId), true);
			} else {
				$file = $this->renderPartial('resources/v2-min', array('server' => $this->defaultServer, 'apiId' => $apiId), true);
			}

			header('Content-Type: application/octet-stream');			

			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Length: '.strlen($file));
			header('Content-Disposition: inline; filename="npush.'.($type == 'min' ? 'min.' : '').'js";');

			echo $file; exit;
	
		} else {
			
			$this->respondHTMLStatus('ERROR');
		}
	}
	
	/*
	* THIS FUNCTION ALLOW TO DOWNLOAD READY TO USE LIBRARY WITH NPUSH SETTINGS
	* INSIDE THE LIBRARY.FUNCTION GENERATE NEW APIKEY, ACTIVATE APIKEY AND SENT 
	* READY TO USE FILE TO THE CLIENT
	*/	
	
	public function actionSetPublicServer()
	{
		// connect to server
		$this->defaultServer = (object)array(
					'mongo_address'		=> $this->publicServerMongoAddress,
					'mongo_port'		=> $this->publicServerMongoPort,
					'mongo_username'	=> $this->publicServerMongoUsername,
					'mongo_password'	=> $this->publicServerMongoPassword,
					'mongo_dbname'		=> $this->publicServerMongoDbName,
					'push_address'		=> $this->publicServerNPushAddress,
					'push_port'			=> $this->publicServerNPushPort,
					'push_version'		=> $this->publicServerNPushVersion,
				);		
	}
	
	public function actionCompileLibraryGetAPI()
	{
		// set pbulic server
		$this->actionSetPublicServer();
		
		// connect to db
		$this->createServerConnection();
		
		// generate new API		
		if($this->pusherManager) {
			
			$apiId = new MongoId(uniqid());
		
			if($this->pusherManager->setCreateAPIKeyEnabled('_public_user', '_public_user', (string)$apiId)) {
				
				echo $_GET['callback']."(".json_encode($apiId).");";				
			} else {
				$this->respondHTMLStatus('ERROR');
				mail($this->publicAPIOnErrorEmailTo, 
						'n:push new API Error', 
						'Problem with n:push library download - actionCompileLibraryGetAPI');
			}			
			
		} else { 
			
			$this->respondHTMLStatus('ERROR');
			mail($this->publicAPIOnErrorEmailTo, 
					'n:push new API Error', 
					'Problem with n:push library download - actionCompileLibraryGetAPI');
		}
	}
	
	public function actionCompileLibrary($apiId, $type)
	{		
		// set pbulic server
		$this->actionSetPublicServer();
		
		// connect to db
		$this->createServerConnection();		
		
		// add settings to file and send to client browser
		if($this->pusherManager) { 
			
			if($type == 'full') {
				$file = $this->renderPartial('resources/v2-full', array('server' => $this->defaultServer, 'apiId' => $apiId), true);
			} else {
				$file = $this->renderPartial('resources/v2-min', array('server' => $this->defaultServer, 'apiId' => $apiId), true);
			}

			header('Content-Type: application/octet-stream');			

			header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Length: '.strlen($file));
			header('Content-Disposition: inline; filename="npush.'.($type == 'min' ? 'min.' : '').'js";');

			echo $file; exit;
	
		} else {
			
			mail($this->publicAPIOnErrorEmailTo, 
					'n:push new API Error', 
					'Problem with n:push library download - actionCompileLibrary');
		}		
	}


   /*
    * Chat Application
    */

	public function actionChat($serverId, $api, $channel, $room, $userName, $userId)
	{
        if($this->defaultServer = $this->getServerSettings($serverId))
            $this->createServerConnection();		

        if($this->pusherManager) {             

                    $this->layout = '//layouts/pusher';
                    $this->render('view/_chat', array('server' => $this->defaultServer,
                                                        'apiId' => $api,
                                                        'channel' => $channel,
                                                        'room' => $room,
                                                        'userName' => $userName,
                                                        'userId' => $userId
                                                        ));
        }
	}
	
	/*
    * Chat Application
    */

	public function actionChatActivity($serverId, $api)
	{
        if($this->defaultServer = $this->getServerSettings($serverId))
            $this->createServerConnection();		

        if($this->pusherManager) {             

                    $this->layout = '//layouts/pusher';
                    $this->render('view/_chat-stats', array('server' => $this->defaultServer,
                                                        'apiId' => $api,
                                                        'channel' => 'npush',
                                                        'roomStats' => 'api/stats',
														'roomUsersApp' => 'api/users',
                                                        'userName' => 'npush-monitor',
                                                        'userId' => '-999'
                                                        ));
        }
	}    
    
	/*
    * Chat Watcher
    */

	public function actionChatWatcher($serverId, $api)
	{
        if($this->defaultServer = $this->getServerSettings($serverId))
            $this->createServerConnection();		

        if($this->pusherManager) {             

                    $this->layout = '//layouts/pusher';
                    $this->render('view/_chat-watcher', array('server' => $this->defaultServer,
                                                        'apiId' => $api,
                                                        'channel' => 'service',
                                                        'room' => 'watcher',
                                                        'userName' => 'watcher',
                                                        'userId' => '-998'
                                                        ));
        }
	}   
    
	/*
    * Chat Watcher
    */

	public function actionChatStress($serverId, $api)
	{
        if($this->defaultServer = $this->getServerSettings($serverId))
            $this->createServerConnection();		

        if($this->pusherManager) {             

                    $this->layout = '//layouts/pusher';
                    $this->render('view/_chat-stress', array('server' => $this->defaultServer,
                                                        'apiId' => $api,
                                                        'channel' => 'service',
                                                        'room' => 'stress',
                                                        'userName' => 'stress',
                                                        'userId' => '-997'
                                                        ));
        }
	}    
    
	/*
	 * CHARTS
	 */
	
	private function getChart($graph, $apiId)
	{
		$data = array();
		
		if(!$this->messagesCollectionData)		
			$this->messagesCollectionData = $this->pusherManager->getClientAppAPIMessages($apiId, array("date" => array('$gt' => (double)strtotime('1 minute ago')*1000)), array("date" => 1), false, array('date', 'user'));
		
		if(!$this->activityCollectionData)		
			$this->activityCollectionData = $this->pusherManager->getClientAppAPIActivity($apiId, array("date" => array('$gt' => (double)strtotime('1 minute ago')*1000)), array("date" => 1), false, array('date', 'user_id'));		
		
		switch($graph)
		{
			case 'MESSAGES_LAST_7_DAYS':
				//$messages = $this->pusherManager->getClientAppAPIMessages($apiId, array("date" => array('$gt' => (double)strtotime('1 minute ago')*1000)), array("date" => 1), false, array('date'));

				// chart - messages in last 7 days

				$output = array(
										date("j", strtotime("7 day ago")) => 0, 
										date("j", strtotime("6 day ago")) => 0, 
										date("j", strtotime("5 day ago")) => 0, 						
										date("j", strtotime("4 day ago")) => 0, 
										date("j", strtotime("3 day ago")) => 0, 						
										date("j", strtotime("2 day ago")) => 0, 
										date("j", strtotime("1 day ago")) => 0, 
										date("j", time()) => 0);

				foreach($this->messagesCollectionData as $m) 
				{
					$day = date("j", $m['date']/1000);
					if(!isset($output[$day])) $output[$day] = 0;
					$output[$day]++;
				}
				
				$data['categories'] = json_encode(array_keys($output));
				$data['data'] = json_encode(array(array('Name' => 'Messages', 'data' => array_values($output))));				
				
			break;
			case 'USERS_LAST_7_DAYS':
				//$messages = $this->pusherManager->getClientAppAPIActivity($apiId, array("date" => array('$gt' => (double)strtotime('1 minute ago')*1000)), array("date" => 1), false, array('date', 'user'));

				// chart - messages in last 7 days
	
				$output = array(
										date("j", strtotime("7 day ago")) => 0, 
										date("j", strtotime("6 day ago")) => 0, 
										date("j", strtotime("5 day ago")) => 0, 						
										date("j", strtotime("4 day ago")) => 0, 
										date("j", strtotime("3 day ago")) => 0, 						
										date("j", strtotime("2 day ago")) => 0, 
										date("j", strtotime("1 day ago")) => 0, 
										date("j", time()) => 0);

				foreach($this->activityCollectionData as $m) {
					
					$day = date("j", $m['date']/1000);
					$user = $m['user_id'];
					
					if(!is_array($output[$day])) $output[$day] = array();
						
					$output[$day][$user] = 1;
				}
				
				foreach($output as $k=>$d) {
					
					if(is_array($d)) {
						$output[$k] = count($d);
					} else {
						$output[$k] = 0;
					}
				}
				
				$data['categories'] = json_encode(array_keys($output));
				$data['data'] = json_encode(array(array('Name' => 'Users', 'color' => 'red', 'data' => array_values($output))));
			break;			
			case 'USERS_MESSAGES_LAST_7_DAYS':
				//$messages = $this->pusherManager->getClientAppAPIMessages($apiId, array("date" => array('$gt' => (double)strtotime('7 minute ago')*1000)), array("date" => 1), false, array('date', 'user'));

				// chart - messages in last 7 days

				$outputUsers = array(
										date("j", strtotime("7 day ago")) => 0, 
										date("j", strtotime("6 day ago")) => 0, 
										date("j", strtotime("5 day ago")) => 0, 						
										date("j", strtotime("4 day ago")) => 0, 
										date("j", strtotime("3 day ago")) => 0, 						
										date("j", strtotime("2 day ago")) => 0, 
										date("j", strtotime("1 day ago")) => 0, 
										date("j", time()) => 0);				
				
				$outputMessages = $outputUsers;
				
				foreach($this->messagesCollectionData as $m) {
					
					$day = date("j", $m['date']/1000);
					$user = $m['user']['id'];
					
					if(!is_array($outputUsers[$day])) $outputUsers[$day] = array();
					if(!isset($outputMessages[$day])) $outputMessages[$day] = 0;
						
					$outputUsers[$day][$user] = 1;
					$outputMessages[$day]++;
				}

				foreach($outputUsers as $k=>$d) {
					
					if(is_array($d)) {
						$outputUsers[$k] = count($d);
					} else {
						$outputUsers[$k] = 0;
					}
				}	
				
				$data['categories'] = json_encode(array_keys($outputUsers));
				$data['data'] = json_encode(array(
												array('name' => 'Users', 'color' => 'red', 'data' => array_values($outputUsers)),
												array('name' => 'Messages', 'data' => array_values($outputMessages)),
											));				
				
			break;
		}
		
		return $data;
	}
	
	/**
	 * Generate HTML respond object for JS $.get and $.post
	 * @param string $status, string $data, $id
	 * echo string
	 */		
	
	public function respondHTMLStatus($status = 'OK', $data = '[]', $id = '0')
	{
		echo '<div style="display:none;">';
		echo '<div id="respondStatus">' .$status. '</div>';
		echo '<div id="respondData">' .$data. '</div>';
		echo '<div id="respondId">' .$id. '</div>';
		echo '</div>';
	}		
}