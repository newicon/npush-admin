<?php
/**
 * Description of PusherManagerClient
 *
 * @author krzyztofstasiak
 */

class PusherManagerAPIKey extends CFormModel {
	
	public $name;
 
	public function attributeLabels() {
        return array(
			'name' => 'Name',
        );
    }	
	
    public function rules()
    {
        return array(
            array('name', 'required'),
        );
    }
}