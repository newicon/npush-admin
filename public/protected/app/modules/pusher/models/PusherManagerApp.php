<?php
/**
 * Description of PusherManagerClient
 *
 * @author krzyztofstasiak
 */

class PusherManagerApp extends CFormModel {
	
	public $name;
    public $application_id;
	public $dscr;
 
	public function attributeLabels() {
        return array(
			'name' => 'Name',
			'application_id' => 'Identifier',
			'dscr' => 'Description',
        );
    }	
	
    public function rules()
    {
        return array(
            array('name, application_id', 'required'),
            array('dscr', 'safe'),
        );
    }
}