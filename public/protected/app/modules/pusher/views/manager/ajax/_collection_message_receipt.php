<?php
/*
 * Display records from Message Read Collection for API KEY
 */
?>

<?php if($data->count()) { ?>	
	<?php foreach($data as $d) { ?>
		<strong>Delivered to sockets/users: <?php echo count($d); ?></strong><br>
		<?php foreach($d['users'] as $k=>$u) { ?>
			<?php echo $k.' -> '.date("H:i:s", $u['date']/1000).'/'.$u['date'].';&nbsp;&nbsp;'; ?>
		<?php } ?>	
	<?php } ?>
<?php } else { ?>
    <strong><i class="icon-warning-sign"></i>&nbsp;no delivery receipt found...</strong>
<?php } ?>