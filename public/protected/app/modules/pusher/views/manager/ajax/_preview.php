
<div class="navbar-inner h30" style="border-left:0px; border-right:0px; border-top-left-radius: 0px; border-top-right-radius: 0px; line-height: 20px; background-color: #eee;" id="RBPagePropertiesTabs">
	<ul class="nav nav-pills" style="width:455px; margin:8px auto;">
		<li class="active"><a href="#ModulePreviewListTabGeneral" data-toggle="tab" graph-view="1">Dashboard</a></li>
		<li><a href="#ModulePreviewListTabTest" data-toggle="tab">Test tools</a></li>		
		<li style=""><a href="#ModulePreviewListTabSettings" data-toggle="tab">Settings</a></li>
		<li class="hide"><a href="#ModulePreviewListTabStat" data-toggle="tab">Stats</a></li>
		<li><a href="#ModulePreviewListTabChat" data-toggle="tab">Debug</a></li>		
		<li style=""><a href="#ModulePreviewListTabData" data-toggle="tab">Data</a></li>		
		<li><a href="#ModulePreviewListTabChart" data-toggle="tab" graph-view="1">Charts</a></li>
	</ul>
</div>     
<div class="tab-content" id="ModulePreviewListTabs" style="-webkit-transform: transform3d(0, 0, 0); margin:0px; padding: 0px;">
	<div class="tab-pane active" id="ModulePreviewListTabGeneral" style="margin:0px; padding: 0px;">
		<div style="height:100%; padding:0px 10px 0px 10px;">
			<div class="row-fluid" id="ModulePreviewListTabGeneralTop" style="-webkit-overflow-scrolling: touch; -webkit-transform: transform3d(0, 0, 0); overflow:hidden; padding: 20px; margin-bottom: 20px; width:auto;">
                <div id="ModulePreviewListTabGeneralTopChart" style="-webkit-transform: transform3d(0, 0, 0); width:100%;height:100%;position:relative;">
                    
                </div>
                <div style="position:relative;top:-100%;width:50%;float:right; text-align: right;">
                    <span style="margin-bottom:0px;" class="hide label label-<?php echo $apiStatus['enabled'] == 'true' ? "success" : "important"; ?>" id="ModulePreviewListTabGeneralTop_nistatus"><?php echo $apiStatus['enabled'] == 'true' ? "API enabled" : "API disabled"; ?></span>
                    <p style="margin-bottom:2px;"><span class="label label-important" id="ModulePreviewListTabGeneralTop_curlrequest">Authorization...</span></p>
                    <p style="margin-bottom:2px;"><span class="<?php echo $apiStatus['enabled'] == 'true' ? "" : "hide"; ?> label label-warning" id="ModulePreviewListTabGeneralTop_activesocket">Counting sockets...</span></p>
                </div>
                <div style="position:relative;top:-100%;width:50%;float:left;">
                    <p style="margin-bottom:2px;"><span class="<?php echo $apiStatus['enabled'] == 'true' ? "" : "hide"; ?> label" id="ModulePreviewListTabGeneralTop_msgperhour">--- msg / h</span></p>
                    <p style="margin-bottom:2px;"><span class="<?php echo $apiStatus['enabled'] == 'true' ? "" : "hide"; ?> label" id="ModulePreviewListTabGeneralTop_msgperminute">--- msg / m</span></p>
                </div>                
                <div class="hide">
                    <div class="row-fluid">
                        <div class="span3">
                            API Key:
                        </div>
                        <div class="span8">
                            <?php echo $api; ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">							
                            Last activity:
                        </div>
                        <div class="span8">
                            <?php echo $lastActivity > - 1 ? date("h:i:s A,  l jS \of F Y", $lastActivity/1000) : 'N/A'; ?>
                        </div>								
                    </div>			
                    <div class="row-fluid">
                        <div class="span3">								
                            Messages collection:
                        </div>
                        <div class="span8">
                            <?php echo isset($msgStorageInfo['count']) ? $msgStorageInfo['count'] : '0'; ?> messages, <?php echo isset($msgStorageInfo['size']) ? $msgStorageInfo['size'] : '0'; ?> Bytes
                        </div>								
                    </div>
                    <div class="row-fluid">
                        <div class="span3">								
                            Activity collection:
                        </div>
                        <div class="span8">
                            <?php echo isset($actStorageInfo['count']) ? $actStorageInfo['count'] : '0'; ?> users, <?php echo isset($actStorageInfo['size']) ? $actStorageInfo['size'] : '0'; ?> Bytes
                        </div>								
                    </div>	
                </div>
                <div id="ModulePreviewListTabGeneralTopChartToolbar" class="hide" style="position:relative;top:-50px;width:50%;text-align:right;float:right;">
                    <div class="btn-group">
                        <a href="#" class="btn btn-small" code="+"><i class="icon-plus"></i></a>
                        <a href="#" class="btn btn-small" code="-"><i class="icon-minus"></i></a>
                    </div>
                </div>                                
                <div id="ModulePreviewListTabGeneralTopDashboardBottom" style="-webkit-transform: transform3d(0, 0, 0); position:relative;top:-109px;width:50%;text-align:left;float:left;">
                    <div id="ModulePreviewListTabGeneralTopChartPeaks" style="width:200px; height:110px;">
                        <!--- last 60 second message count -->
                    </div>                         
                </div>
			</div>
			<div class="row-fluid" id="ModulePreviewListTabGeneralSettings" style="text-align:center; height:55px; width:98.5%; margin:0px 10px 0px 10px; overflow:hidden; box-shadow: 1px 1px 5px 1px #eee; border-radius:10px 10px 0px 0px;">
				<div class="span12" style="padding-top:11px;">
					<div class="btn-group" data-toggle="buttons-radio">
						<a href="#" id="ModulePreviewListTabGeneralAPIEnabled" class="btn <?php echo $apiStatus['enabled'] == 'true' ? "btn-success active" : ""; ?>">API Enabled</a>	
						<a href="#" id="ModulePreviewListTabGeneralAPIDisabled" class="btn <?php echo $apiStatus['enabled'] == 'false' ? "btn-danger active" : ""; ?>">API Disabled</a>
					</div>								
				</div>
			</div>							
			<div class="row-fluid" id="ModulePreviewListTabGeneralSender" style="text-align:center; height:85px; width:98.5%; margin:0px 10px 0px 10px; overflow:hidden; box-shadow: 1px 1px 5px 1px #eee; border-radius:0px;">
				<h5>Broadcast message</h5>
				<div class="input-append">
					<input type="text" class="span2 search-query" placeholder='e.g. Hello world! or  { "a" : "123", "b" : "456" }' id="ModulePreviewListTabGeneralInputText" style="width:50%; border-top-right-radius: 0px; border-bottom-right-radius: 0px;">
					<button type="submit" class="btn" id="ModulePreviewListTabGeneralButtonSendText">Send as text</button>
					<button type="submit" class="btn" id="ModulePreviewListTabGeneralButtonSendJSON">as JSON</button>
				</div>
			</div>							
            <div class="hide" id="ModulePreviewListTabGeneralChart_data">
				<div class="chart-item-title">Messages in last 7 days</div>
				<div class="chart-item-categories"><?php echo $messagesByDay['categories']; ?></div>
				<div class="chart-item-data"><?php echo $messagesByDay['data']; ?></div>				                
            </div>
			<div class="item-chart hide" id="ModulePreviewListTabGeneralChart" style="bottom:0px; height:180px; width:98.5%; margin:0px 10px 0px 10px;  margin:auto; overflow:hidden; box-shadow: 1px 1px 5px 1px #eee; border-radius:0px 0px 10px 10px;">
                <!--- CONTENT FROM HIGHCHARTS --->
			</div>					
		</div>
	</div>
	<div class="tab-pane" id="ModulePreviewListTabChat">
			<div class="navbar-inner" style="border-top-left-radius: 0px; border-top-right-radius: 0px; margin:0px 10px 0px 10px; border-top: 0px; line-height: 20px; height:30px;" id="ModulePreviewListTabChatTop">
				<ul class="nav nav-pills" style="width:510px; margin:9px auto;" id="ModulePreviewListTabChatTopTabs">
					<li><a href="#ModulePreviewListTabChatUserAlpha" iframe-id="1" data-toggle="tab"><span style="font-size:9px; line-height:7px;">user</span>&nbsp;Alpha</a></li>
					<li style=""><a href="#ModulePreviewListTabChatUserBeta" iframe-id="2" data-toggle="tab"><span style="font-size:9px; line-height:7px;">user</span>&nbsp;Beta</a></li>
					<li style=""><a href="#ModulePreviewListTabChatUserGamma" iframe-id="3" data-toggle="tab"><span style="font-size:9px; line-height:7px;">user</span>&nbsp;Gamma</a></li>
                    <li class="active" style="margin-left:30px;"><a href="#ModulePreviewListTabChatUserStats" iframe-id="4" data-toggle="tab">Activity</a></li>
					<li><a href="#ModulePreviewListTabChatUserWatcher" iframe-id="5" data-toggle="tab">Watcher</a></li>
                    <li><a href="#ModulePreviewListTabChatUserStress" iframe-id="6" data-toggle="tab">Stress</a></li>
				</ul>
			</div>							
			<div id="ModulePreviewListTabChatContainer" style="overflow:hidden;">
				<div class="tab-content" style="height:99%; overflow:hidden; border:0px solid black;">
					<div class="tab-pane" id="ModulePreviewListTabChatUserAlpha" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow1" src="<?php echo Yii::app()->createUrl('/pusher/manager/chat/serverId/'.$server->id.'/api/'.$api.'/channel/service/room/chat/userName/alpha/userId/-1001'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>
					<div class="tab-pane" id="ModulePreviewListTabChatUserBeta" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow2" src="<?php echo Yii::app()->createUrl('/pusher/manager/chat/serverId/'.$server->id.'/api/'.$api.'/channel/service/room/chat/userName/beta/userId/-1002'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>
					<div class="tab-pane" id="ModulePreviewListTabChatUserGamma" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow3" src="<?php echo Yii::app()->createUrl('/pusher/manager/chat/serverId/'.$server->id.'/api/'.$api.'/channel/service/room/chat/userName/gamma/userId/-1003'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>                    
					<div class="tab-pane active" id="ModulePreviewListTabChatUserStats" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow4" src="<?php echo Yii::app()->createUrl('/pusher/manager/chatActivity/serverId/'.$server->id.'/api/'.$api.'/'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>     
					<div class="tab-pane" id="ModulePreviewListTabChatUserWatcher" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow4" src="<?php echo Yii::app()->createUrl('/pusher/manager/chatWatcher/serverId/'.$server->id.'/api/'.$api.'/'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>   					
					<div class="tab-pane" id="ModulePreviewListTabChatUserStress" style="width:100%; height:100%;">
						<iframe id="ModulePreviewListTabChatWindow5" src="<?php echo Yii::app()->createUrl('/pusher/manager/chatStress/serverId/'.$server->id.'/api/'.$api.'/'); ?>" style="width:99%; height:100%;" frameborder="0"></iframe>
					</div>   			                    
				</div>
			</div>
			<div class="navbar-inner" id="ModulePreviewListTabChatBottom" style="border-bottom:0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin:0px 10px 0px 10px; background-color: #eee; height:45px; text-align:center;">						
				<div class="row-fluid">	
					<div class="span12" style="text-align:center;">
						<div class="crm-item crm-text-classic h30" style=" line-height: 20px; margin-top:3px;">
							<div class="input-append">
								<input type="text" class="search-query" id="ModulePreviewListTabChatInputMessage" placeholder="write your message here" style=" border-top-right-radius: 0px; border-bottom-right-radius: 0px; width:50%;">
								<button type="submit" class="btn btn-success hide" id="ModulePreviewListTabChatButtonSend">Send room</button>
								<button type="submit" class="btn btn-success hide" id="ModulePreviewListTabChatButtonSendChannel">Channel</button>
                                <button type="submit" class="btn btn-success" id="ModulePreviewListTabChatButtonSendApp">App</button>
							</div>
						</div>			
					</div>
				</div>
			</div>
	</div>					
	<div class="tab-pane" id="ModulePreviewListTabSettings" style="-webkit-overflow-scrolling: touch; ">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6">
				<textarea style="width:100%; margin-top:40px; height:160px;" id="ModulePreviewListTabSettingsTextarea">{&#13;&#10;&#9;apiKey: '<?php echo $api; ?>',&#13;&#10;&#9;url: '<?php echo $server['push_address'] ?>',&#13;&#10;&#9;port: '<?php echo $server['push_port'] ?>',&#13;&#10;&#9;user_id: '1234',&#13;&#10;&#9user_name: 'test',&#13;&#10;&#9api_version: '<?php echo $server['push_version'] ?>'&#13;&#10;}</textarea>
			</div>
			<div class="span3">
				<a href="#" class="btn btn-success" id="ModulePreviewListTabSettingsButtonCopy" style="width:80px; margin-top:45px;">Copy JSON settings to clipboard</a>
			</div>
		</div>
		<div class="row-fluid">	
			<div class="span2"></div>
			<div class="span8" style="text-align:center; margin-top: 30px;">						
				<a href="#" class="btn btn-inverse" id="ModulePreviewListTabSettingsButtonDownloadLibary">Download npush.js library with settings (~100k)</a>
			</div>
			<div class="span2"></div>							
		</div>		
		<div class="row-fluid">	
			<div class="span2"></div>
			<div class="span8" style="text-align:center; margin-top: 10px;">						
				<a href="#" class="btn btn-inverse" id="ModulePreviewListTabSettingsButtonDownloadLibaryMin">Download npush.min.js library with settings (~50k)</a>
			</div>
			<div class="span2"></div>							
		</div>	
		<div class="row-fluid" style="margin-top:20px;">	
			<div class="span2"></div>
			<div class="span8">
				<h4>Basic example</h4>
			</div>
			<div class="span2"></div>
		</div>		
		<div class="row-fluid" style="margin-top:10px;">	
			<div class="span2"></div>
			<div class="span8">
				<h5>Step 1</h5>
			</div>
			<div class="span2"></div>
		</div>			
		<div class="row-fluid">	
			<div class="span2"></div>
			<div class="span8">
				<pre>&#60;script src="npush.js" type="text/javascript">&#60;/script></pre>
			</div>
			<div class="span2"></div>
		</div>		
		<div class="row-fluid" style="margin-top:10px;">	
			<div class="span2"></div>
			<div class="span8">
				<h5>Step 2</h5>
			</div>
			<div class="span2"></div>
		</div>				
		<div class="row-fluid">	
			<div class="span2"></div>
			<div class="span8">
<pre>
&lt;script&gt;
	mychannel = npush.subscribe('mychannel');
	mychannel.on('message', function(message) { alert(message); });
	mychannel.trigger('message', 'Hello world!');
&lt;/script&gt;
</pre>
			</div>
			<div class="span2"></div>
		</div>			
	</div>				
	<div class="tab-pane" id="ModulePreviewListTabData">
		<div class="navbar-inner" style="border-top-left-radius: 0px; border-top-right-radius: 0px; margin:0px 10px 0px 10px; border-top: 0px; line-height: 20px; height:30px;" id="ModulePreviewListTabDataTop">
			<ul class="nav nav-pills" style="width:285px; margin:9px auto;">
				<li class=""><a href="#ModulePreviewListTabDataContainerActivity" data-view="activity" data-toggle="tab">Users activity</a></li>
				<li class="active"><a href="#ModulePreviewListTabDataContainerMessages" data-view="message" data-toggle="tab">Messages</a></li>
				<li ><a href="#ModulePreviewListTabDataContainerSummary" data-view="summary" data-toggle="tab">Summary</a></li>
			</ul>
		</div>					
		<div id="ModulePreviewListTabDataContainer" style="overflow:hidden;">
			<div class="tab-content" style="overflow:hidden; border:0px solid black;">
				<div class="tab-pane" id="ModulePreviewListTabDataContainerActivity" style="-webkit-overflow-scrolling: touch; width:100%; overflow: auto; padding-top: 10px;">
					<!--- DATA FROM AJAX REQUEST -->
				</div>	
				<div class="tab-pane active" id="ModulePreviewListTabDataContainerMessages" style="-webkit-overflow-scrolling: touch; width:100%; overflow: auto; padding-top: 10px;">
					<!--- DATA FROM AJAX REQUEST -->
				</div>			
				<div class="tab-pane" id="ModulePreviewListTabDataContainerSummary" style="-webkit-overflow-scrolling: touch; width:100%; overflow: auto; padding-top: 10px;">
					<!--- DATA FROM AJAX REQUEST -->
				</div>					
			</div>
		</div>
		<div class="navbar-inner" id="ModulePreviewListTabDataBottom" style="border-bottom:0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin:0px 10px 0px 10px; background-color: #eee; height:45px; text-align:center;">						
			<div class="row-fluid">	
				<div class="span4" style="text-align:left;">
					<div class="crm-item crm-text-classic h30" style=" line-height: 20px; margin-top:3px;">
						<input type="text" class="search-query" id="ModulePreviewListTabDataInputSearch" placeholder="Search by text" style="width:85%;">
					</div>			
				</div>
				<div class="span5" style="text-align:right; margin-top:8px; text-align: center;">
					<div class="btn-group" data-toggle="buttons-radio" style="">
						<a href="#" class="btn active btn-primary" docs-on-page="10">limit to 10</a>
						<a href="#" class="btn" docs-on-page="25">25</a>
						<a href="#" class="btn" docs-on-page="50">50</a>
						<a href="#" class="btn" docs-on-page="100">100</a>
						<a href="#" class="btn" docs-on-page="500">500</a>
						<a href="#" class="btn" docs-on-page="1000">1000</a>
					</div>
				</div>
				<div class="span3" style="text-align:right;">
					<a href="#" class="btn btn-danger" id="ModulePreviewListTabDataButtonClear" style="margin-top:8px;">Clear data</a>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="ModulePreviewListTabTest" style="-webkit-overflow-scrolling: touch;">	
		<div class="row-fluid">
			<div class="span2"></div>
			<div class="span8">
				<textarea style="width:100%; margin-top:40px; height:100px;" id="ModulePreviewListTabTestTextarea">curl -H "Content-Type: application/post" -d '{"event":"message", "data":"helloworld"}' "http://<?php echo $server['push_address']; ?>:<?php echo $server['push_port']; ?>/api/channel/service/room/service/api/<?php echo $api; ?>/version/2.0/t/<?php echo time(); ?>/suid/9999/suname/service"</textarea>
			</div>
			<div class="span2"></div>
		</div>
		<div class="row-fluid">	
			<div class="span2"></div>
			<div class="span8" style="text-align:center; margin-top: 20px;">						
				<a href="#" class="btn btn-success" id="ModulePreviewListTabTestButtonCopy">Copy CURL Command to clipboard</a>
			</div>
			<div class="span2"></div>							
		</div>
		<div class="row-fluid">
			<div class="span2"></div>
			<div class="span7">
				<textarea style="width:100%; margin-top:40px; height:100px;" id="ModulePreviewListTabTestTextareaRespondCURL" placeholder="press button 'CURL now!' to see server respond..."></textarea>
			</div>
			<div class="span3">
				<a href="#" class="btn btn-success" id="ModulePreviewListTabTestButtonCURL" style="width:80px; margin-top:45px; margin-left:10px;">CURL now!</a>
			</div>
		</div>		
		<div class="row-fluid">
			<div class="span2"></div>
			<div class="span7">
				<textarea style="width:100%; margin-top:40px; height:80px;" id="ModulePreviewListTabTestTextareaRespondPushPing" placeholder="press button 'Ping Server' to see server respond..."></textarea>
			</div>
			<div class="span3">
				<a href="#" class="btn btn-success" id="ModulePreviewListTabTestButtonPingPush" style="width:80px; margin-top:45px; margin-left:10px;">Ping Server</a>
			</div>
		</div>
		<div class="row-fluid">	
			<div class="span12">&nbsp;</div>
		</div>
	</div>
	<div class="tab-pane" id="ModulePreviewListTabChart" style=" -webkit-overflow-scrolling: touch;">	
		<div style="padding:20px 20px 0px 20px;">
            <div class="hide" id="ModulePreviewListTabChartUserMessagesLastWeek_data">
				<div class="chart-item-title">Users/Messages in last 7 days</div>
				<div class="chart-item-categories"><?php echo $usersAndMessagesByDay['categories']; ?></div>
				<div class="chart-item-data"><?php echo $usersAndMessagesByDay['data']; ?></div>                
            </div>
			<div class="item-chart" id="ModulePreviewListTabChartUserMessagesLastWeek" style="height:250px; margin-top:20px; margin:auto;">
                <!--- CONTENT FROM HIGHCHARTS --->
			</div>
		</div>		
		<div style="padding:20px 20px 0px 20px;">
            <div class="hide" id="ModulePreviewListTabChartMessageLastWeek_data">
                <div class="chart-item-title">Messages in last 7 days</div>
                <div class="chart-item-categories"><?php echo $messagesByDay['categories']; ?></div>
                <div class="chart-item-data"><?php echo $messagesByDay['data']; ?></div>				            
            </div>            
			<div class="item-chart" id="ModulePreviewListTabChartMessageLastWeek" style="height:250px; margin-top:20px; margin:auto;">
                <!--- CONTENT FROM HIGHCHARTS --->
			</div>
		</div>
		<div style="padding:20px 20px 0px 20px;">
            <div class="hide" id="ModulePreviewListTabChartUserLastWeek_data">
				<div class="chart-item-title">Users in last 7 days - authorisation only or sender</div>
				<div class="chart-item-categories"><?php echo $usersByDay['categories']; ?></div>
				<div class="chart-item-data"><?php echo $usersByDay['data']; ?></div>                
            </div>
			<div class="item-chart" id="ModulePreviewListTabChartUserLastWeek" style="height:250px; margin-top:20px; margin:auto;">
                <!--- CONTENT FROM HIGHCHARTS --->
			</div>
		</div>
		<div style="padding:20px 20px 0px 20px;"></div>
	</div>
	<div class="tab-pane" id="ModulePreviewListTabStat">	
		<div class="navbar-inner" style="border-top-left-radius: 0px; border-top-right-radius: 0px; margin:0px 10px 0px 10px; border-top: 0px; line-height: 20px; height:30px;" id="ModulePreviewListTabStatTop">
			<div class="row-fluid">
				<div class="span12" style="padding:10px; text-align: center;">
					Sample no. N/A
				</div>
			</div>
		</div>					
		<div id="ModulePreviewListTabStatContainer">
			<div class="row-fluid">	
				<div class="span6" style="padding:50px;">
					<h3>Avail. in FACE 2</h3>
				</div>
			</div>
		</div>
		<div class="navbar-inner" id="ModulePreviewListTabStatBottom" style="border-bottom:0px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin:0px 10px 0px 10px; background-color: #eee; height:45px; text-align:center;">						
			<div class="row-fluid">	
				<div class="span12" style="text-align:center;">
					<div class="span12" style="padding-top:9px;">
						<div class="btn-group" data-toggle="buttons-radio">
							<a href="#" class="btn">On</a>	
							<a href="#" class="btn active">Off</a>
						</div>								
					</div>			
				</div>
			</div>
		</div>					
	</div>				
</div>			