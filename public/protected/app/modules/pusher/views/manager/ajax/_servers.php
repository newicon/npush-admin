<?php if(count($servers)) { ?>
	<?php foreach($servers as $s) { ?>
		<div class="server-item server-item-id-<?php echo $s->id; ?>" dbid="<?php echo $s->id; ?>" style="padding:24px 10px 9px 10px; width:140px; font-size:60px; line-height:40px; float:left; border-radius: 10px;">
			<div style="width:100%; text-align:center;">
				<i class="icon-hdd" style=" width:50px; background-image: none;"></i>
			</div>
			<div style="width:100%; text-align:center; white-space: nowrap; text-overflow:ellipsis; overflow: hidden; font-size:16px; font-weight:bold;">
				<?php echo $s['config_name']; ?>
			</div>
			<div class="hide server-item-settings">
				{ 
					"id" : "<?php echo isset($s) ? $s->id : "0" ?>",
					"name" : "<?php echo isset($s) ? $s->config_name : "0" ?>",
					"m_address" : "<?php echo isset($s) ? $s->mongo_address : "0" ?>",
					"m_port" : "<?php echo isset($s) ? $s->mongo_port : "0" ?>",
					"m_username" : "<?php echo isset($s) ? $s->mongo_username : "0" ?>",
					"m_password" : "<?php echo isset($s) ? $s->mongo_password : "0" ?>",
					"m_dbname" : "<?php echo isset($s) ? $s->mongo_dbname : "0" ?>",
					"p_address" : "<?php echo isset($s) ? $s->push_address : "0" ?>",
					"p_port" : "<?php echo isset($s) ? $s->push_port : "0" ?>",
					"p_version" : "<?php echo isset($s) ? $s->push_version : "0" ?>"								
				}	
			</div>
		</div>
	<?php } ?>
<?php } else { ?>
	<div class="row-fluid" style="height:150px; margin-top: 10px">
		<div class="span12" style="text-align:center;">
			<h4>Servers list is empty...</h4>
		</div>
		<div class="span10" style="text-align:center; margin-top: 20px;">
			<i class="icon-hdd" style="font-size:100px; background-image: none;"></i>                
		</div>
	</div>
<?php } ?>
