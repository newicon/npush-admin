<?php
/*
 * @Author	Krzysztof Stasiak
 * $Dscr	Pusher Manager Main Admin View
 */

/**
 * Single API preview:
 * - blocked graphs - it needs to be rebuid. It is really slow now.
 */

?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/jsoneditor/jsoneditor.css">

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/highcharts/2.3.0/highcharts.js" type="text/javascript" rel="stylesheet" /></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/highcharts/2.3.0/highcharts-more.js" type="text/javascript" rel="stylesheet" /></script>		
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/clipboard/zeroclipboard.min.js" type="text/javascript" rel="stylesheet" /></script>		
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/jsoneditor/jsoneditor.js" type="text/javascript" rel="stylesheet" /></script>		

<object type="application/x-shockwave-flash" data="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/clipboard/ZeroClipboard.swf" width="1" height="1" style="display:none;"></object>
<iframe id="ModuleHeaderIFrame" style="display:none"></iframe>

<?php $this->renderPartial('style/style'); ?>

<div id="ModuleHeader" class="page-header" style="margin-bottom:0px; border:0px solid white; height:40px;">
	<h4>
		<i class="icon-comments-alt"style="font-size:25px; -webkit-transform: rotate(6deg); padding: 4px 14px 0px 0px;"></i><span id="ModuleHeaderTitle"><?php echo (isset($manager) && $manager != false) ? $server->config_name : 'Pusher Clients Manager<span class="label label-important" style="margin-left:10px;"><h5 style="line-height:1px;">NO SERVER CONNECTION</h5></span>' ?></span>
	</h4>
	<div class="action-buttons" style="top:5px; padding-right:7px;">		
        <div class="btn-group" data-toggle="buttons-radio" style="margin-right:10px;">
            <button class="btn" id="ModuleServerModalButtonServerDashboard">Server Dashboard</button>
            <button class="btn btn-inverse active" id="ModuleServerModalButtonClientDashboard">Clients Dashboard</button>
        </div>
        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#ModuleServerModalSwitchServer">Switch server</a>
	</div>
</div>
<div id="ModuleContainer" style="-webkit-transform: transform3d(0, 0, 0);">
<!--- CLIENTS AREA --->
	<div class="crm-content" id="ModuleContainerClient" style="-webkit-transform: transform3d(0, 0, 0); display:block; width:200px; float:left; border:0px solid black; height:100%;">
		<div class="crm-header h40">
			<div class="crm-header-text crm-text-classic" style="margin-left:5px;">
				<strong>Client</strong>
				<a class="btn btn-mini" id="ModuleContainerClientButtonAdd" rel="tooltip" data-placement="left" data-original-title="Add client" style="margin-right:4px; margin-top:2px; padding-top:3px; float:right; line-height:12px; font-size:20px;"><i class="icon-plus-sign"></i></a>								
			</div>
		</div>
		<div class="navbar-inner h30 " style="border-top-left-radius: 0px; border-top-right-radius: 0px; border-left:0px; border-right:0px; line-height: 20px; padding:0px;" type="search">
			<input type="text" id="ModuleContainerClientListInputSearch" class="search-query" placeholder="Search client" style="width:157px; margin:5px 0px 0px 5px;">
		</div>	
		<div style="overflow: auto; -webkit-overflow-scrolling: touch;" id="ModuleContainerClientList">
			<!--- DATA FROM SERVER / LOAD BY AJAX --->
		</div>
	</div>
<!--- APPS AREA --->	
	<div class="crm-content" id="ModuleContainerApps" style="-webkit-transform: transform3d(0, 0, 0); display:none; opacity:0.00; width:1px; float:left; border:0px solid black; height:100%;">
		<div class="crm-header h40">
			<div class="crm-header-text crm-text-classic" style="margin-left:5px;">
				<strong>Application</strong>
				<a class="btn btn-mini" id="ModuleContainerAppButtonAdd" rel="tooltip" data-placement="left" data-original-title="Add App" style="margin-right:4px; margin-top:2px; padding-top:3px; float:right; line-height:12px; font-size:20px;"><i class="icon-plus-sign"></i></a>								
			</div>
		</div>
		<div class="navbar-inner h30" style="border-top-left-radius: 0px; border-top-right-radius: 0px; border-left:0px; border-right:0px; line-height: 20px; padding:0px;" type="search">
			<input type="text" id="ModuleContainerAppsListInputSearch" class="search-query" placeholder="Search app" style="width:157px; margin:5px 0px 0px 5px;">
		</div>
		<div style="overflow: auto; -webkit-overflow-scrolling: touch;" id="ModuleContainerAppsList">
			<!--- DATA FROM SERVER / LOAD BY AJAX --->
		</div>		
	</div>
<!--- APIs KEYS AREA --->
	<div class="crm-content" id="ModuleContainerApi" style="-webkit-transform: transform3d(0, 0, 0); display:none; opacity:0.00; width:1px; float:left; border:0px solid black; height:100%;">
		<div class="crm-header h40">
			<div class="crm-header-text crm-text-classic" style="margin-left:5px;">
				<strong>API Key</strong>
				<a class="btn btn-mini" id="ModuleContainerApiButtonAdd" rel="tooltip" data-placement="left" data-original-title="Add API key" style="margin-right:4px; margin-top:2px; padding-top:3px; float:right; line-height:12px; font-size:20px;"><i class="icon-plus-sign"></i></a>								
			</div>
		</div>
		<div class="navbar-inner h30" style="border-top-left-radius: 0px; border-top-right-radius: 0px; border-left:0px; border-right:0px; line-height: 20px; padding:0px;" type="search">
			<input type="text" id="ModuleContainerApisListInputSearch" class="search-query" placeholder="Search key" style="width:157px; margin:5px 0px 0px 5px;">
		</div>		
		<div style="overflow: auto; -webkit-overflow-scrolling: touch;" id="ModuleContainerApisList">
			<!--- DATA FROM SERVER / LOAD BY AJAX --->
		</div>	
	</div>	
<!--- PREVIEW AREA --->
	<div class="crm-content" id="ModuleContainerPreview" style="float:left; border:0px solid black; height:100%;">		
		<div id="ModulePreviewHeader" class="crm-header h40">
			<div class="crm-header-text crm-text-classic npush-client" style="margin-left:5px;">
				<strong>Preview</strong>
				<a href="#" class="btn btn-inverse" id="ModulePreviewHeaderButtonExpand" rel="tooltip_" data-original-title="Change full/min width" style="float:right;"><i class="icon-resize-full"style="font-size:16px; -webkit-transform: rotate(1deg);"></i></a>				
				<div class="btn-group" style="float:right; margin-right:5px;">
					<a href="#" class="btn btn-inverse" id="ModulePreviewHeaderButtonExpandApis" rel="tooltip_" title="Show APIs column only"><i class="icon-key"style="font-size:16px; -webkit-transform: rotate(1deg);"></i></a>									
					<a href="#" class="btn btn-inverse" id="ModulePreviewHeaderButtonExpandApps" rel="tooltip_" title="Show Apps column only"><i class="icon-qrcode"style="font-size:16px; -webkit-transform: rotate(1deg);"></i></a>									
					<a href="#" class="btn btn-inverse" id="ModulePreviewHeaderButtonExpandClients" rel="tooltip_" title="Show Client column only"><i class="icon-group"style="font-size:16px; -webkit-transform: rotate(1deg);"></i></a>									
				</div>
			</div>
			<div class="crm-header-text crm-text-classic npush-server hide" style="margin-left:5px;margin-right: 10px;">
				<div class="row-fluid" style="padding-right: 10px;">
					<div class="span4">
						
					</div>
					<div class="span4 align-center" id="ServerManagementStatusBar">
						<div class=""></div>
					</div>
					<div class="span4">
					</div>
				</div>
			</div>				
		</div>
		<div id="ModulePreviewList" style=" -webkit-overflow-scrolling: touch; overflow-x: hidden; overflow-y: auto;">	
			<!--- DATA FROM SERVER / LOAD BY AJAX --->	
			<div class="module-preview-area"></div>
		</div>
	</div>
</div>

<?php $this->renderPartial('view/_modals', array('servers' => $servers)); ?>

<script>	
	
	var ModuleUserAction = '';
    var ModulePreviewWidth = 0;

	var ModuleServerDefaultId =	'<?php echo isset($server) ? $server->id : "0" ?>';
	var ModuleServerSelectedConnectionStatus = 0;
	var ModuleServerSelectedSettings =	{ 
		id : <?php echo isset($server) ? $server->id : "0" ?>,
		name : '<?php echo isset($server) ? $server->config_name : "0" ?>',
		m_address : '<?php echo isset($server) ? $server->mongo_address : "0" ?>',
		m_port : '<?php echo isset($server) ? $server->mongo_port : "0" ?>',
		m_username : '<?php echo isset($server) ? $server->mongo_username : "0" ?>',
		m_password : '<?php echo isset($server) ? $server->mongo_password : "0" ?>',
		m_dbname : '<?php echo isset($server) ? $server->mongo_dbname : "0" ?>',
		p_address : '<?php echo isset($server) ? $server->push_address : "0" ?>',
		p_port : '<?php echo isset($server) ? $server->push_port : "0" ?>',
		p_version : '<?php echo isset($server) ? $server->push_version : "0" ?>'									
	};
										
	var ModuleManagerSelectedClientId = 0;
	var ModuleManagerSelectedAppId = '';
	var ModuleManagerSelectedApiId = '';
	
	var ModuleChatUserSelected = 1;
	var ModuleDataManageCollection = 'message';
	var ModuleDataManageDocumentOnPage = 10;

</script>	

<?php $this->renderPartial('js/_common', array()); ?>
<?php $this->renderPartial('js/_views', array()); ?>
<?php $this->renderPartial('js/_graphs', array()); ?>
<?php $this->renderPartial('js/_pusher', array()); ?>
<?php $this->renderPartial('js/_server', array()); ?>
<?php $this->renderPartial('js/_ready', array()); ?>