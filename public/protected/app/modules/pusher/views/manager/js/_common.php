
<script>
	
	function respondServer(_respond, _code)
	{
		var $response=$(_respond);
		return $response.find("#"+_code).html();		
	}	
	
	function roundNumber(num, dec) 
	{
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		return result;
	}
	
	function convertNumberToMB(_number) {
		
		if(String(_number).indexOf('m') > -1) {
			
			return _number.replace("m", "");
		}
		
		return _number;
	}
	
	String.prototype.escapeSpecialChars = function() {
		return this.replace(/\\n/g, "\\n")
				.replace(/\\'/g, "\\'")
				.replace(/\\"/g, '\\"')
				.replace(/\\&/g, "\\&")
				.replace(/\\r/g, "\\r")
				.replace(/\\t/g, "\\t")
				.replace(/\\b/g, "\\b")
				.replace(/\\f/g, "\\f");
	};
	
	function copyToClipboardCrossbrowser(s) {    
		
        s = document.getElementById(s).value;               
		
        if( window.clipboardData && clipboardData.setData )
        {
            clipboardData.setData("Text", s);
        }           
        else
        {
            // You have to sign the code to enable this or allow the action in about:config by changing
            //user_pref("signed.applets.codebase_principal_support", true);
            netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');

            var clip = Components.classes["@mozilla.org/widget/clipboard;1"].createInstance(Components.interfaces.nsIClipboard);
            if (!clip) return;

            // create a transferable

            var trans = Components.classes["@mozilla.org/widget/transferable;1"].createInstance(Components.interfaces.nsITransferable);
            if (!trans) return;

            // specify the data we wish to handle. Plaintext in this case.
            trans.addDataFlavor('text/unicode');

            // To get the data from the transferable we need two new objects
            var str = new Object();
            var len = new Object();

            var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);

            str.data= s;        

            trans.setTransferData("text/unicode",str, str.data.length * 2);

            var clipid=Components.interfaces.nsIClipboard;              
            if (!clip) return false;
            clip.setData(trans,null,clipid.kGlobalClipboard);      
        }
    }	
	
	(function( $ ){
	
		$.fn.addSpinner = function() {
			
			$(this).html('<div class="module-loading-spinner"></div>');
		}
		
		$.fn.addPreview = function() {
			
			$(this).html('<div class="module-preview-area"></div>');
		}		

	})( jQuery );
	
    function JSONSyntaxHighlight(_object) {
        
        var json = {};
        var container = document.getElementById(_object);
        
        $('#'+_object).hide();
        
        try {
        
            json = JSON.parse($('#'+_object).find('span').text());            
            var editor = new JSONEditor(container, {enableSearch:false, enableHistory:false});        
            editor.set(json);        

            $('#'+_object).find('.jsoneditor-expand-all').trigger('click');        
            $('#'+_object).find('.jsoneditor-table td').css('border', '0px white solid').css('background-color', 'transparent');        
            $('#'+_object).find('.jsoneditor-table').find('.jsoneditor-td-edit').hide();
            $('#'+_object).find('.jsoneditor-table').find('.jsoneditor-append').hide();
            $('#'+_object).find('.jsoneditor-table').find('.jsoneditor-dragarea').hide();        
            $('#'+_object).find('.jsoneditor-collapse-all').trigger('click');
            $('#'+_object).show();            
        } catch(e) {
            
            alert('Sorry, some problems occured!');
        }
    }    
    
</script>