
<script>
	
    var CHART_INIT_VIEW_CONTAINER_WIDTH = 0;
    
	var PRESENTATION_SLIDE_HELP_VAR_CHART_TYPE = '';

	var CHART_PUSHER_GENERAL_chart_type = 'column';
	var CHART_PUSHER_GENERAL_chart_legend_type = null;
	var CHART_PUSHER_GENERAL_chart_legend_style = {enabled:false};
	var CHART_PUSHER_GENERAL_chart_margin_right = '20';		
	
	var CHART_PUSHER_LEGEND_BOTTOM_chart_type = 'column';
	var CHART_PUSHER_LEGEND_BOTTOM_chart_legend_type = 'bottom';
	var CHART_PUSHER_LEGEND_BOTTOM_chart_legend_style = {enabled:true};
	var CHART_PUSHER_LEGEND_BOTTOM_chart_margin_right = '20';
    
    var CHART_PUSHER_SPEED_METER = false;
	var CHART_PUSHER_TRAFFIC_PEAK = false;
    var CHART_PUSHER_SPEED_METER_YAXIS_MAX = 400;
    
    function ModuleChartInit()
    {
        if($('#ModulePreviewListTabGeneralChart')) {
            
            ModuleDisplayGraph('ModulePreviewListTabGeneralChart', 'CHART_PUSHER_GENERAL');
            ModuleDisplayGraph('ModulePreviewListTabChartMessageLastWeek', 'CHART_PUSHER_GENERAL');
            ModuleDisplayGraph('ModulePreviewListTabChartUserLastWeek', 'CHART_PUSHER_GENERAL');
            ModuleDisplayGraph('ModulePreviewListTabChartUserMessagesLastWeek', 'CHART_PUSHER_LEGEND_BOTTOM');

            CHART_PUSHER_SPEED_METER = ModuleChartGeneratorSpeed('ModulePreviewListTabGeneralTopChart', '', '', 'msg/s', '');

            CHART_PUSHER_TRAFFIC_PEAK = ModuleChartGeneratorThumbnail('ModulePreviewListTabGeneralTopChartPeaks', ["1", "2", "3", "4", "5", "6", "7"], [{"name":"Example", "data":[0,0,0,0,0,0,0]}], '', 'CHART_PUSHER_GENERAL');
        }
    }

	function ModuleDisplayGraph(_container, _code)
	{
		ModuleUpdateViews();
        
        CHART_INIT_VIEW_CONTAINER_WIDTH = ModulePreviewWidth;
		
		var _categories = $('#' + _container + '_data').find('.chart-item-categories').html();
		var _data = $('#' + _container + '_data').find('.chart-item-data').html();
		var _title = $('#' + _container + '_data').find('.chart-item-title').html();
		
		try {
			
			_categories = JSON.parse(_categories);
			_data = JSON.parse(_data);
			
		} catch(e) {
		
			ModuleChartGenerator(_container, ["1", "2", "3", "4", "5", "6", "7"], [{"name":"Example", "data":[0,0,0,0,0,0,0]}], 'Example', 'CHART_PUSHER_GENERAL');
		}
		
		ModuleChartGenerator(_container, _categories, _data, _title, _code);
	}
    
    // GRPAHS GENERATE FUNCTIONS
    
    function ModuleChartGeneratorThumbnail(_container, _categeries, _data, _title, _id, _slideid) {
	
        var chart = new Highcharts.Chart({
            
            colors: ["#000000", "#999999"],            
            chart: {
				backgroundColor:'rgba(255, 255, 255, 0.1)',	                
                renderTo: _container,
                type: 'area',
                marginRight: 10
            },
			credits: {
					enabled: false
				},            
            title: null,
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150,
                title:null,
                label:null,
            },
            yAxis: {
                title: null,
                labels: {
                    enabled:false
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return Highcharts.numberFormat(this.y, 0) + 'msg/s';
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
			plotOptions: {
				area: { 
					fillOpacity: 0.3,
                    lineWidth: 0,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 6,
                                lineWidth: 1
                            }
                        }
                    }					
				}
			},			
            series: [{
                data: (function() {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -59; i <= 0; i++) {
                        data.push({
                            x: time + i * 1000,
                            y: 0
                        });
                    }
                    return data;
                })()
            }]
        });       
        return chart;
	}    
    
    function ModuleChartGeneratorChannel(_container, _title, _subtitle) {
    
        var chart = new Highcharts.Chart({

            chart: {
                renderTo: _container,
                type: 'gauge',
                plotBorderWidth: 0,
                backgroundColor:'rgba(255, 255, 255, 0.1)',
                plotBackgroundImage: null,
                height: 400
            },
			credits: {
					enabled: false
				},          
            title: {
                text: 'VU meter'
            },
            pane: [{
                startAngle: -55,
                endAngle: 55,
                background: null,
                center: ['50%', '85%'],
                size: 500
            }],                        

            yAxis: [{
                min: 0,
                max: CHART_PUSHER_SPEED_METER_YAXIS_MAX,
                minorTickPosition: 'outside',
                tickPosition: 'outside',
                labels: {
                    rotation: 'auto',
                    distance: 20
                },
                plotBands: [{
                    from: CHART_PUSHER_SPEED_METER_YAXIS_MAX*0.8,
                    to: CHART_PUSHER_SPEED_METER_YAXIS_MAX,
                    color: '#C02316',
                    innerRadius: '100%',
                    outerRadius: '105%'
                }],
                pane: 0,
                title: {
                    text: _title + '<br/><span style="font-size:8px">' + _subtitle + '</span>',
                    y: -40
                }
            }],

            plotOptions: {
                gauge: {
                    dataLabels: {
                        enabled: false
                    },
                    dial: {
                        radius: '100%'
                    }
                }
            },


            series: [{
                data: [0],
                yAxis: 0
            }]

        });
        
        return chart;
    }
	
	function ModuleChartGeneratorThumbnailPercent(_container, _title, _unit, _series) {
	
		if(_series == undefined || _series == '') {
			
			_series = [{
					name:_unit_s1,
					data: (function() {
						// generate an array of random data
						var data = [],
							time = (new Date()).getTime(),
							i;

						for (i = -59; i <= 0; i++) {
							data.push({
								x: time + i * 1000,
								y: 0
							});
						}
						return data;
					})()
				},
				{
					name:_unit_s2,
					data: (function() {
						// generate an array of random data
						var data = [],
							time = (new Date()).getTime(),
							i;

						for (i = -59; i <= 0; i++) {
							data.push({
								x: time + i * 1000,
								y: 0
							});
						}
						return data;
					})()
				}];
		}
	
        var chart = new Highcharts.Chart({
            
            colors: ["#ccc", "#08C"],            
            chart: {
				backgroundColor:'rgba(255, 255, 255, 0.1)',	                
                renderTo: _container,
                type: 'area',
                marginRight: 10
            },
			credits: {
					enabled: false
				},            
            title: {
				text:_title,
				align:"right",
				floating:true,
				style:{
					color:'#08C',
					fontSize:'8px',
				},
				y:8
			},
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150,
                title:null,
                label:null,
                labels: {
                    enabled:false
                }
            },
            yAxis: {
                min: 0,
                max: CHART_PUSHER_SPEED_METER_YAXIS_MAX,				
                title: null,
                labels: {
                    enabled:true,
					style:{
						fontSize:'8px'
					}
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return Highcharts.numberFormat(this.y, 0) + _unit + ' ' + this.series.name;
                }
            },
			plotOptions: {
				area: { 
                    lineWidth: 0,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 6,
                                lineWidth: 1
                            }
                        }
                    }					
				}
			},
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: _series
        });       
        return chart;
	}    	
	
	function ModuleChartGeneratorSpeed(_container, _categeries, _data, _title, _id) {

		var chart = new Highcharts.Chart({

			chart: {
				backgroundColor: {
							linearGradient: [0, 0, 0, 400],
							stops: [
								[0, 'rgb(255, 255, 255)'],
								[1, 'rgb(255, 255, 255)']
							]
						},				
				renderTo: _container,
				type: 'gauge',
				plotBackgroundColor: null,
				plotBackgroundImage: null,
				plotBorderWidth: 0,
				plotShadow: false
			},
			credits: {
					enabled: false
				},
			title: {
				text: 'Traffic meter'
			},

			pane: {
				startAngle: -150,
				endAngle: 150,
				background: [{
					backgroundColor: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0, '#FFF'],
							[1, '#333']
						]
					},
					borderWidth: 0,
					outerRadius: '109%'
				}, {
					backgroundColor: {
						linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
						stops: [
							[0, '#333'],
							[1, '#FFF']
						]
					},
					borderWidth: 1,
					outerRadius: '107%'
				}, {
					// default background
				}, {
					backgroundColor: '#DDD',
					borderWidth: 0,
					outerRadius: '105%',
					innerRadius: '103%'
				}]
			},

			// the value axis
			yAxis: {
				min: 0,
				max: CHART_PUSHER_SPEED_METER_YAXIS_MAX,

				minorTickInterval: 'auto',
				minorTickWidth: 1,
				minorTickLength: 10,
				minorTickPosition: 'inside',
				minorTickColor: '#666',

				tickPixelInterval: 30,
				tickWidth: 2,
				tickPosition: 'inside',
				tickLength: 10,
				tickColor: '#666',
				labels: {
					step: 2,
					rotation: 'auto'
				},
				title: {
					text: _title
				},
				plotBands: [{
					from: 0,
					to: CHART_PUSHER_SPEED_METER_YAXIS_MAX*0.6,
					color: '#55BF3B' // green
				}, {
					from: CHART_PUSHER_SPEED_METER_YAXIS_MAX*0.6,
					to: CHART_PUSHER_SPEED_METER_YAXIS_MAX*0.8,
					color: '#DDDF0D' // yellow
				}, {
					from: CHART_PUSHER_SPEED_METER_YAXIS_MAX*0.8,
					to: CHART_PUSHER_SPEED_METER_YAXIS_MAX,
					color: '#DF5353' // red
				}]        
			},

			series: [{
				name: 'Traffic',
				data: [0],
				tooltip: {
					valueSuffix: ' msg/s'
				}
			}]

		});
        
        return chart;
	}

	function ModuleChartGenerator(_container, _categeries, _data, _title, _id) {

        var _type = '';
		var _min_x = 0;
		var _min_y = 0;
		var _max_y = null;
		var _type = 'column';
		var _marginRight = 0;
		var _legend = {};
		var _label = {};
		var _polar = 0;
		var _gridInterpolation = null;
		var _series = null;
		var _tick = null;
		var _stacking = null;
		var _allow_event_click = false;
		var _yAxis_text = 'Number';

		for(series in _data) {		
			for(record in _data[series]) {
				for(data in _data[series][record]) {	
					for(point in _data[series][record][data]) {
						if(_data[series][record][data].y < _min_y) {
							_min_y = _data[series][record][data].y;
						}						
					}					
				}			
			}
		}

        if(PRESENTATION_SLIDE_HELP_VAR_CHART_TYPE != '') {
            
            _type = PRESENTATION_SLIDE_HELP_VAR_CHART_TYPE;
        } else {
        
            _type = eval(_id+"_chart_type");
        }
		
		if(_type == 'spline') { 
			
			_min_x = 0.3;		
		}
		
		if(window[_id+"_allow_event_click"]) { 
			
			_allow_event_click = window[_id+"_allow_event_click"];		
			
			_series = {
					cursor: 'pointer',
					point: {
						events: {
							click: function(event) {

								if(!_allow_event_click) return false;

								window[_id+"_click"](event);
							}
						}
					}
				}			
		}		
		
        if(window[_id+"_chart_dragable"] && TAB_ACTIVE != 'MAIN_TABS') {         
            if(window[_id+"_offsets"]) {
                if(window[_id+"_offsets"] == 1) {
                    _series = {
                            cursor: 'ns-resize',
                            point: {
                                events: {
                                    drop: function() {	
                                        setUserChangesForGraph(this.series.index, this.category, this.y);								
                                    }
                                }
                            }
                        }
                }
            }
        }
		
		if(window[_id+"_yAxis_title"]) { 
			
			_yAxis_text = window[_id+"_yAxis_title"];						
		}		
        
		if(window[_id+"_chart_yaxis_max"]) { 
			
			_max_y = window[_id+"_chart_yaxis_max"];						
		}		

		if(window[_id+"_chart_yaxis_max_fix_auto"]) { 
			
			if(window[_id+"_chart_yaxis_max_fix_auto"] == true) {
				
				_max_y = window[_id+"_getMaxYAxisValue"](_data);	
			}
		}	

		if(window[_id+"_chart_margin_right"]) { 
			
			_marginRight = window[_id+"_chart_margin_right"];						
		}	
		
		if(window[_id+"_chart_yaxis_tick"]) { 
			
			_tick = window[_id+"_chart_yaxis_tick"];						
		}	
		
		if(window[_id+"_plot_stacking"]) { 
			
			_stacking = window[_id+"_plot_stacking"];						
		}		
		
		if(window[_id+"_polar"]) { 
			
			_polar = window[_id+"_polar"];	
			_gridInterpolation = 'polygon';
		}		
		
		// REPLACE LEGEND

		var _legend_type = eval(_id+"_chart_legend_type");

		if(_legend_type == 'right') { 
			
			_legend = {
                layout: 'vertical',
                align: 'right',
				fontSize: '15',
                verticalAlign: 'top',
                x: -20,
                y: 40,
                borderWidth: 0,
				itemMarginTop: 20,
				itemStyle: { 						
						fontSize: '18px',
						width:_marginRight-20
					},
            }			
		}
		
		if(window[_id+"_chart_legend_style"]) {
		
			_legend = window[_id+"_chart_legend_style"];
		}
		
		// END LEGENR REPLACE
		
		if(window[_id+"_chart_label_rotate"]) { 	
			
			_label = {
				rotation: window[_id+"_chart_label_rotate"],
				align: 'right',
				style: {
					fontSize: '12px',
					fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif',
					fontWeight:'bold'					
				},
			}		
		}

		CHART_PROVIDER = new Highcharts.Chart({
            chart: {
                renderTo: _container,
                type: _type,
				marginRight: _marginRight,
				polar: _polar
            },
			credits: {
					enabled: false
				},			
            title: {
                text: _title
            },
            subtitle: {
                text: ''
            },
            xAxis: {
				min: _min_x,
				labels: _label,
                categories: _categeries,
            },
            legend: _legend,			
            plotOptions: {
                spline: {
                    lineWidth: 4,
                    states: {
                        hover: {
                            lineWidth: 5
                        }
                    },
                    marker: {
                        enabled: true,
                        states: {
                            hover: {
                                enabled: true,
                                symbol: 'circle',
                                radius: 6,
                                lineWidth: 1
                            }
                        }
                    }
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
					showInLegend:true,
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %';
                        }
                    }
                },				
				column: { stacking: _stacking },
				area: { fillOpacity: 0.3 },
				series: _series
            },			
            yAxis: {
                min: _min_y,
				max: _max_y,
                title: {
                    text: _yAxis_text
                },
				tickInterval:_tick				
            },
            tooltip: {
                formatter: function() {
					
					if(this.y > 1000000) {
						return ''+
							 this.x +': '+ roundNumber(this.y/1000000,0) +'M items';
					} else if(this.y > 1000) {
						return ''+
							 this.x +': '+ roundNumber(this.y/1000,1) +'K items';
					} else {
						return ''+
							 this.x +': '+ this.y +' items';
					}
                }
            },
			series: _data
        });
	}	

	/**
	* Gray theme for Highcharts JS
	* @author Torstein Hønsi
	*/

	Highcharts.theme = {

        colors: ["#DDDF0D", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",
            "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
        chart: {
            backgroundColor: {
                linearGradient: [0, 0, 0, 400],
                stops: [
                    [0, 'rgb(96, 96, 96)'],
                    [1, 'rgb(16, 16, 16)']
                ]
            },
            borderWidth: 0,
            borderRadius: 15,
            plotBackgroundColor: null,
            plotShadow: false,
            plotBorderWidth: 0
        },
        title: {
            style: {
                color: '#FFF',
                font: '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
            }
        },
        subtitle: {
            style: {
                color: '#DDD',
                font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
            }
        },
        xAxis: {
            gridLineWidth: 0,
            lineColor: '#999',
            tickColor: '#999',
            labels: {
                style: {
                    color: '#999',
                    fontWeight: 'bold'
                }
            },
            title: {
                style: {
                    color: '#AAA',
                    font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                }
            }
        },
        yAxis: {
            alternateGridColor: null,
            minorTickInterval: null,
            gridLineColor: 'rgba(255, 255, 255, .1)',
            lineWidth: 0,
            tickWidth: 0,
            labels: {
                style: {
                    color: '#999',
                    fontWeight: 'bold'
                }
            },
            title: {
                style: {
                    color: '#AAA',
                    font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'
                }
            }
        },
        legend: {
            itemStyle: {
                color: '#CCC'
            },
            itemHoverStyle: {
                color: '#FFF'
            },
            itemHiddenStyle: {
                color: '#333'
            }
        },
        labels: {
            style: {
                color: '#CCC'
            }
        },
        tooltip: {
            backgroundColor: {
                linearGradient: [0, 0, 0, 50],
                stops: [
                    [0, 'rgba(96, 96, 96, .8)'],
                    [1, 'rgba(16, 16, 16, .8)']
                ]
            },
            borderWidth: 0,
            style: {
                color: '#FFF'
            }
        },


        plotOptions: {
            line: {
                dataLabels: {
                    color: '#CCC'
                },
                marker: {
                    lineColor: '#333'
                }
            },
            spline: {
                marker: {
                    lineColor: '#333'
                }
            },
            scatter: {
                marker: {
                    lineColor: '#333'
                }
            },
            candlestick: {
                lineColor: 'white'
            }
        },

        toolbar: {
            itemStyle: {
                color: '#CCC'
            }
        },

        navigation: {
            buttonOptions: {
                backgroundColor: {
                    linearGradient: [0, 0, 0, 20],
                    stops: [
                    [0.4, '#606060'],
                    [0.6, '#333333']
                    ]
                },
                borderColor: '#000000',
                symbolStroke: '#C0C0C0',
                hoverSymbolStroke: '#FFFFFF'
            }
        },

        exporting: {
            buttons: {
                exportButton: {
                    symbolFill: '#55BE3B'
                },
                printButton: {
                    symbolFill: '#7797BE'
                }
            }
        },

        // scroll charts
        rangeSelector: {
            buttonTheme: {
                fill: {
                    linearGradient: [0, 0, 0, 20],
                    stops: [
                    [0.4, '#888'],
                    [0.6, '#555']
                    ]
                },
                stroke: '#000000',
                style: {
                    color: '#CCC',
                    fontWeight: 'bold'
                },
                states: {
                    hover: {
                    fill: {
                        linearGradient: [0, 0, 0, 20],
                        stops: [
                            [0.4, '#BBB'],
                            [0.6, '#888']
                        ]
                    },
                    stroke: '#000000',
                    style: {
                        color: 'white'
                    }
                    },
                    select: {
                    fill: {
                        linearGradient: [0, 0, 0, 20],
                        stops: [
                            [0.1, '#000'],
                            [0.3, '#333']
                        ]
                    },
                    stroke: '#000000',
                    style: {
                        color: 'yellow'
                    }
                    }
                }
            },
            inputStyle: {
                backgroundColor: '#333',
                color: 'silver'
            },
            labelStyle: {
                color: 'silver'
            }
        },

        navigator: {
            handles: {
                backgroundColor: '#666',
                borderColor: '#AAA'
            },
            outlineColor: '#CCC',
            maskFill: 'rgba(16, 16, 16, 0.5)',
            series: {
                color: '#7798BF',
                lineColor: '#A6C7ED'
            }
        },

        scrollbar: {
            barBackgroundColor: {
                    linearGradient: [0, 0, 0, 20],
                    stops: [
                    [0.4, '#888'],
                    [0.6, '#555']
                    ]
                },
            barBorderColor: '#CCC',
            buttonArrowColor: '#CCC',
            buttonBackgroundColor: {
                    linearGradient: [0, 0, 0, 20],
                    stops: [
                    [0.4, '#888'],
                    [0.6, '#555']
                    ]
                },
            buttonBorderColor: '#CCC',
            rifleColor: '#FFF',
            trackBackgroundColor: {
                linearGradient: [0, 0, 0, 10],
                stops: [
                    [0, '#000'],
                    [1, '#333']
                ]
            },
            trackBorderColor: '#666'
        },

        // special colors for some of the demo examples
        legendBackgroundColor: 'rgba(48, 48, 48, 0.8)',
        legendBackgroundColorSolid: 'rgb(70, 70, 70)',
        dataLabelsColor: '#444',
        textColor: '#E0E0E0',
        maskColor: 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	var highchartsOptions = Highcharts.setOptions(Highcharts.theme);

</script>