
<script>

	jQuery(document).ready(function() {
		
		ModuleUpdateViews();
		
		// helpers
		$.extend($.gritter.options, { 
			position: 'bottom-right', // defaults to 'top-right' but can be 'bottom-left', 'bottom-right', 'top-left', 'top-right' (added in 1.7.1)
			fade_in_speed: 'medium', // how fast notifications fade in (string or int)
			fade_out_speed: 1000, // how fast the notices fade out
			time: 3000, // hang on the screen for...
		});		
		
		// events
		$(window).resize(ModuleUpdateViews);		

		$('#ModulePreviewHeaderButtonExpand').click(function() {
			ModuleManagerExpandPreview();
		});
		
		$('#ModulePreviewHeaderButtonExpandApis').click(function() {
				ModuleManagerHideClientAppMenu();
				ModuleManagerShowApiMenu();
		});		
		
		$('#ModulePreviewHeaderButtonExpandClients').click(function() {
				ModuleManagerHideAppApiMenu();
				ModuleManagerShowClientMenu();
		});			
		
		$('#ModulePreviewHeaderButtonExpandApps').click(function() {
				ModuleManagerHideClientApiMenu();
				ModuleManagerShowAppMenu();
		});				
		
		$('#ModuleContainerClientButtonAdd').click(function() { 
			if(ModuleServerSelectedSettings.id > 0 && ModuleServerSelectedConnectionStatus) {
                $("#ModulePushManagerModalClient").find("input[type=text]").val("");
                $("#ModulePushManagerModalClient").find("input[type=password]").val("");
                $('#PusherManagerClient_id').val(0);
				$('#ModulePushManagerModalClient').modal('show');
			} else {
				var servers = $('#ModuleServerModalSwitchServerList').children('.server-item').length;
				if(servers) {
					$('#ModuleServerModalSwitchServer').modal('show');
				} else {
					$('#ModuleServerModalNew').modal('show');
				}
			}			
		});
		
		$('#ModuleContainerAppButtonAdd').click(function() { $('#ModulePushManagerModalApp').modal('show'); } );
		$('#ModuleContainerApiButtonAdd').click(function() { $('#ModulePushManagerModalAPIKey').modal('show'); } );	
		$('#ModuleServerModalButtonSettingServer').click(function() { ModuleUserAction = 'update'; $('#ModuleServerModalNew').modal('show'); return false; });
		$('#ModuleServerModalButtonNewServer').click(function() { ModuleUserAction = 'new'; $('#ModuleServerModalNew').modal('show'); return false; });
		$('#ModuleServerModalSettingsButtonSwitchToDefault').click(function() { ModuleServerSwitchTo(ModuleServerDefaultId); $('#ModuleServerModalSettings').modal('hide'); } );		
		$('#ModuleServerModalServerMongoEmptyButtonOK').click(function() { ModuleServerEmptyMongoDB(); });
		$('#ModuleServerDeleteConnectionButtnOK').click(ModuleServerDeleteConnection);
		
        // Server Dashboard / Management Console
        $('#ModuleServerModalButtonServerDashboard').click(function() {

            $(this).parent().find('.btn').removeClass('btn-inverse');
            $(this).addClass('btn-inverse');
			$('#ModulePreviewHeader').find('.npush-client').hide();
			$('#ModulePreviewHeader').find('.npush-server').show();
           
            ModuleManagerSelectedApiId = '';  
            ModulePusherGetServerPreview();
        });      
        
        $('#ModuleServerModalButtonClientDashboard').click(function() {
           $(this).parent().find('.btn').removeClass('btn-inverse');
           $(this).addClass('btn-inverse');		   
		   $('#ModulePreviewHeader').find('.npush-server').hide();
		   $('#ModulePreviewHeader').find('.npush-client').show();
           $('#ModulePreviewList').empty().addPreview();
		   $('#ModuleContainerClient').show();
		   $('#ModulePreviewList').width($('#ModuleContainer').width() -  $('#ModuleContainerClient').width());
        });
        
        // other
        
        $('#ModuleContainerClientListInputSearch').keyup(function(e) { 
            if(e.which == 27) {
                $('#ModuleContainerClientListInputSearch').val('');
            }
            ModulePusherGetClientListForServer();
        });
        
        $('#ModuleContainerAppsListInputSearch').keyup(function(e) { 
            if(e.which == 27) {
                $('#ModuleContainerAppsListInputSearch').val('');
            }
            ModulePusherGetAppsListForClient();
        });      
		
		$('#ModuleContainerApisListInputSearch').keyup(function(e) { 
            if(e.which == 27) {
                $('#ModuleContainerApisListInputSearch').val('');
            }
            ModulePusherGetApisListForApp();
        });
        
        // LOAD DATA
		ModuleManagerInitServersList();		
		ModuleServerSwitchTo(ModuleServerDefaultId);
	
		// MODALS
		$('#ModuleServerModalNew').on('show', function (e) {
			$("#ModuleServerModalNew").find(".control-group").removeClass('error');
			$("#ModuleServerModalNew").find("input[type=text]").val("");
			if(ModuleServerSelectedSettings.id > 0 && ModuleUserAction == 'update') {
				$('#PusherManagerServer_id').val(ModuleServerSelectedSettings.id);
				$('#PusherManagerServer_config_name').val(ModuleServerSelectedSettings.name);
				$('#PusherManagerServer_mongo_address').val(ModuleServerSelectedSettings.m_address);
				$('#PusherManagerServer_mongo_port').val(ModuleServerSelectedSettings.m_port);
				$('#PusherManagerServer_mongo_username').val(ModuleServerSelectedSettings.m_username);
				$('#PusherManagerServer_mongo_password').val(ModuleServerSelectedSettings.m_password);
				$('#PusherManagerServer_mongo_dbname').val(ModuleServerSelectedSettings.m_dbname);
				$('#PusherManagerServer_push_address').val(ModuleServerSelectedSettings.p_address);
				$('#PusherManagerServer_push_port').val(ModuleServerSelectedSettings.p_port);
				$('#PusherManagerServer_push_version').val(ModuleServerSelectedSettings.p_version);
			}			
		});
		
		$('#ModuleServerModalNew').on('shown', function (e) {
			$('#PusherManagerServer_config_name').focus();
		});
		
		$('#ModulePushManagerModalClient').on('shown', function (e) {
			$('#PusherManagerClient_name').focus();
		});	

		$('#ModulePushManagerModalApp').on('shown', function (e) {
			$('#PusherManagerApp_name').focus();
		});
		
		$('#ModulePushManagerModalAPIKey').on('shown', function (e) {
			$('#PusherManagerAPIKey_name').focus();
		});		
		
		$('#ModuleServerModalSettings').on('shown', function(e) {
			$('#ModuleServerModalSettingsList').find('.server-item').removeClass('server-item-default');
			$('#ModuleServerModalSettingsList').find('.server-item-id-' + ModuleServerDefaultId).addClass('server-item-default');
		});			
	});

	function ModuleManagerInitClientList() {
		$('#ModuleContainerClient').find('.crm-item').off().click(function() {
			if($(this).attr('type') == 'search') return;
			ModuleManagerShowAppMenu();
			ModuleManagerHideApiMenu();
			$(this).parent().find('.crm-item').removeClass('active');
			$(this).addClass('active');			
			ModuleManagerSelectedClientId = $(this).attr('mongo-id');
			ModulePusherGetAppsListForClient();
		});		
        
        $('#ModuleContainerClient').find('.crm-item-edit').off().click(function(e) {
            try {
                var _json = JSON.parse($(this).parent().find('.crm-item-edit-json').text());
                // some action
                $('#PusherManagerClient_id').val(_json._id.$id);
                $('#PusherManagerClient_name').val(_json.name);
                $('#PusherManagerClient_dscr').val(_json.dscr);
                $('#PusherManagerClient_username').val(_json.username);
                $('#PusherManagerClient_password').val(_json.password);
                $('#PusherManagerClient_password_repeat').val(_json.password);                
                $('#ModulePushManagerModalClient').modal('show');
            } catch(e) {
                // nothing to do
            }
            e.stopPropagation();
        });
	}
	
	function ModuleManagerInitAppsList() {
		$('#ModuleContainerApps').find('.crm-item').off().click(function() {
			if($(this).attr('type') == 'search') return;
			ModuleManagerShowApiMenu();
			$(this).parent().find('.crm-item').removeClass('active');
			$(this).addClass('active');			
			ModuleManagerSelectedAppId = $(this).attr('mongo-id');
			ModulePusherGetApisListForApp()
		});			
        
        $('#ModuleContainerApps').find('.crm-item-edit').off().click(function(e) {
            var _json = $(this).parent().find('.crm-item-edit-json').text();
            // some action
            alert(_json);            
            e.stopPropagation();
        });        
	}
	
	function ModuleManagerInitApisList() {
		$('#ModuleContainerApi').find('.crm-item').off().click(function() {
			if($(this).attr('type') == 'search') return;
			$(this).parent().find('.crm-item').removeClass('active');
			$(this).addClass('active');
			ModuleManagerSelectedApiId = $(this).attr('mongo-id');
			ModulePusherGetApiPreview();
		});			
        
        $('#ModuleContainerApi').find('.crm-item-edit').off().click(function(e) {
            var _json = $(this).parent().find('.crm-item-edit-json').text();
            // some action
            alert(_json);            
            e.stopPropagation();
        });        
	}
        
	function ModuleManagerInitApiPreview() {
		$('#ModulePreviewListTabGeneralAPIEnabled').off('click').click(function() {
				ModulePusherAPISwitchToEnabled(this);
				$(this).parent().find('.btn').removeClass('btn-danger btn-success active');
				$(this).addClass('btn-success active');
		});

		$('#ModulePreviewListTabGeneralAPIDisabled').off('click').click(function() {
				ModulePusherAPISwitchToDisabled(this);
				$(this).parent().find('.btn').removeClass('btn-danger btn-success active');
				$(this).addClass('btn-danger active');                        
		});     
		
		$('#ModulePreviewListTabDataBottom').find('.btn').off('click').click(function() {
			$(this).parent().find('.btn').removeClass('active btn-primary');
			$(this).addClass('active btn-primary');
			ModuleDataManageDocumentOnPage = $(this).attr('docs-on-page');
			ModulePusherDataCollectionRefreshView();
		});

		$('#ModulePreviewListTabTestButtonCURL').off('click').click(function() {
			ModulePusherGetAuthoristation();
		});
		
		$('#ModulePreviewListTabTestButtonPingPush').off('click').click(function() {
			ModulePusherPingServer();
		});		
		
		$('#ModulePreviewListTabSettingsButtonDownloadLibary').off('click').click(function() {
			ModulePusherDownloadLibraryWithSettings('full');
		});
		
		$('#ModulePreviewListTabSettingsButtonDownloadLibaryMin').off('click').click(function() {
			ModulePusherDownloadLibraryWithSettings('min');
		});
		
		// CHAT TABS
		$('a[data-toggle="tab"]').on('shown', function (e) {
			var _iframe = $(e.target).attr('iframe-id');
			var _data_view = $(e.target).attr('data-view');
            var _graph_view = $(e.target).attr('graph-view');
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            var isiPhone = navigator.userAgent.match(/iPhone/i) != null;		
			if(_iframe > 0 && _iframe < 4) {
				ModuleChatUserSelected = _iframe;
				if (isiPad || isiPhone) {
					// do nothing
				} else {
					$('#ModulePreviewListTabChatInputMessage').focus();
				}
                $('#ModulePreviewListTabChatButtonSend, #ModulePreviewListTabChatButtonSendChannel').show();
                $('#ModulePreviewListTabChatInputMessage').off().keyup(function(e) {
                    if(e.which == 13)
                        $('#ModulePreviewListTabChatButtonSend').trigger('click');
                });                
			} else if(_iframe > 3 && _iframe < 7) {
                ModuleChatUserSelected = 1; // because we want send using alpha chat user
				if (isiPad || isiPhone) {
					// do nothing
				} else {
					$('#ModulePreviewListTabChatInputMessage').focus();
				}                
                $('#ModulePreviewListTabChatButtonSend, #ModulePreviewListTabChatButtonSendChannel').hide();
                $('#ModulePreviewListTabChatInputMessage').off().keyup(function(e) {
                    if(e.which == 13)
                        $('#ModulePreviewListTabChatButtonSendApp').trigger('click');
                });                                
            }
			
			if(_data_view != undefined && _data_view != '') {
				ModuleDataManageCollection = _data_view;
				ModulePusherDataCollectionRefreshView();
			}
            if(_graph_view != undefined && _graph_view != '') {
                ModuleUpdateViews();
                if(CHART_INIT_VIEW_CONTAINER_WIDTH != ModulePreviewWidth) {
					ModuleChartInit();
				}
            }
		});
        
        $('#ModulePreviewListTabChatInputMessage').off().keyup(function(e) {
             if(e.which == 13) {
                 $('#ModulePreviewListTabChatButtonSendApp').trigger('click');
			 }
        });         
		
		$('#ModulePreviewListTabChatButtonSend').click(ModulePusherChatSendRoomMessage);		
		$('#ModulePreviewListTabChatButtonSendChannel').click(ModulePusherChatSendChannelMessage);
        $('#ModulePreviewListTabChatButtonSendApp').click(ModulePusherChatSendAppMessage);
        
        $('#ModulePreviewListTabGeneralInputText').keyup(function(e) {
			if(e.which == 13) {
				$('#ModulePreviewListTabGeneralButtonSendText').trigger('click');
			}
        });
		
		$('#ModulePreviewListTabDataInputSearch').keyup(function(e) {
            if(e.which == 27) {
                $('#ModulePreviewListTabDataInputSearch').val('');
            }
			ModulePusherDataCollectionSearch();	
		});
		
		$('#ModulePreviewListTabDataButtonClear').off().click(function(e) {
			ModulePusherDataCollectionClear();
			e.stopPropagation();
		});
        
        // DASHBOARD
        var ModulePreviewDashboardTimer = null;
        $('#ModulePreviewListTabGeneralTopChart').mouseover(function() {
            $('#ModulePreviewListTabGeneralTopChartToolbar').fadeIn('slow');
            clearInterval(ModulePreviewDashboardTimer);
        });
        
        $('#ModulePreviewListTabGeneralTopChart').mouseout(function() {
           ModulePreviewDashboardTimer = setTimeout(function() {
            $('#ModulePreviewListTabGeneralTopChartToolbar').fadeOut('slow');
           }, 3000);
        });     
        
        $('#ModulePreviewListTabGeneralTopChartToolbar').find('.btn').off().click(function() {
           ModulePusherDashboardUpdateSpeedMeterScale($(this).attr('code'));
           return false;
        });
		
		// GET COLLECTION DATA
		ModulePusherDataCollectionMessages();
		
		// get CURL request status
		$('#ModulePreviewListTabGeneralButtonSendText').click(function() {
			ModulePusherSendBroadCastMessage($('#ModulePreviewListTabGeneralInputText').val());
            $('#ModulePreviewListTabGeneralInputText').val('');
		});

		$('#ModulePreviewListTabGeneralButtonSendJSON').click(function() {
			ModulePusherSendBroadCastData($('#ModulePreviewListTabGeneralInputText').val());
			$('#ModulePreviewListTabGeneralInputText').val('');
		});

		ModulePusherGetAuthoristation();
		
		// CHARTS
        ModuleChartInit();
		// CLIPBOARD
		ZeroClipboard.setMoviePath('<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/clipboard/ZeroClipboard.swf');
			
		var clipSettings = '';
		var clipCURL = '';
		var myTextToCopy = '';
			
		setTimeout(function() {
			clipSettings = new ZeroClipboard.Client();			
			myTextToCopy = $('#ModulePreviewListTabSettingsTextarea').text();
			clipSettings.setText(myTextToCopy);			
			clipSettings.glue($('#ModulePreviewListTabSettingsButtonCopy').attr("id"));

			clipCURL = new ZeroClipboard.Client();			
			myTextToCopy = $('#ModulePreviewListTabTestTextarea').text();
			clipCURL.setText(myTextToCopy);			
			clipCURL.glue($('#ModulePreviewListTabTestButtonCopy').attr("id"));
		}, 500);
			
		$('#ModulePreviewListTabSettingsButtonCopy').off('click').click(function(e) {
			clipSettings = new ZeroClipboard.Client();			
			myTextToCopy = $('#ModulePreviewListTabSettingsTextarea').text();
			clipSettings.setText(myTextToCopy);			
			clipSettings.glue($(this).attr("id"));					
		});
		
		$('#ModulePreviewListTabTestButtonCopy').off('click').click(function(e) {
			clipCURL = new ZeroClipboard.Client();			
			myTextToCopy = $('#ModulePreviewListTabTestTextarea').text();
			clipCURL.setText(myTextToCopy);			
			clipCURL.glue($(this).attr("id"));					
		});		
		// END CLIPBOARD
	}
	
	function ModuleManagerInitServersList() {		
		$('#ModuleServerModalSwitchServerList').find('.server-item').off().click(function() {
			ModuleServerSwitchTo($(this).attr('dbid'));
			$('#ModuleServerModalSwitchServer').modal('hide');
		});
		
		$('#ModuleServerModalSettingsList').find('.server-item').off().click(function() {
			try {
				_tmp = JSON.parse($(this).find('.server-item-settings').text());			
				ModuleServerDefaultId = _tmp.id;
				ModuleServerSetDefault();
				$('#ModuleServerModalSettings').trigger('shown');
				$('#ModuleServerModalSwitchServer').modal('hide');
			} catch(e) {
				alert('Cannot switch to server. No setting available!');
			}
		});
	}

</script>