<style>
	 .p3{padding:3px;}
	 .p4{padding:4px;}
	 .p5{padding:5px;}
	.module-loading-spinner { height:60px; width:100%; background-repeat: no-repeat;background-position:center; background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/images/spinner.gif); }
	.module-preview-area { width:100%; height:100%; background-repeat: no-repeat;background-position:center; background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/images/nopreview.jpg);  }
	.active-text { font-weight:bold; }
	.crm-text-classic { padding:5px; line-height:28px; font-size:14px; }
	.crm-text-fitted { width:115px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }
	.crm-button-float-right { float:right; padding:6px; }
	.crm-button-float-left { float:left; padding:6px; }

	#ModuleContainer [class^="icon-"],
	[class*=" icon-"] {
		display: inline-block;
		width: auto;
		height: auto;
		*margin-right: .3em;
		line-height: 16px;
		vertical-align: text-middle;
		background-image: none;
		background-position: 0px 0px;
		background-repeat: no-repeat;
	}	

	#ModuleHeader [class^="icon-"],
	[class*=" icon-"] {
		display: inline-block;
		width: auto;
		height: auto;
		*margin-right: .3em;
		line-height: 16px;
		vertical-align: text-middle;
		background-image: none;
		background-position: 0px 0px;
		background-repeat: no-repeat;
	}	

	.crm-item-edit { display:none; float:right; padding:6px 6px 0px 0px; }
	.crm-item-empty { text-shadow: 1px 1px #eee; height:35px; text-align: center; vertical-align: middle; padding-top:20px; font-size:11pt; }

	#ModulePreviewList ul li a { padding:3px 10px 3px 10px; border-radius: 15px; }
	#ModuleContainer .crm-item.active { background-color: transparent; }

	.crm-item:hover .crm-item-edit { display:block; }
	.server-item:hover { background-color: #eee; cursor:pointer; }
	.server-item-default { box-shadow: inset 0px 0px 30px 30px #eee; }

	#ModuleContainer .tooltip { z-index: 9999; position: absolute; }
    
    #ModulePreviewListTabDataContainerMessages .btn-expand {width:28px;height:20px;padding:0px;border-radius:6px;border:0px;}
</style>