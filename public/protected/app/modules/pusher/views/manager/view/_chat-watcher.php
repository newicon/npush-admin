<?php
/*
 * BASIC WATCHER APPLICATION
 */
?>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/npush.js" type="text/javascript" rel="stylesheet" /></script>

<?php $this->renderPartial('style/chat'); ?>

<div class="container" id="chat">
	<div id="messages" style="background-color: white; height:auto; overflow:auto;">
        <div class="row-fluid" style="padding-bottom: 5px; padding-top: 5px;">
            <div class="span1"></div>
            <div class="span4">		
                <input id="watcherChannelName" style="width:90%;border-radius:7px;border:1px solid #eee;padding:3px;margin:2px;" placeholder=" e.g. channel-1 or channel-1/room-5" value="service/debug">
            </div>
            <div class="span4">
                <input id="watcherDataObject" style="width:90%;border-radius:7px;border:1px solid #eee;padding:3px;margin:2px;" placeholder=" e.g. {&quot;event&quot;:{&quot;a&quot;:&quot;$this&quot;}" value="{&quot;dataSystem&quot;:{&quot;user_name&quot:&quot$this&quot;}}">
            </div>
            <div class="span3" style="padding-top:2px;">
                <div class="btn-group">
                    <a href="#" id="watcherStartWatching" class="btn btn-danger"><i class="icon-play"></i></a>
                    <a href="#" id="watcherStopWatching" class="btn btn-info"><i class="icon-stop"></i></a>
                </div>
            </div>
        </div>
        <div class="row-fluid"><br></div>
        <div class="row-fluid" style="text-align: center;"><span class="badge" id="watcherStatus">Watcher is stopped now</span></div>
        <div class="row-fluid"><br></div>
        <div class="row-fluid">
            <div class="span1"></div>
            <div class="span10">
                <div class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;" id="watcherActualValue">
                    <strong>Actual value</strong><br>waiting for data...
                </div>
            </div>
            <div class="span1"></div>
        </div>
        <div class="row-fluid">
            <div class="span1"></div>
            <div class="span10">
                <div class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;">
                    <strong>History</strong>
                    <div class="row-fluid" id="watcherHistoryValue">  
                        waiting for data...
                    </div>
                </div>
            </div>
            <div class="span1"></div>
        </div>
        <div id="connecting">
                <div class="wrap module-loading-spinner">Connecting to Push Notification Server</div>
        </div>        
        <div id="nicknames" style="display:none;"></div>			
        <div id="userschannel" style="display:none;"></div>
        <div id="usersapplist" class="hide alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"></div>
        <div id="channels" class="hide alert alert-success" style="margin-bottom:6px; margin-left:10px; margin-right: 5px;"></div>			
        <div id="lines" style="display:none;"></div>
	</div>
	<div id="history" style="display:none;"></div>
</div>

<script>
	
	var mychannel = null;
    var watcherStatus = 0;
	
    var testStreeChannel = new Array();
    var currentChannelName = '<?php echo $channel; ?>/<?php echo $room; ?>';
    var currentDataPattern = {};
    
    function getValueForPattern(_pattern, _data) {
        if(_pattern.length == 0) {
               return '';
		}
        for(a in _pattern) {
            if(_pattern[a] == '$this') {
                try {
                    return _data[a];
                } catch(e) {
                    return null;
                }
            }
            
            for(b in _pattern[a]) {
                if(_pattern[a][b] == '$this') {
                    try {
                        return _data[a][b];
                    } catch(e) {
                        return null;
                    }
                }        
                
                for(c in _pattern[a][b]) {
                    if(_pattern[a][b][c] == '$this') {
                         try {
                             return _data[a][b][c];
                         } catch(e) {
                             return null;
                         }
                     }                    
                }
            }
        }
        return null;
    }
    
    function getChannelNameFromPath(channel) {
		
		var room = 'default';
		
		if(channel.indexOf('/') > -1) {
			// get the room path
			room = channel.substring(channel.indexOf('/') + 1, channel.length);
			if(room.substring(room.length-1, room.length) == '/')
				room = room.substring(0, room.length - 1);
			
			if(room == '') {
				room = 'default';
			}
			// get the channel name
			channel = channel.substring(0, channel.indexOf('/'));		
		}
        return channel;
    }
    
    function ModulePusherSubscribeServiceChannel(_channel) {

        currentChannelName = _channel;

		npush.init({ 
				'apiKey' : '<?php echo $apiId; ?>',
				'url' : '<?php echo $server['push_address']; ?>', 
				'port' : <?php echo $server['push_port']; ?>, 
				'user_id' : '<?php echo $userId; ?>', 
				'user_name' : '<?php echo $userName; ?>'
			}		
		);		
        
        mychannel = npush.subscribe(_channel, {
                'connect' : ModulePusherSubscribeService_connected,
                'connecting' : ModulePusherSubscribeService_connecting,
                'connect_fail' : null,
                'reconnect' : ModulePusherSubscribeService_reconnect,
                'reconnecting' : ModulePusherSubscribeService_reconnecting,
                'reconnect_fail' : null,
                'disconnect' : ModulePusherSubscribeService_disconnect,
                'error' : ModulePusherSubscribeService_error,
                'message' : ModulePusherSubscribeService_messageNative,
            } 		
        );

        mychannel.on('message', function(dataUser, dataSystem) {
            ModulePusherSubscribeService_message({'dataUser':dataUser, 'dataSystem' : dataSystem });			
        });
		
		/*
		 *	BASIC EXAMPLE
		 
			mychannel = npush.subscribe('mychannel');
			
			mychannel.on('message', function(message) { 

				alert(message); 
			});			
	
			mychannel.trigger('message', 'Hello world!);
		 
		 *	ADVANCED EXAMPLE:

			// init push

			npush.init({
						'apiKey' : 'your_api_key',
						'url' : 'push_server_ip_or_url', 
						'port' : 'push_server_port', 
						'user_id' : 'id_as_number_or_string',	// allow use history
																// as user activity
						'user_name' : 'name_asnumber_or_String'
					});
	
			// create channel
		
			mychannel = npush.subscribe('mychannel');
			mychannel = npush.subscribe('mychannel/myroom');
			mychannel = npush.subscribe('mychannel/myrooms/family');
			mychannel = npush.subscribe('mychannel', { 
														connect : function() {},
														disconnect : function() {},
														error : function() {}
													});
	
			// events
			// event_name is any string
	
			mychannel.on('event_name', function(dataUser) { 

				alert(dataUser); 
			});	
	
			mychannel.on('event_name', function(dataUser, dataSystem) { 

				alert(dataUser + ' ' + dataSystem); 
			});

			// user activity
			// to work with activity you have to provide uniqe USER_ID on init
	
			mychannel.activity(0);		// from last activity on channel/room
			mychannel.activity(60000);	// from last 60 seconds
	
			// send message
	
			mychannel.trigger('event_name', _msg);		// send to api/channel/room/*
			mychannel.channel('event_name', _msg);		// send to api/channel/*
			mychannel.app('event_name', _msg);			// send to api/*
	
			// system events, you can add callback on channel subscribe
				- connect			callback()
				- connecting		callback()
				- connect_fail		callback()
				- reconnect			callback()
				- reconnecting		callback()
				- reconnect_fail	callback()
				- disconnect		callback()
				- error				callback(e)
				- message			callback(dataObject) 
									-> dataObject = { 
												dataUser: StringOrObject, 
												dataSystem: {
													
													client		: String
													room		: String
													channel		: String
													time		: Int
													user_id		: String
													user_name	: String
													type		: Int
													archive		: Int
												}, 
												event : String 
											}
				- announcement		callback(dataString)
				- usersroom			callback(dataArray)
				- userschannel		callback(dataArray)
				- usersapp			callback(dataArray)
		 */
    }    
	
	// npush channel events
    function ModulePusherSubscribeService_connected() {
        $('#chat').addClass('connected');
        $('#chat').addClass('nickname-set'); 
        // set status            
        if(watcherStatus) {
            $('#watcherStatus').addClass('badge-success').html('Watcher is working now...');
		}
    }

    function ModulePusherSubscribeService_connecting() {
        // nothing to do now
    }

    function ModulePusherSubscribeService_announcement(msg) {
		$('#lines').append($('<p>').append($('<em>').text(msg)));
    }

    function ModulePusherSubscribeService_nicknames(nicknames) {
		$('#nicknames').empty().append($('<span>Online in my room: </span>'));
		for (var i in nicknames) {
			$('#nicknames').append($('<b>').text(nicknames[i]));
		}
    }

    var _max_connection = 0;
    function ModulePusherSubscribeService_nicknamesapp(app) {

		var _string = '';
		var _count = 0;

        $('#channels').empty().html('<strong>Active channels and rooms:</strong>');

		for (var a in app) {
            $('#channels').append('<li>' + a + '</li>');
			for (var r in app[a]) {
                $('#channels').append('<li>' + a + '/' + r + '</li>');
				for (var u in app[a][r]) {
                    //$('#channels').append('<li>' + a + '/' + r + '/' + u + '</li>');    
					_string = _string + '<span class="badge badge-inverse">' + app[a][r][u] + ' : ' + u +'</span>&nbsp;';
					_count++;
				}
			}            
		}
      
		if(_count > _max_connection) _max_connection = _count;
		var _time = new Date();
		$('#usersapplist').empty().append($('<span><strong>Active sockets</strong><br>(now: ' + _count + ', max. in session: ' + _max_connection + '):</span><br>'));
		$('#usersapplist').append(_string);
		window.parent.ModulePusherDashboardUpdate();
    }

    function ModulePusherSubscribeService_nicknameschannel(rooms) {
		$('#userschannel').empty().append($('<span>Online in rooms on my channel: </span>'));
		for (var r in rooms) {
			for (var u in rooms[r]) {
				$('#userschannel').append($('<b>').text(rooms[r][u]));
			}
		}
    }

    function ModulePusherSubscribeService_reconnect() {
		ModulePusherSubscribeService_message('System', {msg: { object : 'Reconnected to the server', data : '' } } );
    }

    function ModulePusherSubscribeService_disconnect() { 
		try {
            // set status                
            $('#watcherStatus').removeClass('badge-success').html('Watcher is stopped now');            
			ModulePusherSubscribeService_message('System', {msg: { object : 'disconnected', data : '' } } );
		} catch(e) { }
    } 

    function ModulePusherSubscribeService_reconnecting() {
		ModulePusherSubscribeService_message('System', {msg: { object : 'Attempting to re-connect to the server', data : '' } } );
    }

    function ModulePusherSubscribeService_error(e) {
		try {
			ModulePusherSubscribeService_message('System', e ? e : {msg: { object : 'A unknown error occurred', data : '' } } );
		} catch(e) { }			
    }

	function ModulePusherSubscribeService_messageNative(data) {
        var t = new Date();
        var v = getValueForPattern(currentDataPattern, data);
        if(v != null) {
            $('#watcherActualValue').html('<strong>Actual value</strong>&nbsp;<h3>' + v + '</h3>');        
            $('#watcherHistoryValue').prepend('<p>' + t.getTime() + ':&nbsp;&nbsp;' + v + '</p>');
        }
	}

    function ModulePusherSubscribeService_message(data, system) {
        // classic message callback
    }
    
    // dom manipulation

    $(document).ready(function() {
		
		viewUpdate();
		ModulePusherSubscribeServiceChannel(currentChannelName);
		
		$(window).resize(function() {
			viewUpdate();
		});
        
        $('#watcherStartWatching').click(function() {
           
           var _channel = getChannelNameFromPath(currentChannelName);
           NIPUSH.channels[_channel].disconnect();           
           
           try {
                // set new data pattern
                currentDataPattern = JSON.parse($('#watcherDataObject').val());
                // connect to new channel
                ModulePusherSubscribeServiceChannel($('#watcherChannelName').val());
                // status
                watcherStatus = 1;
                // set view
                $('#watcherHistoryValue').empty();
                $('#watcherActualValue').html('<strong>Actual value</strong><br>waiting for data...');
           } catch(e) {
               alert('Wrong data object: ' + e.message);
           }
        });
        
        $('#watcherStopWatching').click(function() {
           var _channel = getChannelNameFromPath(currentChannelName);
           NIPUSH.channels[_channel].disconnect(); 
           // status
           watcherStatus = 0;
        });
    });
    
    // Send functions called from parent windows
    // this chat app is used in iframe window
	function sendMessage(_msg) {
		mychannel.trigger('message', _msg);
        $('#lines').get(0).scrollTop = 10000000;
	}
	
	function sendMessageToChannel(_msg) {
		mychannel.channel('message', _msg);		
        $('#lines').get(0).scrollTop = 10000000;
	}	
	
	function sendMessageToApp(_msg) {
		mychannel.app('message', _msg);		
        $('#lines').get(0).scrollTop = 10000000;
	}       
    
    // refreshing view on resize
	function viewUpdate() {
		$('#messages').height($(window).height()-110);
	}
</script>