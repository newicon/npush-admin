
<?php

	$form = $this->beginWidget('NActiveForm', array(
		'id' => 'newServerForm',
		'action' => Yii::app()->createUrl('/pusher/manager/addServer'),
		'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form, data, hasError){
				
				$.post(form.attr("action"), form.serialize(), function(data){
				
					$("#ModuleServerModalNew").find("input[type=text]").val("");
					$("#ModuleServerModalNew").modal("hide");
					$("#ModuleServerModalNew").find(".modal-body").scrollTop(0);
					ModuleServerGetList("update-selected");
                    
                    if(parseInt($("#PusherManagerServer_id").val()) > 0) {
                    
                        ModuleManagerHideAppApiMenu();
                        $("#ModulePreviewList").addPreview();
						ModuleServerGetList("update-selected");
                    } else {
						ModuleServerGetList();
					}
					
					$.gritter.add({ title:"Server", text:"New Server successfuly added..." });
				});
				
				return false;
			}'),
		'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'htmlOptions'=>array('class'=>'form-horizontal')
	));
	$modelServer = new PusherManagerServer();
?>

<!-- New Server Modal -->

<div class="modal hide fade" id="ModuleServerModalNew" style="z-index:9999;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="RBAlertWindowTitle"><i class="icon-hdd" style="background:none;line-height:30px;width:33px;"></i>New Server</h3>
	</div>
	<div class="modal-body">
		<?php echo $form->hiddenField($modelServer,'id', array('value'=>'0')); ?>
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'config_name') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'config_name', array('placeholder' => 'e.g. master-server')); ?>
				<?php echo $form->error($modelServer,'config_name'); ?>
			</div>
		</div>	
		<div class="control-group">
			<div class="controls" style="margin-left:0px; text-align: center;">
                <h4><span class="label label-info" style="line-height:19px;font-size:15px;">MongoDB</span> Server Settings</h4>
			</div>
		</div>			
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'mongo_address') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'mongo_address', array('placeholder' => 'e.g. push.newicon.net')); ?>
				<?php echo $form->error($modelServer,'mongo_address'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'mongo_port') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'mongo_port', array('style'=>'width:50px', 'value' => '27017')); ?>
				<?php echo $form->error($modelServer,'mongo_port'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'mongo_username') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'mongo_username'); ?>
				<?php echo $form->error($modelServer,'mongo_username'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'mongo_password') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'mongo_password'); ?>
				<?php echo $form->error($modelServer,'mongo_password'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'mongo_dbname') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'mongo_dbname', array('placeholder' => 'e.g. mydb')); ?>
				<?php echo $form->error($modelServer,'mongo_dbname'); ?>
			</div>
		</div>	
		<div class="control-group">
			<div class="controls" style="margin-left:0px; text-align: center;">
                <h4><span class="label label-info" style="line-height:19px;font-size:15px;">n:Push</span> Server Settings</h4>
			</div>
		</div>		
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'push_address') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'push_address', array('placeholder' => 'e.g. push.newicon.net')); ?>
				<?php echo $form->error($modelServer,'push_address'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'push_port') ?>
			<div class="controls">
				<?php echo $form->textField($modelServer, 'push_port', array('style'=>'width:50px', 'value' => '80')); ?>
				<?php echo $form->error($modelServer,'push_port'); ?>
			</div>
		</div>
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'push_version') ?>
			<div class="controls">
				<?php echo $form->dropDownList($modelServer, 'push_version', array('1' => '2.0')); ?>
				<?php echo $form->error($modelServer,'push_version'); ?>
			</div>
		</div>				
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'push_control_key') ?>
			<div class="controls">
				<?php echo $form->passwordField($modelServer, 'push_control_key', array('placeholder' => 'allow to control server services')); ?>
				<?php echo $form->error($modelServer,'push_control_key'); ?>
			</div>
		</div>
		<div class="control-group">
			<?php echo $form->labelEx($modelServer,'push_status_key') ?>
			<div class="controls">
				<?php echo $form->passwordField($modelServer, 'push_status_key', array('placeholder' => 'allow to view server activity')); ?>
				<?php echo $form->error($modelServer,'push_status_key'); ?>
			</div>
		</div>	        
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
		<?php echo NHtml::submitButton('Save', array('class'=>'btn btn-primary')) . '&nbsp;'; ?>
	</div>
</div>

<?php $this->endWidget(); ?>

<!-- Switch to Other Server Modal -->

<div class="modal hide fade" id="ModuleServerModalSwitchServer" style="z-index:9998;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Switch Server</h3>
	</div>
	<div class="modal-body" style="padding-left: 35px;" id="ModuleServerModalSwitchServerList">
		<?php $this->renderPartial('ajax/_servers', array('servers' => $servers)); ?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" style="float:right;">Cancel</button>
	</div>
</div>

<!-- Current Server Settings Modal -->

<div class="modal hide fade" id="ModuleServerModalServerSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Server settings</h3>
	</div>
	<div class="modal-body">
		
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-primary" style="float:right;">OK</button>	
	</div>
</div>

<!-- General Manager Settings Modal -->

<div class="modal hide fade" id="ModuleServerModalSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Change default</h3>
	</div>
	<div class="modal-body" style="padding-left: 35px;" id="ModuleServerModalSettingsList">
		<?php $this->renderPartial('ajax/_servers', array('servers' => $servers)); ?>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" style="float:right;" data-dismiss="modal" aria-hidden="true">Done</button>
		<button class="btn btn-success" style="float:right; margin-right: 10px;" id="ModuleServerModalSettingsButtonSwitchToDefault">Switch to default</button>
	</div>
</div>

<!-- Empty Current Server Mongo DB -->

<div class="modal hide fade" id="ModuleServerModalServerMongoEmpty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Empty MongoDB database</h3>
	</div>	
	<div class="modal-body" style="height:150px;">
        <div class="row-fluid" style="margin-top: 42px; padding-left: 10px;">
            <div class="span3" style="text-align: center;">
                <i class="icon-book" style="font-size:100px; background-image: none;"></i>        
            </div>
            <div class="span5" style="text-align: center;">
                <i class="icon-arrow-right" style="font-size:100px; background-image: none;"></i>        
            </div>
            <div class="span2" style="text-align: center;">
                <i class="icon-trash" style="font-size:100px; background-image: none;"></i>                
            </div>            
        </div>        
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-danger" style="float:right;" id="ModuleServerModalServerMongoEmptyButtonOK">Yes, delete all collections</button>	
	</div>
</div>

<!-- Empty Current Server Mongo DB -->

<div class="modal hide fade" id="ModuleServerModalServerMongoInstall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Install</h3>
	</div>
	<div class="modal-body">
		Install default collections in DB for Newicon Push Services:
		<ul>
			<li>empty all collections</li>
			<li>create "accounts" collection</li>
			<li>create "apis" collection</li>
		</ul>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-primary" style="float:right;">OK</button>	
	</div>
</div>

<!-- Delete Server From Connection List -->

<div class="modal hide fade" id="ModuleServerModalServerDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Delete server</h3>
	</div>
	<div class="modal-body">
	<div class="modal-body" style="height:150px;">
        <div class="row-fluid" style="margin-top: 42px; padding-left: 10px;">
            <div class="span3" style="text-align: center;">
                <i class="icon-hdd" style="font-size:100px; background-image: none;"></i>        
            </div>
            <div class="span5" style="text-align: center;">
                <i class="icon-arrow-right" style="font-size:100px; background-image: none;"></i>        
            </div>
            <div class="span2" style="text-align: center;">
                <i class="icon-trash" style="font-size:100px; background-image: none;"></i>                
            </div>            
        </div>        
	</div>		
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-danger" style="float:right;" id="ModuleServerDeleteConnectionButtnOK">Yes, delete server connection</button>	
	</div>
</div>

<!-- Push Manager : Client Modal  -->

<?php

	$form = $this->beginWidget('NActiveForm', array(
		'id' => 'newClientForm',
		'action' => Yii::app()->createUrl('/pusher/manager/addClient/serverId/'),
		'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form, data, hasError){
				
				if(hasError) return;

				$.post(form.attr("action") + "/" + ModuleServerSelectedSettings.id, form.serialize(), function(data){
				
					switch(respondServer(data, "respondStatus"))
					{
						case "USERNAME_EXIST":
							$("#ModuleServerModalNewUserNameExist").removeClass("success").addClass("alert alert-error error");
							$("#ModuleServerModalNewUserNameExist").find(".errorMessage")
								.show()
								.html("Username already exists for another client. Choose diffrent username.");
						break;
						case "OK":
							$("#ModulePushManagerModalClient").find("input[type=text]").val("");
							$("#ModulePushManagerModalClient").modal("hide");
							
							ModulePusherGetClientListForServer();

							$.gritter.add({ title:"Server", text:"New Server successfuly added..." });
						break;
						case "ERROR":
							alert("ERROR");
						break;
					}
				});
				
				return false;
			}'),
		'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'htmlOptions'=>array('class'=>'form-horizontal')
	));

	$model = new PusherManagerClient();
?>

<div class="modal hide fade" id="ModulePushManagerModalClient" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Client</h3>
	</div>
	<div class="modal-body">
        <?php echo $form->hiddenField($model,'id', array('value'=>'0')); ?>
		<div class="control-group">
			<?php echo $form->labelEx($model,'name') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'name', array('placeholder' => 'e.g. Newicon Ltd.')); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($model,'dscr') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'dscr', array('placeholder' => 'e.g. IT Company')); ?>
				<?php echo $form->error($model,'dscr'); ?>
			</div>
		</div>	
		<div class="control-group" id="ModuleServerModalNewUserNameExist" style="padding-left:0px;">
			<?php echo $form->labelEx($model,'username') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'username', array('placeholder' => 'e.g. newicon')); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
		</div>	
		<div class="control-group">
			<?php echo $form->labelEx($model,'password') ?>
			<div class="controls">
				<?php echo $form->passwordField($model, 'password', array('placeholder' => 'e.g. Xq12WaQ9')); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>
		<div class="control-group">
			<?php echo $form->labelEx($model,'password_repeat') ?>
			<div class="controls">
				<?php echo $form->passwordField($model, 'password_repeat', array('placeholder' => 'e.g. Xq12WaQ9')); ?>
				<?php echo $form->error($model,'password_repeat'); ?>
			</div>
		</div>		
	</div>
	<div class="modal-footer">
        <button class="btn btn-danger hide" data-dismiss="modal" aria-hidden="true" style="float:left;">Delete client</button>
        <button class="btn btn-danger hide" data-dismiss="modal" aria-hidden="true" style="float:left;">Delete apps</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>        
        <?php echo NHtml::submitButton('Save', array('class'=>'btn btn-primary')) . '&nbsp;'; ?>
	</div>
</div>

<?php $this->endWidget(); ?>

<!-- Push Manager : App Modal  -->

<?php

	$form = $this->beginWidget('NActiveForm', array(
		'id' => 'newAppForm',
		'action' => Yii::app()->createUrl('/pusher/manager/addApp/serverId/'),
		'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form, data, hasError){
				
				if(hasError) return;

				$.post(form.attr("action") + "/" + ModuleServerSelectedSettings.id + "/clientId/" + ModuleManagerSelectedClientId, form.serialize(), function(data){
				
					switch(respondServer(data, "respondStatus"))
					{
						case "IDENTIFIER_EXIST":
							$("#ModulePushManagerModalAppIdentifierExist").removeClass("success").addClass("alert alert-error error");
							$("#ModulePushManagerModalAppIdentifierExist").find(".errorMessage")
								.show()
								.html("App ID already exists for another app. Choose diffrent ID.");
						break;
						case "OK":
							$("#ModulePushManagerModalApp").find("input[type=text]").val("");
							$("#ModulePushManagerModalApp").modal("hide");

							ModulePusherGetAppsListForClient();
							$.gritter.add({ title:"Application", text:"New app successfuly added..." });
						break;
						case "ERROR":
							alert("ERROR");
						break;
					}
				});
				
				return false;
			}'),
		'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'htmlOptions'=>array('class'=>'form-horizontal')
	));

	$model = new PusherManagerApp();
?>


<div class="modal hide fade" id="ModulePushManagerModalApp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">Application</h3>
	</div>
	<div class="modal-body">
		<div class="control-group">
			<?php echo $form->labelEx($model,'name') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'name', array('placeholder' => 'e.g. Messanger')); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>			
		<div class="control-group" id="ModulePushManagerModalAppIdentifierExist" style="padding-left:0px;">
			<?php echo $form->labelEx($model,'application_id') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'application_id', array('placeholder' => 'e.g. msn')); ?>
				<?php echo $form->error($model,'application_id'); ?>
			</div>
		</div>			
		<div class="control-group">
			<?php echo $form->labelEx($model,'dscr') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'dscr', array('placeholder' => 'e.g. Super Fast Communication')); ?>
				<?php echo $form->error($model,'dscr'); ?>
			</div>
		</div>		
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-primary" style="float:right;">OK</button>	
	</div>
</div>

<?php $this->endWidget(); ?>

<!-- Push Manager : API Key Modal  -->

<?php

	$form = $this->beginWidget('NActiveForm', array(
		'id' => 'newAPIForm',
		'action' => Yii::app()->createUrl('/pusher/manager/addAPIKey/serverId/'),
		'clientOptions'=>array(
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form, data, hasError){
				
				if(hasError) return;

				$.post(form.attr("action") + "/" + ModuleServerSelectedSettings.id + "/clientId/" + ModuleManagerSelectedClientId + "/appId/" + ModuleManagerSelectedAppId, form.serialize(), function(data){
				
					switch(respondServer(data, "respondStatus"))
					{
						case "OK":
							$("#ModulePushManagerModalAPIKey").find("input[type=text]").val("");
							$("#ModulePushManagerModalAPIKey").modal("hide");

							ModulePusherGetApisListForApp();
							$.gritter.add({ title:"API Key", text:"New API Key successfuly added..." });
						break;
						case "ERROR":
							alert("ERROR");
						break;
					}
				});
				
				return false;
			}'),
		'enableAjaxValidation'=>false,
		'enableClientValidation'=>true,
		'htmlOptions'=>array('class'=>'form-horizontal')
	));

	$model = new PusherManagerAPIKey();
?>

<div class="modal hide fade" id="ModulePushManagerModalAPIKey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="RBAlertWindowTitle">API Key</h3>
	</div>
	<div class="modal-body">
		<div class="control-group">
			<?php echo $form->labelEx($model,'name') ?>
			<div class="controls">
				<?php echo $form->textField($model, 'name', array('placeholder' => 'e.g. Production')); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>	
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:left;">Cancel</button>
        <button class="btn btn-primary" style="float:right;">OK</button>	
	</div>
</div>

<?php $this->endWidget(); ?>