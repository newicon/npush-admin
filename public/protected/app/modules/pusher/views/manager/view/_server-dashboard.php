<?php

/**
 *	Server Dashboard View 
 */

/*
 * ALARMS
 *  - server disconnected/connecting
 *  - cluster node died
 *  - CPU above %
 *  - MEM above %
 *  - sockets above number
 * 
 * SETTINGS
 *  - 
 */

?>

<style>
	h8{font-size:9px;}	
    .ml20{margin-left:20px;}
    .pl20{padding-left:20px;}
	.mb10{margin-bottom:10px;}
	.mb5{margin-bottom:5px;}
	.module-loading-spinner { height:60px; width:100%; background-repeat: no-repeat;background-position:center; background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/images/spinner.gif); }
	.module-preview-area { width:100%; height:100%; background-repeat: no-repeat;background-position:center; background-image: url(<?php echo Yii::app()->theme->baseUrl; ?>/images/nopreview.jpg);  }
	.border-sharp-right{border-top-right-radius:0px;border-bottom-right-radius:0px;}
	.border-sharp-left{border-top-left-radius:0px;border-bottom-left-radius:0px;}
	
	#ModuleContainerDashboardLeftTabs ul li a { padding:5px 10px 3px 11px; border-radius: 15px; margin-bottom:10px; margin-left:5px; }
	#ModuleContainerDashboardTabSettings ul li a { padding:5px 10px 3px 11px; border-radius: 15px; margin-bottom:10px; margin-left:5px; }
	#ModuleContainerDashboardLeftTabs .btn.active { color: white; }
	
	#ModuleContainer [class^="icon-"],
	[class*=" icon-"] {
		display: inline-block;
		width: auto;
		height: auto;
		*margin-right: .3em;
		font-size:18px;
		line-height: 16px;
		vertical-align: text-middle;
		background-image: none;
		background-position: 0px 0px;
		background-repeat: no-repeat;
	}	
</style>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/npush.js" type="text/javascript" rel="stylesheet" /></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/highcharts/2.3.0/highcharts.js" type="text/javascript" rel="stylesheet" /></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/../pusher/js/highcharts/2.3.0/highcharts-more.js" type="text/javascript" rel="stylesheet" /></script>		

<div class="row-fluid" id="ModuleContainer">
	<div class="span1" style="width:3%; height:100%; padding:5px; margin:0px; margin-top:6px;" id="ModuleContainerDashboardLeftTabs">
		<ul class="nav nav-pills">
			<li class="active"><a href="#ModuleContainerDashboardTabClients" data-toggle="tab" title1="Dashboard" data-placement="right" rel="tooltip"><i class="icon-dashboard"></i></a></li>
			<li><a href="#ModuleContainerDashboardTabReports" data-toggle="tab" title1="Details report" data-placement="right" rel="tooltip"><i class="icon-cogs"></i></a></li>
			<li><a href="#ModuleContainerDashboardTabAlarms" data-toggle="tab" title1="Alrams" data-placement="right" rel="tooltip"><i class="icon-bell"></i></a></li>
			<li><a href="#ModuleContainerDashboardTabSettings" data-toggle="tab" title1="Actions" data-placement="right" rel="tooltip"><i class="icon-magic"></i></a></li>
		</ul>
	</div>
    <div class="tab-content span3" style="width:30%; padding:10px; padding-right: 20px; margin-top:5px; margin-left:13px; overflow: none; height:100%; position: relative;" id="ModuleContainerDashboardLeft">
        <div class="tab-pane active" id="ModuleContainerDashboardTabClients" style="height:100%; -webkit-overflow-scrolling: touch;overflow: auto; box-shadow: 1px 1px 5px 2px #eee;border-radius:5px;">
            <table class="table table-striped" style="font-size:12px; margin-bottom: 1px;">
                <thead>
                    <th>Client</th>
                    <th>m/s</th>
                    <th>m/m</th>
                    <th>AS</th>
                    <th></th>
                </thead>
                <tbody id="ServerManagementAPIs">
                    <tr><td colspan="5">No active APIs</td></tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="ModuleContainerDashboardTabReports" style="height:100%; -webkit-overflow-scrolling: touch;overflow: auto; box-shadow: 1px 1px 5px 2px #eee;border-radius:5px;">
            <div class="row-fluid align-left">  
                <div class="span12 pl20">
                    <h4><div class="label label-info">Details</div></h4>
                </div>
            </div>
            <div class="row-fluid">
				&nbsp;&nbsp;HOST NAME / IP<br>
                &nbsp;&nbsp;mongo process details<br>
                &nbsp;&nbsp;cluster processes details<br>
				&nbsp;&nbsp;nodes: cpu,mem,api,sockets,time,errors<br>
                &nbsp;&nbsp;server processes details<br>
                &nbsp;&nbsp;db traffic details<br>
                &nbsp;&nbsp;sockets traffic details<br>
            </div>
        </div>
        <div class="tab-pane" id="ModuleContainerDashboardTabAlarms" style="height:100%; -webkit-overflow-scrolling: touch;overflow: auto; box-shadow: 1px 1px 5px 2px #eee;border-radius:5px;">
            <div class="row-fluid align-left">
                <div class="span12 pl20">
                    <h4><div class="label label-info">Alarms</div></h4>
                </div>
            </div>
            <div class="row-fluid">
                &nbsp;&nbsp;alarms stored on server in db<br>
                &nbsp;&nbsp;alarms by mail for user on list<br>
                &nbsp;&nbsp;alarms by sound in browser<br>                
            </div>
        </div>
        <div class="tab-pane" id="ModuleContainerDashboardTabSettings" style="height:100%; -webkit-overflow-scrolling: touch;overflow: auto; box-shadow: 1px 1px 5px 2px #eee;border-radius:5px;">
			<div class="row-fluid align-center">
				<div class="span12" style="padding:10px 0px 0px 10px; border-bottom: 1px dotted #eee;">
					<ul class="nav nav-pills align-center" style="margin-bottom:0px;">
						<li class="active"><a href="#ModuleContainerDashboardTabSettingsCluster" data-toggle="tab" title1="Dashboard" data-placement="right" rel="tooltip">Cluster</a></li>
						<li><a href="#ModuleContainerDashboardTabSettingsSockets" data-toggle="tab" title1="Details report" data-placement="right" rel="tooltip">Sockets</a></li>
						<li><a href="#ModuleContainerDashboardTabSettingsMongo" data-toggle="tab" title1="Alrams" data-placement="right" rel="tooltip">Mongo</a></li>
						<li><a href="#ModuleContainerDashboardTabSettingsRedis" data-toggle="tab" title1="Alrams" data-placement="right" rel="tooltip">Redis</a></li>
					</ul>            					
				</div>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="ModuleContainerDashboardTabSettingsCluster">
					<div class="row-fluid"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span6 align-right">
							<a href="#" class="btn w80 btn-mini" style="line-height:25px;">
								<i class="icon-sitemap" style="font-size:12px;"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-sitemap"></i>
								<br>add instance
							</a>
						</div>
						<div class="span6 align-left">
							<a href="#" class="btn w80 btn-mini" style="line-height:25px;">
								<i class="icon-cogs" style="font-size:12px;"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-bolt"></i>
								<br>select & kill
							</a>
						</div>
					</div>
					<div class="row-fluid"><br/></div>
					<div class="row-fluid align-center">
						<div class="span6 align-right">
							<a href="#" class="btn w80 btn-mini" style="line-height:25px;">
								<i class="icon-hdd"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-refresh"></i>
								<br>db reconn.
							</a>
						</div>		
						<div class="span6 align-left">
							<a href="#" class="btn w80 btn-mini" style="line-height:25px;">
								<i class="icon-sitemap"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-bolt"></i>
								<br>kill cluster
							</a>
						</div>							
					</div>
					<div class="row-fluid align-center"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<div class="btn-group">
								<a href="#" class="btn btn-mini btn-success" style="line-height:25px;">Cluster Start</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Stop</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Restart</a>
							</div>				
						</div>
					</div>							
				</div>
				<div class="tab-pane" id="ModuleContainerDashboardTabSettingsSockets">
					<div class="row-fluid"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<a href="#" class="btn w100 btn-mini" style="line-height:25px;">
							<i class="icon-comments-alt"></i>
							&nbsp;<i class="icon-arrow-right"></i>
							&nbsp;<i class="icon-trash"></i>
							<br>delete all sockets
							</a>
						</div>
					</div>
					<div class="row-fluid align-center"><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<a href="#" class="btn w100 btn-mini" style="line-height:25px;">
							<i class="icon-cogs"></i>
							&nbsp;<i class="icon-arrow-right"></i>
							&nbsp;<i class="icon-trash"></i>
							<br>delete by instance
							</a>
						</div>
					</div>					
				</div>
				<div class="tab-pane" id="ModuleContainerDashboardTabSettingsMongo">
					<div class="row-fluid"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">			
							<a href="#" class="btn w100 btn-mini" style="line-height:25px;">
								<i class="icon-hdd"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-bolt"></i>
								<br>kill process
							</a>
						</div>
					</div>
					<div class="row-fluid align-center"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<div class="btn-group">
								<a href="#" class="btn btn-mini btn-success" style="line-height:25px;">Service Start</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Stop</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Restart</a>
							</div>				
						</div>
					</div>
				</div>
				<div class="tab-pane" id="ModuleContainerDashboardTabSettingsRedis">
					<div class="row-fluid"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<a href="#" class="btn w100 btn-mini" style="line-height:25px;">
								<i class="icon-inbox"></i>
								&nbsp;<i class="icon-arrow-right"></i>
								&nbsp;<i class="icon-bolt"></i>
								<br>kill process
							</a>		
						</div>
					</div>
					<div class="row-fluid align-center"><br/><br/></div>
					<div class="row-fluid align-center">
						<div class="span12">
							<div class="btn-group">
								<a href="#" class="btn btn-mini btn-success" style="line-height:25px;">Service Start</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Stop</a>
								<a href="#" class="btn btn-mini" style="line-height:25px;">Restart</a>
							</div>								
						</div>
					</div>
				</div>      				
			</div>				
		</div>              
    </div>
    <div class="span8" id="ModuleContainerDashboardRight" style="width:65%; margin-left:4px; margin-top:5px;">
        <div class="row-fluid" id="ModuleContainerDashboardMain" style="border:0px solid #eee;overflow:hidden;">
            <div class="row-fluid" style="height:100%; position: relative; text-align: center;">
				<br>
				<div id="ModuleContainerDashboardChart" style="height:85%;">
					<br/><br/><br/><br/>
					<h3>THIS IS DRAFT - NOW IN DEVELOPMENT</h3>
					<h4>Chart - speed meter / msg/time / db/s</h4>
					<h4>history in time: last 60 second / last 60 minutes</h4>
					<h4>CPU/MEM for mongo/node</h4>
					<h4>connection/disconnection per second</h4>
					<h4>stop traffic/start traffic</h4>
				</div>
            </div>
            <div style="position:relative; width:20%; top:-98%; text-align: left; float:left; border:0px solid black;">
                <div class="row-fluid">
                    <div class="row-fluid">                        
                        <div class="label" id="ServerDashboardTrafficMPH">--- msg/h</div>
                    </div>
                    <div class="row-fluid">
                        <div class="label" id="ServerDashboardTrafficMPM">--- msg/m</div>
                    </div>					
                    <div class="row-fluid">
                        <div class="label" id="ServerDashboardTrafficMPS">--- msg/s</div>
                    </div>
                </div>
            </div>
            <div style="position:relative; width:20%; top:-98%; text-align: right; float:right; border:0px solid black;">
                <div class="row-fluid">
                    <div class="label label-success hide">Connected</div>
                </div>                                
                <div class="row-fluid">
                    <div class="label border-sharp-right" title="This server usage" data-placement="left" rel="tooltip">CPU</div><div class="label label-important border-sharp-left w40" id="ServerDashboardOSCPU">---</div>
                </div>
                <div class="row-fluid hide">
                    <div class="label border-sharp-right">npush</div><div class="label border-sharp-left" id="ServerDashboardNodeCPU">---</div>
                </div>		
                <div class="row-fluid hide">
                    <div class="label border-sharp-right">mongo</div><div class="label border-sharp-left" id="ServerDashboardMongoCPU">---</div>
                </div>   
				<div class="row-fluid hide"><br></div>
                <div class="row-fluid">
                    <div class="label border-sharp-right" title="This server usage" data-placement="left" rel="tooltip">MEM</div><div class="label label-important border-sharp-left w40" id="ServerDashboardOSMem">---</div>
                </div>
                <div class="row-fluid hide">
                    npush <div class="label" id="ServerDashboardNodeMem">---</div>
                </div>			
                <div class="row-fluid hide">
                    mongo <div class="label" id="ServerDashboardMongoMem">---</div>
                </div>
             	<div class="row-fluid hide"><br></div>
                <div class="row-fluid">
                    <div class="label border-sharp-right" title="This server instances" data-placement="left" rel="tooltip">Cluster</div><div class="label label-important border-sharp-left w40" id="ServerDashboardNPUSHInstances">---</div>
                </div>					
                <div class="row-fluid">
                    <div class="label border-sharp-right" title="All Servers & Instances" data-placement="left" rel="tooltip">Sockets</div><div class="label label-info border-sharp-left w40" id="ServerDashboardSockets">---</div>
                </div>                
                <div class="row-fluid">
                    <div class="label border-sharp-right">APIs</div><div class="label label-info border-sharp-left w40" id="ServerDashboardAPIs">---</div>
                </div> 			
            </div>            
            <div style="position:relative; width:50%; top:-98%; margin:auto; text-align: center; border:0px solid black;">
                <div class="btn-group" id="ServerDashboardTabs">
                    <a href="#" class="btn btn-mini" code="socket">Sockets</a>
                    <a href="#" class="btn btn-mini active" code="os">OS</a>
                    <a href="#" class="btn btn-mini" code="db">Database</a>                    
                </div>
            </div>
            <div id="ModuleContainerDashboardToolbar" style="position:absolute; width:66%; bottom:120px; height:40px; margin:auto; padding:0px 25px 0px 0px; text-align: center; border:0px solid black;">
                <div class="btn-group" style="margin:0px 10px 0px 10px;">
                    <a href="#" class="btn btn-mini btn-success" id="ModuleContainerDashboardToolbarTrafficON" title="Start traffic" data-placement="top" rel="tooltip" >Traffic Enabled</a>
                    <a href="#" class="btn btn-mini" id="ModuleContainerDashboardToolbarTrafficOFF" title="Stop public traffic / Monitoring still ON" data-placement="top" rel="tooltip" >Traffic Disabled</a>
                </div>
            </div>
		</div>
		<div class="row-fluid h100" id="ModuleContainerDashboardBottom" style="box-shadow: 1px 1px 5px 2px #eee; border-radius: 8px; border:0px solid #000;">
            <div class="row-fluid" style="padding: 5px 10px 5px 10px;">
                <div class="span4">
                    <div id="ModuleContainerDashboardChartBottom1" style="width:90%; height:96px; ">
						
					</div>
                </div>
                <div class="span4">
                    <div id="ModuleContainerDashboardChartBottom2" style="width:90%; height:96px;">
						
					</div>
                </div>				
                <div class="span4">
                    <div id="ModuleContainerDashboardChartBottom3" style="width:90%; height:96px;">
						
					</div>
                </div>
            </div>
		</div>
    </div>    
</div>

<script>
	
	var ModuleViewWidth = 0;
	
	// messages
	
	var MSG_TRAFFIC_STOPPED = '<div class="label label-important p4">APIs TRAFFIC LIMITED TO SERVICE CHANNELS ONLY</div>';
	var MSG_TRAFFIC_ALLOWED = '<div class="label label-info p4">FULL APIs TRAFFIC ALLOWED</div>';
	
	var MSG_SERVER_CONNECTED = '<div class="label label-info p4">MONITORING IS ON-LINE</div>';
	var MSG_SERVER_DISCONNECTED = '<div class="label label-important p4">SERVER DISCONNECTED</div>';
	var MSG_SERVER_CONNECTING = '<div class="label label-warning p4">... CONNECTING TO THE SERVER ...</div>';
	
	// server settings
	
	var ModuleServerSelectedSettings =	{ 
											id : <?php echo isset($server) ? $server->id : "0" ?>,
											name : '<?php echo isset($server) ? $server->config_name : "0" ?>',
											m_address : '<?php echo isset($server) ? $server->mongo_address : "0" ?>',
											m_port : '<?php echo isset($server) ? $server->mongo_port : "0" ?>',
											m_username : '<?php echo isset($server) ? $server->mongo_username : "0" ?>',
											m_password : '<?php echo isset($server) ? $server->mongo_password : "0" ?>',
											m_dbname : '<?php echo isset($server) ? $server->mongo_dbname : "0" ?>',
											p_address : '<?php echo isset($server) ? $server->push_address : "0" ?>',
											p_port : '<?php echo isset($server) ? $server->push_port : "0" ?>',
											p_version : '<?php echo isset($server) ? $server->push_version : "0" ?>',
											p_control_key: '<?php echo isset($server) ? $server->push_control_key : "0" ?>',
											p_status_key: '<?php echo isset($server) ? $server->push_status_key : "0" ?>'
										};	
	
	var mystatsserver = null;
	var server_traffic_state = -1;
	
	var ModuleServerStatusData = null;
	var ModuleServerClients = {};	
	var ModuleServerSelectedConnectionStatus = 0;
	
	//tmp
	
	var ModuleViewSwitchPreviewOfValues = 0;
	
	// VIEW
	
	var ModuleViewDashboardTabSeledted = 'os';
	
	// GRAPHS
	
	var CHART_PUSHER_TRAFFIC_PEAK1 = false;
	var CHART_PUSHER_TRAFFIC_PEAK2 = false;
	var CHART_PUSHER_TRAFFIC_PEAK3 = false;
	
	// Methods
    
    function ModulePusherSubscribeServiceChannel() {

		npush.init({ 
				'apiKey' : '<?php echo $server['push_status_key']; //$server['push_address']; ?>',
				'url' : '<?php echo $server['push_address']; ?>', 
				'port' : <?php echo $server['push_port']; ?>, 
				'user_id' : '<?php echo $userId; ?>', 
				'user_name' : '<?php echo $userName; ?>'
			}		
		);		
				
		mystatsserver = npush.subscribe('<?php echo $channel; ?>/<?php echo $room; ?>', {
			
				'connect' : ModulePusherSubscribeService_connected,
				'connecting' : ModulePusherSubscribeService_connecting,
				'reconnecting' : ModulePusherSubscribeService_reconnecting,
				'disconnect' : ModulePusherSubscribeService_disconnect,
				'error' : ModulePusherSubscribeService_error,
				'announcement' : ModulePusherSubscribeService_announcement
			} 		
		);
	}
	
	function ModulePusherGetClientList()
	{
		if(ModuleServerSelectedSettings.id < 1) return;
		
		$('#ServerManagementAPIs').empty().html('<tr><td colspan="5"><div class="module-loading-spinner"></div></td></tr>');
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getClientsInJSON/serverId/'); ?>/' + ModuleServerSelectedSettings.id, function(data) {
			
			ModuleServerClients = JSON.parse(data);
			ModuleServerSelectedConnectionStatus = 1;
			
			if(ModuleServerStatusData != null)
				ModulePusherSubscribeService_announcement(ModuleServerStatusData);
			
		}).error(function(e) {
			
			ModuleServerSelectedConnectionStatus = 0;
		});
	}	
	
// npush channel events

    function ModulePusherSubscribeService_connected()
    {
		server_traffic_state = -1;
		ModuleServerStatusBar(MSG_SERVER_CONNECTED);	
    }

    function ModulePusherSubscribeService_connecting() 
    {
		ModuleServerStatusBar(MSG_SERVER_CONNECTING);
    }
	
	function ModuleServerStatusBar(_msg)
	{
		if(_bar = window.parent.document.getElementById('ServerManagementStatusBar')) {
			
			$(_bar).fadeOut('slow', function() {
				
				$(_bar).html(_msg).fadeIn('slow');
			});
		}
	}

    function ModulePusherSubscribeService_announcement(data) {		

        if(toString.call(data) != '[object Object]') return;
		
		ModuleServerStatusData = data;
		
		if(ModuleViewSwitchPreviewOfValues++ == 5)
			ModuleViewSwitchPreviewOfValues = 0;

        if(data.node.length) {
            
            if(ModuleViewSwitchPreviewOfValues) {

                $('#ServerDashboardNodeMem').html(data.node[0].pmem);
            } else {
                $('#ServerDashboardNodeMem').html(data.node[0].rss + '%');
            }

            $('#ServerDashboardNodeCPU').html(data.node[0].cpu + '%');
        }
        
        if(data.mongo.length) {
			if(ModuleViewSwitchPreviewOfValues) {
				$('#ServerDashboardMongoMem').html(data.mongo[0].pmem);				
			} else {
				$('#ServerDashboardMongoMem').html(data.mongo[0].rss + '%');
			}
			$('#ServerDashboardMongoCPU').html(data.mongo[0].cpu +'%')
		}

		if(ModuleViewSwitchPreviewOfValues) {
			$('#ServerDashboardOSMem').html(roundNumber((data.os.tm-data.os.fm)/1024,2) + 'g');
		} else {
			$('#ServerDashboardOSMem').html(roundNumber(((data.os.tm-data.os.fm)/data.os.tm)*100,0) + '%');
		}
		
        $('#ServerDashboardAPIs').html(Object.keys(data.api.act).length);
		$('#ServerDashboardOSCPU').html(roundNumber(data.os.cpu*100, 0) + '%');   
        $('#ServerDashboardSockets').html(data.sockets.active);
		$('#ServerDashboardNPUSHInstances').html(data.os.node.i+'/'+data.os.node.s);
		
		// traffic info
		
		$('#ServerDashboardTrafficMPS').html(data.traffic.mps +  ' msg/s');
		$('#ServerDashboardTrafficMPM').html(data.traffic.mpm +  ' msg/m');
		$('#ServerDashboardTrafficMPH').html(data.traffic.mph +  ' msg/h');
		
		if(server_traffic_state != data.sockets.traffic) {
			if(data.sockets.traffic) {

				$('#ModuleContainerDashboardToolbarTrafficON').addClass('btn-success');
				$('#ModuleContainerDashboardToolbarTrafficOFF').removeClass('btn-danger');
				
				ModuleServerStatusBar(MSG_TRAFFIC_ALLOWED);
				
			} else {

				$('#ModuleContainerDashboardToolbarTrafficON').removeClass('btn-success');
				$('#ModuleContainerDashboardToolbarTrafficOFF').addClass('btn-danger');			
				
				ModuleServerStatusBar(MSG_TRAFFIC_STOPPED);
			}
			
			server_traffic_state = data.sockets.traffic;
		}
		
		// render list of active APIs
		
		_.sortBy(data.api.act, function(d) { return d.mpm; });
			
		var apis = '';
		
		for(api in data.api.act) {
			
			if(toString.call(data.api.act[api]) == '[object Object]') {
			
				apis = apis + '<tr>'
					+ '<td>' + (ModuleServerClients.hasOwnProperty(api) ? ModuleServerClients[api].cn + '<br>' + ModuleServerClients[api].appn + '<br>' + ModuleServerClients[api].apin + '<br>' : (api == ModuleServerSelectedSettings.p_status_key) ? 'SYSTEM' : '<h8>' + api + '</h8>') + '</td>'
					+ '<td><div class="align-right">' + data.api.act[api].mps + '</div></td>'
					+ '<td><div class="align-right">' + data.api.act[api].mpm + '</div></td>'
					+ '<td><div class="align-right">' + data.api.act[api].s + '</div></td>'
					+ '<td><div class=" ' + (api == ModuleServerSelectedSettings.p_status_key ? 'hide' : '') + '">'
					+ '<a href="#" class="btn btn-mini hide">on</a>'
					+ '<a href="#" class="btn btn-mini hide">off</a>'
					+ '<a href="#" class="btn btn-mini"><i class="icon-wrench" style="font-size:14px;"></i></a>'
					+ '</div></td>'
					+ '</tr>';
			}
		}
		
		for(i=0;i<9;i++) {
		
			apis = apis + '<tr><td colspan="5" style="background-color:white;">&nbsp;</td></tr>';
		}
		
		if(apis != '')
			$('#ServerManagementAPIs').empty().append(apis);
			
		// set CPU meter
		
		if(CHART_PUSHER_SPEED_METER) {
            
			var _value = 0;
			
			switch(ModuleViewDashboardTabSeledted) {

				case 'socket':
					_value = data.traffic.mps;
				break;
				case 'os':
					_value = roundNumber(data.os.cpu*100,0);
				break;
				case 'db':
					_value = data.traffic.dbw + data.traffic.dbr + data.traffic.dbu;
				break;
			}
			
            var point = CHART_PUSHER_SPEED_METER.series[0].points[0];
			point.update(_value);
		}
		
        if(CHART_PUSHER_TRAFFIC_PEAK1) {
            
			var _value = -1;
			var _value1 = -1;
			
			switch(ModuleViewDashboardTabSeledted) {

				case 'socket':
					_value = data.traffic.mps;
				break;
				case 'os':
					_value = data.os.cpu*100;
					_value1 = roundNumber(((data.os.tm-data.os.fm)/data.os.tm)*100,0);
				break;
				case 'db':
					_value = data.traffic.dbw;
				break;
			}			
			
            var series = CHART_PUSHER_TRAFFIC_PEAK1.series[0]; 
			var series1 = CHART_PUSHER_TRAFFIC_PEAK1.series[1]; 
            var x = (new Date()).getTime();  
			
			if(_value > -1)
				series1.addPoint([x, _value], true, true);
			if(_value1 > -1)
				series.addPoint([x, _value1], true, true);			
        }		
		
        if(CHART_PUSHER_TRAFFIC_PEAK2) {
            
			var _value = -1;
			var _value1 = -1;
			
			switch(ModuleViewDashboardTabSeledted) {

				case 'socket':
					_value = data.sockets.active;
				break;
				case 'os':
					_value = 0;
					_value1 = 0;                    
                    if(data.mongo.length) {
                        _value = parseFloat(data.mongo[0].cpu);
                        _value1 = parseFloat(data.mongo[0].pmem);
                    }
				break;
				case 'db':
					_value = data.traffic.dbu;
				break;
			}			
			
            var series = CHART_PUSHER_TRAFFIC_PEAK2.series[0];   
			var series1 = CHART_PUSHER_TRAFFIC_PEAK2.series[1];
            var x = (new Date()).getTime();           
			if(_value > -1)
				series1.addPoint([x, _value], true, true);
			if(_value1 > -1)
				series.addPoint([x, _value1], true, true);
        }	

        if(CHART_PUSHER_TRAFFIC_PEAK3) {
            
			var _value = -1;
			var _value1 = -1;
			
			switch(ModuleViewDashboardTabSeledted) {

				case 'socket':
					_value = Object.keys(data.api.act).length;
				break;
				case 'os':
					_value = 0;
					_value1 = 0;
                    if(data.node.length) {
                        for(n in data.node) {

                            if(data.node[n].cpu != undefined && data.node[n].cpu != '')
                                _value += roundNumber(parseFloat(data.node[n].cpu),3);

                            if(data.node[n].cpu != undefined && data.node[n].pmem != '')
                                _value1 += roundNumber(parseFloat(data.node[n].pmem),3);
                        }
                    }
				break;
				case 'db':
					_value = data.traffic.dbr;
				break;
			}			
			
            var series = CHART_PUSHER_TRAFFIC_PEAK3.series[0];            
			var series1 = CHART_PUSHER_TRAFFIC_PEAK3.series[1];  
            var x = (new Date()).getTime();
			
			if(_value > -1)
				series1.addPoint([x, _value], true, true);
			if(_value1 > -1)
				series.addPoint([x, _value1], true, true);
        }	
    }

    function ModulePusherSubscribeService_disconnect() { 

		try {
			ModuleServerStatusBar(MSG_SERVER_DISCONNECTED);
		} catch(e) {
			
		}
    } 

    function ModulePusherSubscribeService_reconnecting() {

		ModuleServerStatusBar(MSG_SERVER_CONNECTING);
    }

    function ModulePusherSubscribeService_error(e) {

		try {
			
		} catch(e) {
			
		}			
    }
    
    function ModulePusherSubscribeService_apiactivity(data) {

        try {
            
            //$('#apiactivity').empty().append($('<span><strong>Traffic meter</strong></span><br><span class="badge badge-inverse">' + data.mps + ' msg/s</span>&nbsp;<span class="badge badge-inverse">' + data.mpm + ' msg/m</span>&nbsp;<span class="badge badge-inverse">' + data.mph + ' msg/h</span>&nbsp;<span class="badge badge-inverse">' + data.m + ' msg/server-running</span>&nbsp;'));
			//$('#dbactivity').empty().append($('<span><strong>Database meter</strong></span><br><span class="badge badge-inverse">' + data.dbrps + ' reads/s</span>&nbsp;<span class="badge badge-inverse">' + data.dbwps + ' writes/s</span>&nbsp;<span class="badge badge-inverse">' + data.dbups + ' updates/s</span>&nbsp;'));
			//$('#sysactivity').empty().append($('<span><strong>System meter</strong></span><br><span class="badge badge-inverse">PID MEM-RSS: ' + roundNumber(data.proc.mem/1048576, 2) + ' MB</span>&nbsp;<span class="badge badge-inverse">PID: ' + data.proc.pid + '</span>&nbsp;'));
            
        } catch(e) {}
    }	
	
	function ModuleViewGraphSeriesCreate(_series_name) {
	
		var _series = new Array();
		
		for(s in _series_name) {
			
			_series[s] = {
				name:_series_name[s],
				data: (function() {
					// generate an array of random data
					var data = [],
						time = (new Date()).getTime(),
						i;

					for (i = -59; i <= 0; i++) {
						data.push({
							x: time + i * 1000,
							y: 0
						});
					}
					return data;
				})()
			};
		}	
		
		return _series;
	}
	
    function ModuleViewDashboardTab() {
    
		switch(ModuleViewDashboardTabSeledted) {
			
			case 'socket':
				
				var _series = ModuleViewGraphSeriesCreate([' ', ' ']);
				
				CHART_PUSHER_SPEED_METER_YAXIS_MAX = 400;
				CHART_PUSHER_SPEED_METER = ModuleChartGeneratorChannel('ModuleContainerDashboardChart', 'Sockets', 'total messsages per second');
				CHART_PUSHER_SPEED_METER_YAXIS_MAX = null;
				CHART_PUSHER_TRAFFIC_PEAK1 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom1', 'msg/s', '', _series);
				CHART_PUSHER_TRAFFIC_PEAK2 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom2', 'sockets', '', _series);
				CHART_PUSHER_TRAFFIC_PEAK3 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom3', 'apis', '', _series);			
				
			break;
			case 'db':
				
				var _series = ModuleViewGraphSeriesCreate([' ', ' ']);
				
				CHART_PUSHER_SPEED_METER_YAXIS_MAX = 1000;
				CHART_PUSHER_SPEED_METER = ModuleChartGeneratorChannel('ModuleContainerDashboardChart', 'DB', 'total operation per second');
				CHART_PUSHER_SPEED_METER_YAXIS_MAX = null;
				CHART_PUSHER_TRAFFIC_PEAK1 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom1', 'Writes', '', _series);
				CHART_PUSHER_TRAFFIC_PEAK2 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom2', 'Updates', '', _series);
				CHART_PUSHER_TRAFFIC_PEAK3 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom3', 'Reads', '', _series);		
				
			break;
			case 'os':
				
				var _series = ModuleViewGraphSeriesCreate(['MEM', 'CPU']);
				
				CHART_PUSHER_SPEED_METER_YAXIS_MAX = 100;
				CHART_PUSHER_SPEED_METER = ModuleChartGeneratorChannel('ModuleContainerDashboardChart', 'CPU', '% of usage');
				CHART_PUSHER_TRAFFIC_PEAK1 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom1', 'OS', '%', _series);
				CHART_PUSHER_TRAFFIC_PEAK2 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom2', 'Mongo', '%', _series);
				CHART_PUSHER_TRAFFIC_PEAK3 = ModuleChartGeneratorThumbnailPercent('ModuleContainerDashboardChartBottom3', 'n:Push', '%', _series);							
				
			break;
		}
    }
    
	jQuery(document).ready(function() {
		
		ModuleViewWidth = $('#ModuleContainer').parent().width();
		
		ModuleUpdateViews();		
		$(window).resize(ModuleUpdateViews);
		
		ModulePusherGetClientList();
		ModulePusherSubscribeServiceChannel();
		
        $('#ServerDashboardTabs').find('.btn').click(function(){
           
		   $(this).parent().find('.btn').removeClass('active');
		   $(this).addClass('active');
		   
		   ModuleViewDashboardTabSeledted = $(this).attr('code');
           ModuleViewDashboardTab();
        });
		
		$('#ModuleContainerDashboardToolbarTrafficON').click(function() {
			
			NIPUSH.trigger('npush', 'system', {
												action:	'traffic',
												value:	'1'
											});
											
			$('#ModuleContainerDashboardToolbarTrafficON').addClass('btn-success');
			$('#ModuleContainerDashboardToolbarTrafficOFF').removeClass('btn-danger');	
			
			server_traffic_state = true;
			
			ModuleServerStatusBar(MSG_TRAFFIC_ALLOWED);
		});
		
		$('#ModuleContainerDashboardToolbarTrafficOFF').click(function() {
			
			NIPUSH.trigger('npush', 'system', {
												action:	'traffic',
												value:	'0'
											});
											
			$('#ModuleContainerDashboardToolbarTrafficON').removeClass('btn-success');
			$('#ModuleContainerDashboardToolbarTrafficOFF').addClass('btn-danger');	
			
			server_traffic_state = false;
			
			ModuleServerStatusBar(MSG_TRAFFIC_STOPPED);
		});
		
		ModuleViewDashboardTab();
		ModuleServerStatusBar(MSG_SERVER_CONNECTING);
	});
	
	function ModuleUpdateViews() {
		
		$('#ModuleContainer').width(ModuleViewWidth);
		$('#ModuleContainer').height($(window).height());	
		$('#ModuleContainerDashboardMain').height($('#ModuleContainer').height() - $('#ModuleContainerDashboardBottom').height()- 10);
		$('#ModuleContainerDashboardMain, #ModuleContainerDashboardBottom').width($('#ModuleContainerDashboardRight').width() - 3);
		
		if($('#ModuleContainerDashboardMain').height() < 435) {
			
			$('#ModuleContainerDashboardToolbar').css('text-align', 'right');
		} else {
			$('#ModuleContainerDashboardToolbar').css('text-align', 'center');
		}
	}
	
</script>

<?php $this->renderPartial('js/_common', array()); ?>
<?php $this->renderPartial('js/_graphs', array()); ?>
<?php $this->renderPartial('js/_pusher', array()); ?>