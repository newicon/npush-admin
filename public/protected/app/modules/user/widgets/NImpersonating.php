<?php

/**
 * NImpersonating class file.
 *
 * @author Steven O'Brien <steven.obrien@newicon.net>
 * @link http://newicon.net/framework
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */

/**
 * show a message displaying the user you are impersonating and a restore option
 *
 * @author 
 */
class NImpersonating extends NWidget
{
	public function run()
	{
		$this->render('impersonating', array());
	}
}