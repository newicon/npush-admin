<?php

/**
 * Nii view file.
 * 
 * Renders a user.
 * 
 * To be expanded to show a popup card.
 * Gets passed $user as a User row
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>
<?php  if ($this->includeName): ?>
<?php $attr = isset($user) ? 'data-user="' . $user->id . '"' : ''; ?>
<div class="media man" <?php echo $attr; ?>>
	<a class="img" style="padding:2px;">
		<?php
			if(isset($user->name) || $this->showUnknownImage) {
				$this->widget('user.widgets.NUserImage',array(
					'user'=>$user,
					'size'=>$size
				)); 
			}
		?>
	</a>
	<div class="bd pls">
		<p><?php echo isset($user->name) ? $user->name : $this->unknown; ?></p>
	</div>
</div>
<?php else: ?>
	<?php
		if(isset($user->name) || $this->showUnknownImage) {
			$this->widget('user.widgets.NUserImage',array(
				'user'=>$user,
				'size'=>$size
			)); 
		}
	?>
<?php endif; ?>
